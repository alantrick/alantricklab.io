#!/usr/bin/env python3

# AUTHOR = ''
SITENAME = 'Alan Trick’s Website'
SITEURL = 'http://localhost:8000'

PATH = 'content'
OUTPUT_PATH = 'public'

STATIC_PATHS = ['']
PLUGIN_PATHS = ['plugins']
PLUGINS = ['rst_bib', 'html5_rst_reader', 'filetime_from_git',
           'tipue_search', 'rst_strike']

THEME = 'themes/attila'
DIRECT_TEMPLATES = ['index', 'categories', 'authors', 'archives',
                    'search']

TIMEZONE = 'America/Vancouver'

DEFAULT_LANG = 'en'
# metadata
PATH_METADATA = '(?P<slug>.*)\.rst'
DEFAULT_METADATA = {
    'author': 'Alan Trick',
}
MENUITEMS = (
    ('Home', '/'),
    ('Contact', '/contact'),
    ('Projects', '/projects'),
    ('Resume (PDF)', '/resume.pdf'),
    ('Trips', '/tag/trip'),
    ('Programming', '/tag/programming'),
    ('Recipes', '/tag/recipe'),
    ('Papers', '/tag/paper'),
    ('Sites I Like', '/sites_i_like'),
)

# nice urls

PAGE_URL = 'page/{slug}'
PAGE_SAVE_AS = 'page/{slug}.html'
ARTICLE_URL = '{slug}'
ARTICLE_SAVE_AS = '{slug}.html'
TAG_URL = 'tag/{slug}'
TAG_SAVE_AS = 'tag/{slug}.html'
CATEGORY_URL = 'category/{slug}'
CATEGORY_SAVE_AS = 'category/{slug}.html'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

HEADER_COLOR = 'green'
HEADER_COVER = '/trips/2016/shuksan_clouds.jpg'
HEADER_COVERS_BY_TAG = {}
HEADER_COVERS_BY_CATEGORY = {}

LINKS = (('Timothy', 'https://timothytrick.ca/'),)

SOCIAL = (('gitlab', 'https://gitlab.com/alantrick'),
          ('flickr', 'https://www.flickr.com/alantrick/'),
          ('email', 'mailto:me@alantrick.ca'))

DEFAULT_PAGINATION = 10

AUTHORS_BIO = {
  "Alan Trick": {
    'name': 'Alan Trick',
    # "cover": "https://arulrajnet.github.io/attila-demo/assets/images/avatar.png",
    # "image": "https://arulrajnet.github.io/attila-demo/assets/images/avatar.png",
    'website': 'https://alantrick.ca',
    'location': '',
    'bio': ''
  }
}

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = False
