# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from docutils import nodes
from docutils.parsers.rst import directives, Directive


def media(argument):
    """Conversion function for the "class" option."""
    return directives.choice(argument, (
        'web', 'ebook', 'cdrom', 'email', 'speech'))


class Bib(Directive):
    """ Display a bibliographic entry

    Usage:

    .. bib::
        :authors: George and Bill
        :title: How to eat an Apple
        :journal: Eating
        :volume: 20
        :published: 2002
        :pages: 9-12
        :url: http://howtoeatapple.com
        :accessed: 2005-2-1
    """

    required_arguments = 0
    optional_arguments = 19
    option_spec = {
        'retrieved': directives.unchanged,
        'published': directives.unchanged,
        'authors': directives.unchanged,
        'author': directives.unchanged,
        'chapter': directives.unchanged,
        'title': directives.unchanged,
        'journal': directives.unchanged,
        'volume': directives.unchanged,
        'issue': directives.unchanged,
        'pages': directives.unchanged,
        'page': directives.unchanged,
        'url': directives.uri,
        # ignored for now
        'ref-page': directives.unchanged,
        'ref-pages': directives.unchanged,
        'publisher': directives.unchanged,
        'edition': directives.unchanged,
        'issn': directives.unchanged,
        'doi': directives.unchanged,
        'media': media
    }

    final_argument_whitespace = False
    has_content = False

    def get_title(self, emphasis=False):
        if 'title' not in self.options:
            return None
        node = nodes.inline(text=self.options['title'])
        if emphasis:
            node = nodes.emphasis(text=self.options['title'])
        if 'url' in self.options:
            result = nodes.reference(text='', refuri=self.options['url'])
            result.append(node)
            return result

        return node

    def run(self):
        url = self.options.get('url')
        parts = []
        authors = self.options.get('authors')
        if authors is None:
            authors = self.options.get('author')
        if authors is not None:
            parts.append(authors)
        if 'published' in self.options:
            parts.append(self.options['published'])

        title = self.get_title()

        title_node = nodes.inline(text='')
        if 'chapter' in self.options:
            title_node.append(nodes.inline(
                text=self.options['chapter'] + '. In '))
        if 'journal' in self.options:
            if title is not None:
                parts.append(title)
            title_node.append(nodes.emphasis(
                text=self.options['journal']))
            spec = ''
            if 'volume' in self.options:
                spec += self.options['volume']
            if 'issue' in self.options:
                spec += '({})'.format(self.options['issue'])
            if spec != '':
                title_node.append(nodes.inline(text=', ' + spec))
        elif title is not None:
            # TODO: emphasis
            title_node.append(self.get_title(True))
        elif url:
            title_node.append(nodes.reference(refuri=url, text=url))
        parts.append(title_node)

        pages = self.options.get('pages')
        if pages is None:
            pages = self.options.get('page')
        if pages is not None:
            page_parts = pages.split('-', 1)
            if len(page_parts) == 1:
                page_text = 'p. {}'.format(pages)
            else:
                page_text = 'pp. ' + '–'.join(page_parts)
            parts.append(nodes.inline(text=page_text))
        media_spec = []

        if 'media' in self.options:
            media_spec.append(self.options['media'])
        if 'retrieved' in self.options:
            media_spec.append('retrieved ' + self.options['retrieved'])
        if media_spec:
            parts.append(nodes.inline(
                text='({})'.format(', '.join(media_spec))))
        node = nodes.inline()

        for idx, part in enumerate(parts):
            if idx > 0:
                node.append(nodes.inline(text='. '))
            if isinstance(part, str):
                node.append(nodes.inline(text=part))
            else:
                node.append(part)
        if parts:
            node.append(nodes.inline(text='.'))
        return [node]


def register():
    directives.register_directive('bib', Bib)
