from pelican import signals
from pelican.readers import RstReader
from docutils.writers.html5_polyglot import Writer, HTMLTranslator
import docutils


def render_node_to_html(document, node):
    visitor = FieldBodyTranslator(document)
    node.walkabout(visitor)
    return visitor.astext()


class PelicanHTMLTranslator(HTMLTranslator):
    def visit_abbreviation(self, node):
        attrs = {}
        if node.hasattr("explanation"):
            attrs["title"] = node["explanation"]
        self.body.append(self.starttag(node, "abbr", "", **attrs))

    def depart_abbreviation(self, node):
        self.body.append("</abbr>")

    def visit_image(self, node):
        # set an empty alt if alt is not specified
        # avoids that alt is taken from src
        node["alt"] = node.get("alt", "")
        return HTMLTranslator.visit_image(self, node)


class PelicanHTML5Writer(Writer):
    def __init__(self):
        super().__init__()
        self.translator_class = PelicanHTMLTranslator


class FieldBodyTranslator(HTMLTranslator):
    def __init__(self, document):
        super().__init__(document)
        self.compact_p = None

    def astext(self):
        return "".join(self.body)

    def visit_field_body(self, node):
        pass

    def depart_field_body(self, node):
        pass


class HTML5RstReader(RstReader):
    writer_class = PelicanHTML5Writer
    field_body_translator_class = FieldBodyTranslator

    def _get_publisher(self, source_path):
        extra_params = {
            "initial_header_level": "2",
            "syntax_highlight": "short",
            "input_encoding": "utf-8",
            "exit_status_level": 2,
            "embed_stylesheet": False,
        }
        user_params = self.settings.get("DOCUTILS_SETTINGS")
        if user_params:
            extra_params.update(user_params)

        pub = docutils.core.Publisher(
            writer=self.writer_class(), destination_class=docutils.io.StringOutput
        )
        pub.set_components("standalone", "restructuredtext", "html")
        pub.process_programmatic_settings(None, extra_params, None)
        pub.set_source(source_path=source_path)
        pub.publish(enable_exit_status=True)
        return pub


def add_reader(readers):
    for ext in HTML5RstReader.file_extensions:
        readers.reader_classes[ext] = HTML5RstReader


def register():
    signals.readers_init.connect(add_reader)
