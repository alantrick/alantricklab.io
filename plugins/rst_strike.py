# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from docutils import nodes
from docutils.parsers.rst import roles


def htmlstrike_role(role, rawtext, text, lineno, inliner,
                    options={}, content=[]):
    """Strikethrough text

    Usage:

    I ate :strike:`five` one scoop of ice cream
    """
    return [nodes.raw(text='<s>', format='html'),
            nodes.inline(rawtext, text=text),
            nodes.raw(text='</s>', format='html')], []


def register():
    roles.register_canonical_role('strike', htmlstrike_role)
