==========================
 The North Ridge of Baker
==========================

:trip date: 2014 May 25
:party: Alan Trick, Clayton Matthews, Spartak Kirov, Mike Van Werkoven,
        Toby Stier (also included support team of Julio Velandia, Gema Rozo,
        and Dorothy Filpula)
:tags: Trip, Outdoors, 2014
:cover: /trips/2014/climbing_into_the_storm.jpg
:date: 2014-6-9


A few weeks ago, me and 4 other guys from the BC Mountaineering Club
headed up Mt. Baker to climb the North Ridge. Some hiker friends joined us
for the day on Saturday for the hike up to base camp and we were able to
persuade them to carry our ropes up for us. Apparently I have awesome friends.

Base camp was on a rocky bump at 1900 meters, a bit up and to climbers
left of the moraine camp. It seems like a pretty ideal spot if you want
to make this an overnight trip.

.. figure:: /trips/2014/bnr_base_camp.jpg
   :alt: bivy site on rocky outcropping

   Our campsite on a rocky outcrop around 1900 meters

The forecast was for a storm to come in on Sunday night, so we wanted to
get up and down as early as possible. We wake up at 23:00 and the sky is
completely clear and finally get going at 00:30. Crossing the Coleman
glacier wasn’t exceptionally difficult, but our route meandered a little
as worked our way around crevasses. Coming upon a large crevasse in the
darkness is an experience that I doubt will ever become boring.

We ended up a little high and ended up traversing straight across to
gain the ridge via the hourglass. We didn’t bother protecting the
traverse, and it was steeper than I had expected. I remember thinking
that it would be a bad place to slip, but nobody slipped and things went
smoothly.

.. figure:: /trips/2014/bnr_ice_cliff.jpg
   :alt: ice cliff

   The looming ice cliff

.. figure:: /trips/2014/rope_faffing_on_bnr.jpg
   :alt: playing with tangled rope

   So much for Beal’s anti-tangle packaging. Or maybe I just suck.

Once we gained the ridge, the steepness eased off and the snow was
punchy and plastic, making for an easy ascent to the ice cliff. We reach
the ice cliff just after sunrise (6am) and we can see clouds rolling in
from the other side of the summit. We build an anchor and the two
leaders start climbing. I was supposed to be one of them, but as I had
been finding out that morning, my Petzl strap-ons were simply refusing to
fit my ski boots properly and needed regular adjustments (leading to
some rather terrified and careful climbing). At this point our progress
slowed to a halt, over the hour or so that it took the leaders to climb
the cliff and set up an anchor, the snow began to fall and visibility
dropped to about 50 meters. After the pitch on the ice cliff we
simul-climbed the rest of the way to the summit.

.. figure:: /trips/2014/climbing_into_the_storm.jpg
   :alt: snowstorm

   Climbing into the storm

We finally arrived on the summit plateau at 12:00, and the weather which
had been getting increasingly worse was now a proper storm. It was a
lovely cocktail of snow, rime, and wind that made you hate your life
whenever you looked into the wind. I had thought this descent down the
Coleman-Deming would be a cakewalk, having done it 5 or so times before,
but I could only see about 10–20 meters in front of myself and there
were no tracks for the most part. Carefully following a map, compass,
and a GPS without tracks on it, worked for the most part, but navigating
around the crevasses was a little frustrating.

Half-way down the Coleman Glacier, I ran into the strangest sight: a
rope team hiking up the route, at 16:30 in a storm. I yelled at him,
and he yelled back at me and gave me advice on how to get around the
crevasse that was currently giving me difficulty. At first I wondered if
for some reason there were guides or rescue workers out looking for
people, but it turned out they were a group of Microsoft employees who
had gotten themselves tremendously turned around and lost. Exactly where they
had been, I wasn’t quite sure, but they had convinced themselves that they
had accidentally descended the Deming Glacier and were attempting
to go back to the Grant-Colfax col and return to their camp at the base
of the Coleman. Fortunately we got them sorted out and shortly afterward
we heard someone calling in the distance for the lost Microsoft team.
Our bedraggled team finally made it to camp at 18:00 and pack up and down
to the car at 20:30.

Gear Notes
==========

6 screws and 4 pickets per team, could have used more screws or more
efficient use of them.

Half-way between the ice cliff and the summit (about 2900 meters) we
dropped a picket, it’s probably somewhere down on the Coleman Glacier.
Finders keepers.

.. note:: This trip was originally reported on
          `CascadeClimbers.com <http://cascadeclimbers.com/forum/ubbthreads.php/topics/1125661/Re_TR_Mt_Baker_North_Ridge_5_2>`_.
