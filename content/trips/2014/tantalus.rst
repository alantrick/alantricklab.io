========================
 Tantalus Climbing Camp
========================

:trip date: 2014 July 1–6
:party: Alan Trick, Julio Velandia, Amir Khanlou, Chris Hill, Ivona Kellie,
    David Hiscock, Carl Douglas, Paul Geddes, Ian Ross, Philip Ng,
    Mandana Amiri, Benam Giwi, Monica Durigon, Gent Moo Khoo
:tags: Trip, Outdoors, 2014, Mountaineering
:cover: /trips/2014/tantalus_dione_top.jpg
:summary: Did some climbing in the Tantalus area with great food and fun
    people. Got a storm with it too.


I’d heard a lot of nice things about climbing camps, you get flown in, you get
all your food taken care of for you. It’s basically a holiday with climbing. So
I finally decided to splurge a little and spend the $500 or so to go to the
Tantalus Climbing Camp that Benam was organizing.

So, on July 1, we all load up into a helicopter and fly off to the Jim Halberl
Hut. That afternoon, we dorked around on the Northeast Spur of Seratus. It was
pretty clear there was a wide variety of experience and skill (I painstakingly
watched someone down-climb 25 degree snow face-in with two ice axes). Another
lesson I learned was: just because the guy below you says “Off rappel”, that
doesn’t mean they won’t yank hard on the rope shortly afterward.


July 2: The Northwest Buttress of Alpha Mountain
================================================

Alpha mountain is one of the dominant peaks in the range. It looked like it
might be quite a long walk, but the weather forecast was looking pretty good,
so we thought we might try it out. `Alpine Select` lists a few ways up, and
the Northwest Buttress seemed pretty direct from the where we were staying.
So around 4:00 we got up and walked our way down to about 1600 meters. The
walk down is pretty easy in the early summer, but may be tricky when the
snow gets low.

From the base of the buttress, you start meandering your way up. It doesn‘t
take too long before the climbing becomes 4th class, and then it mostly
just stays that way.

.. figure:: /trips/2014/tantalus_alpha_cowboy.jpg
   :alt: Julio siting on the rock

   Julio riding the “au cheval”

The description in `Alpine Select` goes something like:
“Climb up buttress, 4th class. Gendarme can be climbed or bypassed on right.
10–12 hours”. From the perspective of a lawyer, there’s nothing really
wrong about that description, but I’d say it’s rather malnourished. The
climbing is definitely “BC” 4th class, which would go as easy-5th most anywhere
else, and the main reason they probably bothered to call it 4th instead of 5th
is that the rock is so easy to break, and so smooth, that there’s not much
point in using a rope for most of the time.


The climb kept going and going. There were rappel anchors all over the top half
which implied that we were not the first ones who thought that this route was
a lot longer than it looked. After hours of working our way up the buttress,
we finally got to what seemed like the top, only to find out that it was a
sub-summit, and that there was a notch that cut deeply between this and the
true summit. Since the descent was from the true summit, we had to make our
way across, so we carefully climbed down to a little ledge where we made a
rappel anchor out of all matters of old tat & garbage pro.

.. figure:: /trips/2014/tantalus_alpha_notch.jpg
   :alt: Climbers on a small ledge

   Working on setting up the rappel into the notch


A little more walking after that and we were at the summit, the real one this
time. The descent (south face) was pretty straightforward: a bunch of gravel,
and a bunch of snow and then a bunch of steep dirt mixed with stream. Once
we got to the base of the peak, we walked the long way around Seratus and
back to the hut.


July 3: Dione
=============

Unfortunately, July 3rd was the last good day in the forecast. A bunch of
people had decided to go an climb Tantalus, but Julio & I were a but tired
from the lon day yesterday and figured it would be nicer to try something
easier like Dione. Over-all, there’s not a whole lot to say about this route.
The bergschrund wasn’t a problem for us, but I could see it becoming quite
interesting in the late summer. The climbing is mostly class 2 & 3.

The rock is *very* friable. Because the mountain isn’t steep, a lot of loose
rock just sits there, without falling down. One of our party dropped a
refrigerator sized chuck of rock down a gully on the Southwest side.
Fortunately, nobody was there. Safety & loose rock very important to pay
attention to on this peak.

.. figure:: /trips/2014/tantalus_dione_top.jpg
   :alt: Climbing along the south ridge of Dione

   Climbing along the ridge


July 4: Seratus
===============

Bad weather was coming in, so a few of us figured we’d take a short trip up
to Seratus and try to get back before the weather got bad. We went up the
Northwest Spur, which is not a route described in `Alpine Select`, but will
probably become the standard route in the near future. The route is a little
difficult at the bottom, but there are bolts along the way for rappels if
necessary. We got up and down in about 3 hours. There were dark clouds
everywhere, so the view wasn’t any good, but it was good exercise.

.. figure:: /trips/2014/tantalus_seratus_storm.jpg
   :alt: On the top of Seratus

   On the top of Seratus


July 5: Storm
=============

The rest of the day and the next day it just howled, rained, and snowed.
It was definitely nice to have the shelter of the hut, but being stuck in a
small, little place in the middle of nowhere without anything to do for a
while can get a little stressful. We were supposed to leave that afternoon,
but the weather was too bad for the chopper to come in. That night we debated
the pros and cons of trying to hike out ourselves.

Fortunately the weather the next morning was a little better, and we were able
to get quick ride out in the morning.


.. figure:: /trips/2014/tantalus_chopper_out.jpg
   :alt: A helicopter

   The chopper

