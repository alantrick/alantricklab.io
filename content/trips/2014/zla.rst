=====================================
 Skiing the Zupjock-Lama-Alpaca Trio
=====================================

:trip date: 2014 May 3
:party: Alan Trick, Dorothy Filpula, and someone I can’t remember.
:tags: Trip, Outdoors, 2014
:cover: /trips/2014/alpaca_south_ridge.jpg
:date: 2014-10-12

Since I’d never gotten around to making it up to this peak before, I decided
to subject my friends to this very wind-blown ridge on this rather windy day.
We started a bit after 9:00, and skied up behind a pair of snowshoers. The
snowshoers turned around at Zupjok, presumably on account of the wind.

We, however, were not as given to following common sense and continued on,
reaching the summit of Alpaca about 14:30. On the way back, the song in my
head was “This is the song that never ends”, replacing “song” with “ridge”
and “singing” with “skiing”. We got back to the parking lot around 18:30.

.. figure:: /trips/2014/alpaca_south_ridge.jpg
   :alt: skier looking back on windy ridge

   Alpaca’s south ridge in all its windy glory

Apart from the weather, the conditions we’re pretty good for skiing.
Everything is pretty well covered. With the long hours of daylight, now is
a great time to go bag this peak.

.. note:: This trip report was originally posted on Clubtread.