============================
 Baby Munday & Mount Stuart
============================

:trip date: 2014 September 27–28
:party: Alan Trick, Dorothy Filpula, ...
:tags: Trip, Outdoors, 2014, Mountaineering
:cover: /trips/2014/baby_munday_top.jpg
:summary: Really fun climbing & scrambling, and a pain-in-the-ass to get to.


The Approach
============

The approach to this area starts part way up the road to Mount Laughington.
The beginning of the road is an old, deactivated spur; mostly covered in
alder and annoying, but easy to walk along. Eventually, the road starts to
peter out, and a trail goes down to the right and crosses a creek.

From here the trail goes up, and I don’t just mean that there’s an incline.
It’s kind of like climbing a ladder, only instead of nicely fabricated rungs,
it’s made of dirt and branches. Expect to spend about an hour or so cursing
the day you were born.

Eventually, the trail breaks out into the sub-alpine, and steepness drops
significantly. Shortly after that, we set up camp on a nice little ledge.
There were blueberries all over the place.

Mount Stuart
============

.. figure:: /trips/2014/stuart_up.jpg
   :alt: Hikers on a steep slope

   Hiking up along the steep heather towards Mount Stuart. Baby Munday is the
   prominent peak in the background.

The first afternoon, we went to go climb Mount Stuart. Mount Stuart is the
further of the two peaks, but it’s easier to climb. We hiked our way up,
around some steep heather, and a big tarn, and then up a long talus slope.

.. figure:: /trips/2014/stuart_scramble.jpg
   :alt: Scrambling on the NW Ridge of Stuart

   Scrambling up the Northwest Ridge of Mount Stuart

We climbed Mount Stuart via the Northwest Ridge. I don’t remember it being
terribly difficult, but I do remember the huge drop on the east side—about
500 meteres, straight down to Waleach Glacier. This peak is probably easier
to climb in low visibility. After enjoying the summit for a bit, we descended
via the Southwest Ridge. This felt a bit tricky, but that was probably mostly
because we were going down.

The hike back to camp was easy, and we gorged ourselves on blueberries when
we got there.

Baby Munday
===========

The second day we climbed Baby Munday. It’s normally scrambled from the East,
and the scrambling route takes you to a sub-summit. We aimed to climb to the
actual summit.

It was a bit of work to get back up to the base again, but not too bad. We
worked our way up a little notch full of loose gravel. After that, we put on
ropes and climbed some easy 5th to little step that lead to the summit.

.. figure:: /trips/2014/baby_munday_dna.jpg
   :alt: Siting on top of Baby Munday

   Dorothy & Alan at the top of Baby Munday

The way down was a bunch of rappels, and crab walking down 5.2 rock. From the
bottom of the notch, we walked down to camp, ate more blueberries, and make
way out of there.

