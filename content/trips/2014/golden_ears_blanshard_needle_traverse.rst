=======================================
 Golden Ears–Blanshard Needle Traverse
=======================================

:trip date: 2014 July 26-27
:cover: /trips/2014/between_edge_and_blanshard.jpg
:tags: Trip, Outdoors, 2014
:party: Alan Trick, Andrew Brown, John Gill
:summary: A great summer adventure for any mountaineer/arborist.
:date: 2014-10-03

Me and two other BCMCers went
to attempt the Golden Ears–Blanshard Needle Traverse. As a matter of
caution I decided to do the trip in two days, which in retrospect is a
much saner option. On Saturday, we hiked up the West Canyon trail and
basked in the boiling sun. We had a nice, late start at about 9:30 and
got up to the hut around 14:00–14:30. We decided to climb Golden Ears that
afternoon and then returned to the shelter for the night. The shelter
has seen better days and there are currently a good number of mice in
the area. It would probably be best to camp a little distance away from
the shelter.

.. figure:: /trips/2014/robie_reid_sunrise.jpg
   :alt: sunrise

   Sunrise on Mt. Robie Reid

.. figure:: /trips/2014/edge_approach.jpg
   :alt: walking towards a mountain

   Approaching Edge Peak


On Sunday we wake up, pack our bags and head out about 6am. The hike
gets into scramble and bushwack territory pretty quickly. We scrambled
around a bit before we came up to a 15 meter-or-so vertical wall which
we assume is the one mentioned in the route description on this site.

.. figure:: /trips/2014/golden_ears_traverse_start.jpg
   :alt: scrambling

   Easy scrambling at the start. The harder climbing does not get any photos
   on account of being too runout.

I think we may have ended up slightly off-route at this point, since the
climbing was harder than I expected. Pitch 1 was a small, airy traverse
to the left onto a large platform (10m). Pitch 2 went up a ramp to the
right and back out on the face we previously looked at, and over the top
of the wall. Lots of hard-to-protect conglomerate (5.6+ PG13, 20m).
Pitch 3 climbed up another short wall via a nice chimney (easy 5th,
12m).

Once that was finished, the rout up the north-west side of Edge was easy
to pick out and we scrambled up it. Of particular note was the large
quantity of gravel and loose rock on the route.


The next bit heading south from
Edge has some superb scrambling. Unfortunately, all good things come to
an end (and on this route, with great speed). We shortly found ourselves
in a bushy, narrow, notch, with steep drops on both sides. It is unclear
whether a rappel down the gulley to the west would have worked here, but
we continued on. We did another 10 meter pitch climbing along and up
some exposed trees (there was a little rock too, honest!)

.. figure:: /trips/2014/bush_climbing.jpg
   :alt: climber wallowing in bush

   No animals were were harmed in the climbing of this pitch

From there we climbed the next bulge only to find ourselves in another
notch. From here we rappelled down the gully to the snow slopes to the
west. We took some time here to have a lunch break and melt some snow
for water.

From the snow slopes we got back onto the ridge and followed it until we
got to the top of a tiny, narrow fin just North of Blanshard Needle. We
ended up rappelling off that, straight down a steep forest to reach the
base of Blanshard Needle.

Instead of staying strictly on the North ridge of Blanshard Needle, we
tended a little to the West. The climbing here is a little more exposed,
but involves less bush. It should bee noted again that loose rock was a
real danger. We finally made it to the summit of Blanshard around 5pm.
From here we take a short break and begin the long trek back to the car
via Alouette & the Evans col.

.. figure:: /trips/2014/between_edge_and_blanshard.jpg
   :alt: climbing on the north side of Blanshard Needle

   Overview of the steep and bushy climbing along the ridge

Gear Notes
==========

I brought an ice ax and this turned out to be a bad idea. It wasn’t
useful and just got caught in all the bushes.
