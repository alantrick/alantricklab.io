=============================================
 Thar's North Face Couloir (right variation)
=============================================

:trip date: 2014 February 1
:party: Alan Trick, Julio Velandia, and Jason Porter
:cover: /trips/2014/tnf_couloir_bottom.jpg
:tags: Trip, Outdoors, 2014
:summary: A great couloir climb in the Coquihalla, I’d love to do it again.
:date: 2014-10-03

On Saturday, me, Julio Velandia (my climbing partner), and Jason Porter
climbed the North Face Coulior of Thar. It is an excellent line, and I don't
think it gets anywhere near the attention it deserves.

.. figure:: /trips/2014/tnf_couloir_bottom.jpg
   :alt: climbers in a couloir

   View from the bottom half of the couloir.

   The route straight up is the right variation, the left variation is
   hidden by the buttress to the left.

The climb starts up a very obvious gully from the far end of Falls Lake.
The start is not particularly difficult. Somewhere between 35° and 45° snow.
As you climb higher the slope gradually steepens and leads to a fork.
Other trip reports I've seen have gone to the left. We took the right which
goes somewhat steeper (maybe 55°). It contained a rather surprising moat
and even a little grade 2 ice at one point. We simulclimbed about 150 meters,
and topped out in glorious sunlight.


.. figure:: /trips/2014/munchies_on_thar.jpg
   :alt: me eating food

   Yours truly, having the munchies and wearings his red Michelin Man
   costume above the couloir.

It's not too often when the weather on the coast here is as miserable enough
to make you want plastic boots. Yesterday was one of those days. I think it
was about -20 degrees. A friend of mine reported -25 down by the highway that
evening. Keeping warm was quite difficult and fortunately we never had to stop
for more than about 10-15 minutes, even at belays. Still, Julio suffered from
a bit of frostnip on his toes, partly due to having his laces too tight.

Someone else has suggested a grade of AD. I'd probably suggest AD-.