=================
 A Scottish Tale
=================

:trip date: 2019 February 11
:party: Alan Trick, Mike VanWerkhoven
:grade: WI 3+/4-
:cover: /trips/2019/ast_upper.jpg
:summary: A new cold spell, a new climb. A Scottish Tale is a nice,
    long, and mostly easy ice climb; as long as you appreciate the
    thinner things in life
:tags: Trip, 2019, Outdoors

I think Mike had been hankering to climb A Scottish Tale (AST) for some
time now. AST is basically in Mike’s backyard, as long as you consider a
2 hour hike “your backyard”. He had been able to get Monday and Tuesday off
but a snowstorm was coming in in Monday night & Tuesday, and so we had to
go Monday. I guess I’d sort of forgot about the last time I climbed with
Mike on the cusp of a snowstorm [#f1]_. Our plan was to climb up from the
bottom, and then take a nice, leisurely gondola ride down, but when we
arrived at the gondola parking lot, we were told it wasn’t opening for the
day on account of the weather. We stayed optimistic.

The approach hike was pretty straightforward, the route is “lightly”
maintained, but it is fairly well flagged, so it’s fairly easy not to get lost.
The conditioned we traveled in were mostly okay, but some things were a little
slippery. The flagging cuts out at the very end, but at that point you’re right
at the base of the gully, and if it’s not obvious, that probably means it isn’t
formed. The guidebook suggests 90 minutes for the approach, but it took us
closer to 2 hours.


.. figure:: /trips/2019/ast_approach.jpg
   :alt: View of A Scottish Tale from below

   View of A Scottish Tale on the approach

The first few pitches of AST are pretty easy (like WI 2); however, they’re
mostly a bunch of thin ice that forms in runnels, and so while the angle is
only 45°, it’s pretty consistent, which makes it a real calf burner. This is
not good when you just had a long-ish climb 2 days before, and that was your
first in the season. We simul-climbed the first bit.

About 1/3rd of the way up you run into a pair of chimneys. These are sort of
a “classic” part of the route, roughly in the same way that boot camp is a
“classic” part of the military. In fact, I think it might be common practice
these days to rappel the top of the route and only climb the section above the
chimneys so as to not have to deal with them. The first chimney was
particularly thin and the bottom of wouldn’t even hold the weight of my body,
so I had to scrape myself up the ice with my arms.

.. figure:: /trips/2019/ast_thin.jpg
   :alt: Thin chimney on A Scottish Tale

   The thin chimney on A Scottish Tale


After the chimneys, the climbing opens up and becomes quite enjoyable. A long
pitch takes you to the base of the upper pitches, where the climbing steepens
significantly. Overall, the grade on the upper pitches is only about WI3+, but
it’s quite steep for the grade, making for great scenery, and a nice lovely
drop behind you. We climbed the upper pitches in 2.5 pitches. There are bolts
on the top bit, if you can find them.

.. figure:: /trips/2019/ast_upper.jpg
   :alt: Mike on the upper pitches

   Mike on the upper pitches

We made it to the top of the climb at about 17:00, about 6 hours after
stopping. I think a normal party could probably do it in 5 hours, but we
were slowed down by how weak and exhausted I was from climbing two days
before [#f2]_. The views from the top are great, as you’re a bit above the
Chief and have a great view of the Squamish Valley.

.. figure:: /trips/2019/ast_top.jpg
   :alt: Mike topping out

   Mike topping out

The top out is right by the Chief Overlook Viewpoint in the Sea-to-Sky
Gondola’s alpine trails. This means you just waddle up snow for a few minutes
and then you’re on a nice, flat trail that takes you straight to the gondola.
Unfortunately, as previously mentioned, it was closed; so we had to hike down.
Also unfortunately, we tried taking the Skid Trail, as it was steeper and more
direct. It was also covered with ice and poorly maintained. In the dark, we
ended up loosing the trail, and then had to bushwhack and rappel down to the
main trail. We were a little late.

.. [#f1] `Baker North Ridge`_ (it did not go as planned).
.. [#f2] I would personally also like to follow the parental tradition of
         blaming one’s faults on one’s offspring and say that it was because
         of my “dad bod”.

.. _Baker North Ridge: /trips/2014/baker_north_ridge

