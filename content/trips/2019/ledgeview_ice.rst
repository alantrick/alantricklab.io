===============
 Ledgeview Ice
===============

:trip date: 2019 February 19
:party: Alan Trick, Dorothy Trick
:grade: WI 2
:cover: /trips/2019/ledgeview_broken_heart_climbing.jpg
:summary: Found some easy ice on Ledgeview. Our first climb as new parents
    and possible first ascent.
:tags: Trip, 2019, Outdoors

On Monday, Dorothy and Timothy and I went on a little hike at Ledgeview
(McKee Peak). I was trying to find the bottom of a trail called Grandpa’s
Eyebrows. We eventually did, and walked up the icy trail as far as we were
comfortable. On the trail, however, we spotted some short
easy sections of nice ice. So we quickly made babysitting plans [#f1]_ and came
back the next day and quickly raced up them. I haven’t heard of these climbs
before, so it’s possible it’s a first ascent, as such, we bothered to name them.

Normally, climbs of this size would not be worthy of much note, but their
proximity to Abbotsford, and the quick approach make them a worthwhile
objective when you’re short on time. Overall, the approach and climb takes
about 1 hour if you solo, 2 if you don’t.

The approach is quick. Park at the north access to McKee Peak (on McKee Road,
just across from Golf Course Drive). Hike up the road. A little after it
crosses a large stream, turn left onto Mixed Bag, cross the stream again, and
immediately after, turn right and go upwards. This is Grandpa’s Eyebrows. Hike
up this trail a few hundred and the ice should be visible in gullies above you.
There are two obvious patches of ice, and a small little flow in between that
is not worth the slightest attention.

The terrain is easy enough that should shouldn’t need to anchor a bottom,
belayer. A top belayer can use some trees for anchors, but they’re a little set
back, so it’s nicest to extend the belay a little with the rope.

Broken Heart (WI 2, 7 m)
------------------------

The eastern climb forms in two parts, and it looks like a broken heart when
viewed straight on. The climbing is pretty straightforward. The left portion
of the climb has more interesting features, and could be made a little harder
if you tried.


.. figure:: /trips/2019/ledgeview_broken_heart.jpg
   :alt: The Broken Heart ice climb

   View of the climb from the side

Valentine (WI 2, 10 m)
----------------------

This v-shaped climb is a little longer, and there’s a tree sticking its
branches out in the middle that get into your way.

.. figure:: /trips/2019/ledgeview_valentine.jpg
   :alt: The Valentine ice climb

   Me climbing up the ice

.. rubric:: Footnotes

.. [#f1] Many thanks to my Mom for taking care of Timothy at the last moment.


