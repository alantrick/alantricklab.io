==========================================
 Thar’s West Ridge Couloir (First Ascent)
==========================================

:date: 2015-3-9
:trip date: 2015 March 7
:party: Alan Trick and Mike VanWerkhoven
:grade: AD+
:cover: /trips/2015/thar_west_ridge_topo.jpeg
:tags: Trip, 2014

Summary: I found this route incredibly fun. While mostly moderate
climbing, the short steps add variety, and a great sense of adventure.
Be careful with conditions, an avalanche on this route would be very
bad.

Last year, when I first came to this area, I made a
vain attempt to climb this route, only to be turned away at the very
bottom by an impassable wall of sugary powder snow. In the end, we went
and `climbed the North Face Couloir
instead </trips/2014/thar_north_face_couloir_right>`_, and much fun was
had; but I did not forget my defeat. This year, with the wonderful
mixture of a spring snowpack and winter temperatures, I figured that I
owed this route another attempt. Mike and I drove up on a beautiful
Saturday morning to test our luck.

The hike in went quickly. The trail to Falls Lake is very icy right now,
so there was a little excitement, but fortunately not too much. Once I
got to the lake, there were some fishermen already there and they told
me the lake had 60cm of ice. However, what the frozen lake didn’t have
much of was snow; and so we ended up half walking, half sliding are way
across to the other side. On the other side we took a break, scoped out
our route and went for it.

.. figure:: /trips/2015/thar_west_ridge_topo.jpeg
   :alt: photo of thar’s west ridge with route line drawn on it

   Route topo (photo by Mike)

We climbed up some moderately steep
snow to get straight in the base of the couloir. Mike took the fist lead
up an ice ramp to the first step, which lead to the wall I had met last
time. The bad news is that it was still a pile of sugary snow, but this
time, instead of just snow, there was also bits of ice and rock in
there. I spent 10 minutes of faffing about, clearing away snow, looking
around at all the options. Then a lieback on a column of ice, and a bit
of gymnastics, and it was in the bag.

.. figure:: /trips/2015/thar_west_ridge_p1.jpg
   :alt: climbing on steep snow

   First pitch, the first step is just out of view.

Following this, there is some easier (45° snow)
leading to 50–70° snow and ice for most of the climb. Snow quality &
protection was highly variable. There was lots of styrofoam, and lots of
soft snow, often within a few meters of space. Pickets placements were
the most frequent and generally good, but there were also a decent ice
screw placements and even several cracks that accepted cams nicely
(mostly .75 or .1/.2-ish).

Near the finish, the quality and quality of the snow diminished, and the
protection was more sparse. On the second to last pitch, Mike’s first
three pieces of gear were a 10 cm screw, a tied off 13 cm screw, and
then a sling around a small bush that could have passed for a bonsai
tree.

.. figure:: /trips/2015/thar_west_ridge_interesting.jpg
   :alt: climbing on steep snow

   Marginal protection near end

The top of the couloir was mostly guarded by a cornice, but the left
side was wide open. After a careful traverse to left, we topped out into
the glorious sun. From there we walked over to the old avalanche path
below Nak and hiked down to the Pipeline Road and back to the cars.

.. figure:: /trips/2015/thar_west_ridge_topout.jpg
   :alt: coming out of the top of the couloir

   Topout

Gear Notes
==========

As I already mentioned, some cams/pins may be useful on this
route, ice screws as well depending on conditions. Pickets are
definitely recommended, but don’t expect the snow to be of superb
quality. We brought 2 pickets, and more would have been nice. There are
a few trees slightly off-route which can make for solid anchors.
