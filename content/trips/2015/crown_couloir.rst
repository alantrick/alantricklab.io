===============
 Crown Couloir
===============

:date: 2015-3-2
:trip date: 2015 March 1
:party: Alan Trick, Mike VanWerkhoven
:grade: M3- 40° snow
:cover: /trips/2015/snow_machines.jpg
:tags: Trip, Outdoors, 2014

With all the disappointment that this winter has brought for skiers
there is one upside: mountaineering rocks. None of that soul-killing
wallowing in powdery snow, all the trails are bare, and all the climbs
are either dry rock or hard snow and ice. It’s pretty much like the
summer, but less sweating and less slush. Small slivers of snow are
often strong enough to support body weight right now.

.. figure:: /trips/2015/snow_machines.jpg
   :alt: snow machines running

   Snow machines working hard on Grouse mountain. Is there still hope
   for skiers?

After having been shut down on Brunswick Mountain last weekend, I was itching
to do some real mountaineering. My buddy Mike had suggested climbing Crown
Couloir and I eagerly joined. Being lazy and wanting to sleep in we
decided to take the gondola up Grouse. Arriving at the parking lot
around 8:20, we made it to the top of the Gondola at 8:45 and began
hiking out to Crown Pass. After Dam Mountain, the trail became snowy and
icy, and crampons might have been a good idea, but we did without and
were fine.

Things were fairly easy until the descent into Crown Pass. The route
isn’t hard, but the slippery snow certainly kept us on our toes, and
further down there are several body lengths of class 2 climbing down
rock which currently has bits of ice on it. Then we reached Crown Pass.

From here you leave the main trail and hike down into the top of
the Hanes Valley area. I had read that this descent was particularly
disliked. I actually found the top to be quite nice. The talus was suck
together quite well and there was even a flagged route! Further down,
however the rock quality deteriorates and the rocks become a lot more
mobile.

At one point I caught my boot on something and fell forward, down the
rocky slope. I managed to turn my fall into a somersault and rolled over
my head and onto my backpack. A few “crap”s were uttered as I assessed
the situation. As it turn out, the roll had spared my head from any
blunt trauma and I had managed come out with only several scrapes on my
face, a cut lip, a few bruises here and there, and a few more holes in
my pants. I put on my helmet and we continued on.

.. figure:: /trips/2015/crown_face.jpg
   :alt: face with cuts

   My face has seen better days

Near the bottom of the talus, you
veer off to the left and go through some thorny bushes. The bushes
really aren’t very nice (leather gloves would be a nice thing to have on
here) but it goes quickly. This is is the low point of the trip
(literally). After this, you round the South East buttress of Crown and
begin hiking up a boulder field. The boulder field makes for fairly easy
and enjoyable hiking if you take the right path. Some of it is crumbly
and dicey but it can be easily avoided.

.. figure:: /trips/2015/lower_crown_couloir.jpg
   :alt: snow and rocks in a colouir

   A view of the easy mixed climbing in the lower couloir

Eventually you reach the couloir. Currently there is a large deposit of
snow at the bottom from an old avalanche, this doesn’t last long, so it
is best avoided by hiking up the rock on the side. A little bit later
the snow starts again, and here you can either stay on the rocks or the
snow. Eventually the couloir walls close in on you and you will be
forced on to the snow. Putting crampons on before this point is highly
recommended. We didn’t and ended up with some pretty awkward climbing on
icy snow.

.. figure:: /trips/2015/crown_crampon.jpg
   :alt: Mike putting on crampons

   Mike finally puts his crampons on after stubbornly chopping steps for
   ten minutes.

The crux is about halfway up. It is
quite short, but involves stemming between some thin face on the right
and intermittent snow on the left. After this, the route goes right.
Another party who was ahead of us had chosen go to the left here and
were busy belaying each other on some mid-5th and narrow snow ledges.
From here the climbing is not very hard, and mostly fun (M2–3, leading
to 30 degree snow). The top of the couloir puts you just at the base of
the summit block, and at about 14:10 we had reached the summit.

.. figure:: /trips/2015/crown_summit.jpg
   :alt: Mike on the summit

.. figure:: /trips/2015/crater_slabs_pillow.jpg
   :alt: pillow on crater slabs

   We spotted a pillow on crater slabs. Apparently someone had taken a
   snooze there and forgotten their pillow.

Just before we left the summit, we made contact with the other climbers
by yelling across the mountain again (sound travels very well around
there). They had been moving pretty slowly for the last 2 hours and
weren’t that far above where they were before. We asked them if they
were okay and they said yes, so we sauntered off down the regular trail.
I guess they were a little bit too optimistic, because the next day I
heard about them in the new.

On our way down we found some of our friends who had been climbing Crown
Buttress. One of them had fallen and was immobile with a sprained ankle.
Search and Rescue was on the way. We chatted for a while and continued
down.

Post trip
=========

The guy with the sprained ankle got long-lined out by a
helicopter and as far as I know is doing well. Once I got home I got a
call from another friend saying that Search and Rescue was looking for
those other climbers. I called Mike Danks and gave him all the info I
knew about their location, and a photo of the last ledge we saw them on.
The next day I heard about them on the news. I don’t know how their
rescue went, but it must have been pretty harrowing.

Gear Notes
==========

We brought twin ropes and some rock gear. It ended up
staying in our packs. Unless you are very confident with your snow
climbing abilities, you should bring pickets and maybe ice screws, but
large parts of the route may be impossible to protect because the
snow/ice just isn’t thick enough.
