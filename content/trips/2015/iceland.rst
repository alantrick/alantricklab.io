======================
 Honeymoon in Iceland
======================

:date: 2015-9-4
:trip date: 2015 August 22–September 1
:party: Dorothy Trick, Alan Trick
:cover: /trips/2015/sudhurland_sunrise.jpg
:tags: Trip, 2014


.. epigraph:: “Blow, blow, thou winter wind”—Lord Amiens

August 22
=========

We arrived at the Keflavik airport at 7:00. The weather was overcast
with a brisk breeze. Little did we know that the breeze is unusual in
Iceland. As we learned a few days later, it is usually more of a gale.

We were picked up by a person from Sadcars where we were driven to their
place, stood in line for a while, and eventually got a car. A little bit
later, we were all packed into a little Hyundai and I was trying to
remember how to drive a stick-shift while we made our way along the
little Icelandic roads.

Our first goal was to try to get some stove fuel and some food. The
first grocery store we stopped at didn't open till 10:00. A little
weird, but maybe that was just because it was rural. Then we tried a
shopping center in the south end of Reykjavik. It didn't open till
11:00! Great, do people in Iceland even realize that the sun rises
earlier in the summer and they can stop sleeping in? We went on our way,
hoping to find food somewhere before we starve to death in this new
land.

Due to the time change we had more-or-less completely lost a night if
sleep, and a noisy uncomfortably airplane ride had not done anything to
help that. As it was, we spent the rest of the day slowly driving
towards Skaftafell, stopping every hour or two to pull off and have a
nap on the side of the road. Eventually we found some food, and even
some stove fuel. Life was good. We got to the campsite about 17:00, and
got our tent setup just before it started raining. We ate a quick dinner
of ramen noodles, tuna, and lettuce; and settled into our cozy sleeping
bags for the night.

August 23
=========

Our plan today had been to climb Hvannadalshnúkur; however, when we got
up in the morning to leave at 6:00, we found that the campsite was
gated. Shortly after that, it began to rain, so we crawled back into our
little tent and slept a bit longer. Due to being unable to find a way
out of the campsite, we didn't really have enough time for our original
plans, so we decided to try climbing Kristínartindar instead.

We headed up the Lambhagi trail, stopping shortly by a few waterfalls along Stórilækur
creek and an old hydroelectric building. At first we were all alone, but
after a few drops the lower trails were crowded with people. Once we
left the lower trails though, no one followed, and we on our own again.
After this the trail ascends through some heather. Part way through it
started to rain on us, and the wind picked up. By the time we reached
the good viewpoints, it was impossible to see anything. We bailed on
climbing Kristínartindar and just ended up hiking around
Skaftafellsheiði. On the way out, we went down the Austurbrekkur trail,
but contrary to the advertisement of the Vatnajökull park website, we
saw no elves.

.. figure:: /trips/2015/skaftafell_selfie.jpg
   :alt: Photo of Dorothy & Alan

   Us at the beginning of our hike (notice how we are not drenched)

.. figure:: /trips/2015/svartifoss.jpg
   :alt: The Black Falls (Svartifoss)

.. figure:: /trips/2015/austurbrekkur.jpg
   :alt: Hiking down through small trees

   Descending the Austurbrekkur trail (notice how there are no elves
   in this photo)

Once we were back at the campsite, I inquired about the gate, since we
needed to leave early the next morning. Turns out you just need to drive
up to it, and it opens automatically from a sensor.

After that, we went and had some burgers at the nearby shop in Freysnes,
and then drove to Jökulsárlón: the lake where Vatnajökull meets the ocean, and
breaks off into icebergs. On the way back, we took photos of indignant sheep
showing us their backsides.

.. figure:: /trips/2015/joekulsarlon.jpg
   :alt: Glacier runnoff lake

.. figure:: /trips/2015/sheep.jpg
   :alt: Sheep butts

August 24
=========

Today we woke up around 4:30, maybe still a little jet lagged. With our
new-found knowledge on how to escape the campsite, we packed up and
leave for the canyon of Fjaðrárgljúfur. Both of us decided that the
vegan “cheese” paste was really no good at all, and the mushrooms
smelled funny, so we were left with eating a bunch of tomatoes.

.. figure:: /trips/2015/sudhurland_sunrise.jpg
   :alt: sunrise

   Sunrise

We went to Fjaðrárgljúfur. Like much of Iceland, this also looks like
something from a Dr Seuss book—only more clouds and less sun. Fjaðrárgljúfur
is full of brown clay/sandstone towers covered by tuffs of green dirt.

.. figure:: /trips/2015/fjadhrargljufur.jpg
   :alt: Green canyon

After this we drove to Vik, got gas, and eventually found an open for
place and had a most delicious pastry. Our goal here was to visit , and
find a few puffins. What we found was a lot of wind, a bunch of rain,
and a few puffins. We took some pictures and went on to our next stop:
Vestmannaeyjar.

.. figure:: /trips/2015/dyrholaey.jpg
   :alt: Dorothy in the wind

   Trying to watch puffins in the blustery wind

Vestmannaeyjar
--------------

From Landeyjahöfn there is a ferry to a cute group of islands called
Vestmannaeyjar. A beautiful place to visit, but maybe not the best place
to live since the small island's few volcano seem to have a habit of
erupting.

Our first destination on this island was a step hill (really mostly a
big bunch of cliffs) called Heimaklettur. Heimaklettur is an Icelandic
term meaning “slipping on sheep shit”—not really, but it should be,
since avoiding this is the most difficult part of the climb.
Heimaklettur is steep and, from a distance, the existence of any trail
seems improbable; but a few ladders at the base of its western cliffs
reach a lookout, and from there the narrow tail climbs up a steep grassy
slope. The difficulty of the hike does a good job of keeping humans
away, and a good number of sheep and puffins make their home there.

.. figure:: /trips/2015/heimaklettur_ridge.jpg
   :alt: descending the ridge in a fog

.. figure:: /trips/2015/heimaklettur_sheep.jpg
   :alt: sheep the steep slope

.. figure:: /trips/2015/heimaklettur_puffins.jpg
   :alt: puffins on a little rocky knob

Once we got down from there, we proceeded to look for food
establishments. Found a bunch of expensive ones, also found some unusual
puffin artwork. Eventually we settled on a cute little place named Gott,
ate fish soup, and then dragged our tired bodies back to the ferry.

.. figure:: /trips/2015/heimaklettur.jpg
   :alt: Selfie photo in nice weather

   And suddenly the weather got really nice for about 4 hours

That night we camped in Skógar. We couldn't find anyone to take our
money and ended up camping for free, which was fine for us because the
campsite was really not very nice. Also, it was windy that night—
really windy.

August 25
=========

.. figure:: /trips/2015/skogafoss.jpg
   :alt: skogafoss

   Dorothy next to Skógafoss

Fortunately we woke up alive, and the tent was still okay. Neither of us
had gotten a great sleep because of all the noise that the wind made. We
packed up our tent, which was an interesting endeavor. The inside had
collected some dirt and grass and needed emptying, so I hoisted it up,
to shake it out, and nearly got carried away like a paraglider.

From there we went to the museum in Skógar for coffee and pastries and
then headed of to go swimming at Seljavallalaug. Seljavallalaug is an
old outdoor swimming pool located in a valley of the south side of
Eyjafjallajökull. The scenery in the area is tremendous. The pool is
warmed up by water pipped in from nearby hot springs. The result is a
pool that is about 30–32°C, and a bit warmer if you huddle around the
water that is pipped in.

After swimming for a bit, we set off to go visit some legitimate tourist
sites: Gullfoss and Geysir. Both of them are located in the highlands
south of Langjökull, and when driving up route 30, you can see the vast
expanse of glacier. Gullfoss is a big waterfall, kind of like Niagra falls.
Geysir is a big geyser; but unless you go there during, or shortly after
an earthquake, it's not very interesting because it doesn't like to
erupt. There's another nearby geyser named Strokkur, however, which erupts
every 5–10 minutes or so, so we joined the crowds around it and watched it
erupt a few times.


.. figure:: /trips/2015/gullfoss.jpg
   :alt: Gullfoss

   Gullfoss

.. figure:: /trips/2015/strokkur.jpg
   :alt: Strokkur

   Strokkur


After that, we had to figure out where to stay. We'd seen a sign for a
campsite with a pool nearby on our way up, which seemed really nice, but
it was down in Brautarholt, which was out of our way now. We set out to
see if we could find another such place. The first place that our GPS
found was Laugarvatn, but the campsite area there looked closed, and the
main swimming place looked fancy and over-priced. The next place we
found was the town of Borg í Grímsnesi. The campsite there was almost
completely empty, and again, we were unable to find somebody to pay.

The pool there was very nice. We'd never been to a public pool in
Iceland before, but they all have hot tubs, and multiple ones at at
different temperatures. You have to wash yourself quite thoroughly
before entering, so the hot tubs don't have to be so full of chemicals.
We relaxed there for a while before going back to our tent to have a
yummy dinner of rice and a spicy goulash.

August 26
=========

The next morning we got up and made some breakfast and coffee, and
headed off for Þingvellir, or Thingvellir when anglicised. This is the
place where the AlÞing (“all-thing”), [#f1]_ or the Icelandic Parliament,
meet back in the early days of Iceland. Þingvellir is kind of interesting.
There is a cute little church, and a fancy cross-shaped cemetery; though
there are only two graves in it, they seem to be having trouble finding
occupants. Also, there's some neat-looking cliffs that I wished I could
climb.

From there, we headed toward Akranes. On the way there, we drove through
this big long tunnel underneath Hvalfjörður, it was about 6 km long. We
were going to climb up Akrafjall, but ended up being lazy and wanting to
go to our hotel early, so we went to go visit the Glymur waterfall
instead.

Glymur is a a really tall waterfall, nestled in a canyon at the end of
the Bostnsdalur valley. Visiting it involves about an hour long hike. Of
course, such is our luck, that after about 40 minutes of hiking up, it
starts raining on us. We raced up to the viewpoint, took a few photos
and raced down. By the time we were back at the car though, we were
pretty drenched, and very happy that we had booked a hotel for this
evening.

.. figure:: /trips/2015/glymur.jpg
   :alt: Glymur

   Glymur


We drove up to Hraunsnef where our hotel was and got there about 17:00.
We settled in for the evening with some cracker/tuna/lettuce snacks for
dinner, and lots of hot chocolate. That night we could hear the wind
howling outside, and the cars were bouncing a little from the gusts of
wind.

August 27
=========

After a very nice sleep, we woke up and ate a breakfast buffet at the
hotel restaurant. It was probably over-priced, but we were hungry, so it
tasted good. From there, we hopped in our car and drove to Snaefellsnes
to check things out. We first stopped in Grundarfjorður for groceries
and gas, and then went to the tourist information place in Ólafsvík to
get advice on the weather and the condition of route 570, the road up to
Snaefellsjökull (a mountain we were hoping to climb).

Unfortunately, the weather forecast wasn't that promising. Given that
the weather was going to be mediocre anyway, we figured we'd just go
stay in our car and check out route 570. We were actually able to drive
up quite high. We drove back down the mountain and around the other end
of the peninsula to Arnarstapi.

We had originally thought we might want to rent bikes in Arnarstapi, but
it was windy enough that if you tried to get on a bike, you'd probably
just be knocked straight off. We walked around for a bit and headed back
to Ólafsvík for our favorite activity: siting in hot tubs. We went to
the pool there and chatted with a bunch of Albanian soccer players who
where there for some national championship. They though Ólafsvík was the
most boring place in the world because there were no good bars. After
that, we went to the campsite, which was really nice and had a small
building with an indoor cooking area. Unfortunately, our dinner of pasta
and meatballs was miserable. The pasta was fine, but the canned
meatballs reminded me of that barely edible protein-paste that they have
in sci-fi shows.

August 28
=========

Since Snaefellsjökull is not a terribly long climb, and since we knew
the weather was not going to be any better in the morning, we slept in a
little. We got up to pack up camp and had breakfast. We drove up and got
to the end of the road, but unfortunately the weather wasn't any better.
We walked about for a bit to check things out, but in the end we decided
to bail due to high winds and poor visibility. We drove down a bit and
hiked up some small 495 meter peak as a consolation before driving back
to Ólafsvík, and then to Grundarfjorður.

I had been wanting to climb Kirkjufell, since it looked so cool. [#f2]_ We
had the whole afternoon free, so while we were a little unsure about the
weather, we decided to give it a try anyways.

The first difficulty is they very short approach from the road to the
base of the south ridge which also happens to be somebody's sheep farm.
Based on a little bit of research I did, it seemed that what we might
call trespassing is completely acceptable in these parts. The farm is
surrounded by a short wire fence, and based on our complete lack of
experience with electrical fences, we were a little concerned that it
might be electrical. I gingerly stepped over it, and Dorothy
successfully jumped, and we rambled up through the meadows.

At least by the way I went, there is no defined path at the beginning,
by the time you reach the first cliff band, there is a faint, definable
path. The climbing is very fun. The trail meanders up between various
weaknesses in the cliff bands that surround the peak: starting with easy
sections of class 2 (YDS), and progressing to several steps of class 3
climbing, with the final class 4 step right before the summit ridge.
Each of the class 3/4 steps has a fixed rope on it, but these ropes
look they are as old as the Vikings, so they're really only good as
“physiological pro”, climber be ware!

.. figure:: /trips/2015/kirkjufell_edge.jpg
   :alt: Walking around the cliffs

   Climbing around the cliffs that guard the mountain

.. figure:: /trips/2015/kirkjufell_ropes.jpg
   :alt: old ropes

   Yeah ... about these ropes

.. figure:: /trips/2015/kirkjufell_crux.jpg
   :alt: The crux

   The crux right before the summit

The start of the summit ridge is a narrow, only a few meters wide. It's
covered with grass, but I don't know what the beneath the grass there,
because I could actually feel vibrations in the ground from Dorothy
walking nearby. We walked over to the summit, took a few photos, and
made our way down. On the way back we both carefully stepped over the
(probably-not-electric) fence, and hopped back in our car.


We drove back to Grundarfjorður to stuff our faces with victory ice
cream and then started our drive back to Reykjavik. We were hoping to
relax in a hot pool near Skjálg, but when we got out of the car, the
wind was so bad, we could barely walk without falling over; so we
continued on to find a campsite with a pool attached. We decided to go
to Hlaðir, which to our surprise is actually a museum with a pool and a
campsite attached. The wind there was even worse, which lead me to
sprint after a glove and a bag of lettuce that got inadvertently
dropped. We gave up on the pool idea, since it was outside, but
fortunately there was a little embankment that we could hide our tent
behind which kept it mostly protected from the wind. We were quite happy
this was our last night in a tent.

.. figure:: /trips/2015/hlaðir_camp.jpg
   :alt: View from Hlaðir

   Hlaðir: our last campsite in Iceland

August 29
=========

We woke up again at the usual time of 8:00, made ourselves some coffee,
and hopped in the car to head to Reykjavik. When we arrived at the city
outskirts, it was only about 10:00, so we went to one of the city pools
(Árbæjarlaug) to go hot tubing again. When we got out of our car, we
noticed it was surprisingly quiet—the wind was gone! In fact, it was
almost sunglasses weather!

After a nice swim we picked up food at a grocery store and had a little
picnic in a nearby park before heading down to Smáralind to check out
our first Icelandic shopping mall. We checked out all the various shops,
before settling on some more delicious ice cream at the Ísbúðin.

From there we when to the apartment that we had rented via Airbnb, and
dropped off our rental car. The apartment had no wifi, and for some
reason we couldn't get a DHCP address from the wifi in the bar
downstairs. There was an adjacent restaurant name Gló, however, that had
wifi as long as you had an access code. We figured we'd go over there
and order a coffee to get wifi. To our surprise, it was actually a raw
vegan restaurant, and we ended up having some surprisingly delicious
mushroom soup instead.

August 30
=========


Today Dorothy's relatives gave us a wonderful tour around Reykjanes.
Meeting up was a little complicated, because we had no Internet
connection and no way to hear back from them. Eventually, they showed
up, and everything was good. We drove to Sad Cars and picked up my
passport, and then to the base of Helgafell to go hiking. We met up with
four more relatives there, and each one of them greeted us with “Hi, did
you find your passport okay?” I guess news travels fast in this family.

Helgafell is a little mountain outside of Reykjavik, Gummi explained to
us that the mountain was formed by a subglacial eruption. Much of it is
composed of a sort of sandstone slab, and find places have rocks buried
in the sandstone. The mountain seems quite popular, almost like the
Grouse Grind, but less grind.

.. figure:: /trips/2015/iceland_family.jpg
   :alt: picture of relatives on a big hill

   Icelandic relatives on Helgafell

After the hike, we went to Binna's house and we're treated to a
delicious dinner: rice and some peanut satay with lettuce and yummy
other salad things. Sigga's grandson, Óskar, tried to teach us
Icelandic.

After the meal, Binna and Sigga took us on a tour of the Reykjanes
Peninsula. First we visited Grindavík, where they used to work in the
fish factories. Being on the south coast, Grindavík gets a lot of bad
storms, and you drive up right beside old shipwrecks if you take the
road that goes around Hópsnes.

.. figure:: /trips/2015/reykjanes_shipwreck.jpg
   :alt: Shipwreck

   Shipwreck near Hópsnes

We also drove by a large radio tower own by the Americans. Despite its
strategic location in the middle of the Atlantic., Iceland apparently
has no military—at all. The US used to have some military bases in
the country, but they pulled out about 10 years ago. Now all that is
left is a few radio towers to look out for Russian submarines.

.. figure:: /trips/2015/gunnuhver_bridge.jpg
   :alt: destroyed bridge in steam

   That's where the bridge used to go

After that, we went to the south west of end of the peninsula,
Reykjnestá. We saw some more hot springs and saw a statue of a Great
Awk—an Icelandic version of the Dodo Bird. It was large and flightless
and could only waddle around; so it made for an easy meal when humans
first arrived in Iceland, and didn't last very long.

After that, Binna and Sigga took us out for dinner to the Salthúsið
restaurant in Grindavík. Dorothy had fish, and I had lamb chops.
Traditional Icelandic food involves a lot of meat, very yummy.

August 31
=========

The Blue Lagoon
---------------

Today we woke up at the early hour of 7:15 to prepare for our arduous
task of visiting the Blue Lagoon. Almost every place in Iceland refers
to itself by its Icelandic name, but the Blue Lagoon (Bláa Lónið in
Icelandic), exclusively refers to itself by the English translation.
Pretty much all the signs are just in English too. If you want to forget
you're in Iceland. This is the place to go.

The first task was to go to the bakery and pick up a large pastry, since
you aren't allowed to eat you own food at the Blue Lagoon, we needed
some calories to munch on as soon as we left. Then, we went to look for
our bus, which was a little confusing because we had unwittingly booked
our tickets for a road which didn't also allow vehicle traffic, so the
bus ended up wobbling around in a nearby intersection until we found it.
Also, the bus was supposed to have Wi-Fi, but it didn't. It did,
however, get us to our destination alive, so that's nice.

.. figure:: /trips/2015/blue_lagoon.jpg
   :alt: selfie at blue lagoon

   Obligatory selfie at the Blue Lagoon

The Blue Lagoon is basically a luxury hot pool. It's shtick is that
there is a lot of silica in the water there, and this gives it an
unusual light-blue color. It also leaves a smooth and slippery,
porcelain-like, coating on the volcanic rock there, which would
otherwise be very sharp and abrasive. There's a fair bit of sand on the
bottom too, which feels nice on your feet. It also leaves residue on the
body which decreased the skin's permeability. Apparently this amounts to
“healing powers”.

The place is quite scenic, but it is also quite crowded. The pool
entrance area smelled funny for some reason, but as long as you don't
think too much about what other people might be doing underneath the
murky water, is fine. Many people look like clowns there, because they
put the silica on their face. This can be very entertaining to watch,
particularly when it's applied unevenly. I snuck in a chocolate bar
because I expected food prices there to be highway robbery, and they
were.

Biking Reykjavik
----------------

After eating some proper food, we walked over to a nearby bike shop
(Borgarhjól) and rented bikes. It was a little pricey (about $40 for
each bike for 24 hours), but definitely worth it. Reykjavik has a number
of nice bike paths, and many of the sidewalks/walking paths are large
and accommodate bicycle traffic well.

We first went up to Hallgrímskirkja, the iconic church in the heart of
the city. It sports a really large organ which would be amazing to hear,
but nobody was there to play it. Then we went up to Perlan, a big glass
dome on top of a hill. We didn't know what it was and thought it might
be interesting, but it turns out it was just a restaurant. We did,
however, get a photo with some dancing metal figures.

We went down by the university and then through the park in
Fossvogsdalur. The people of Reykjavik really know how to make nice
parks. There were nice paths, a complete frisbee golf course, a bunch of
sports fields, play grounds, and even a small neglected climbing
structure with plastic holds that were prone to spinning.

We continued along to Elliðaár, and then down to Mjóddin. I had noticed
an “apparel store” on my map there, and Dorothy had been wanting to look
for a scarf. It was only when when got there, that I realized that the
store was named “Prinsessan”, and that it specialized in selling
princess dresses to little girls.

On our way back, we stopped at another mall. Kringlan, and got a cute
ear warmer at the Hagkaup instead. Success. Then we went down along the
sea wall to the harbor, where we ate ice cream at Valdís—a good way
to end the day.

.. figure:: /trips/2015/reykjavik_seawall.jpg
   :alt: biking along the ocean

   Biking along the seawall

.. figure:: /trips/2015/reykjavik_bnb_view.jpg
   :alt: houses in reykjavik

   View from our cute little apartment

September 1
===========

This would be our last morning in Iceland. We got up and ate breakfast
before biking over to the nearby bakery for coffee and pastries.

From there we biked over to the zoo. The zoo here only has animals that
domestic Icelandic animals, which means it has about 6 kinds of animals,
including ducks, sheep, and cows. There are not many animals in Iceland.
Just across the way, however, is a botanical garden, which to our
surprise had all sorts of plants in it (since most of Iceland has
nothing but moss and grass). We also visited Þvottalaugar, the remains
of an hold hot springs that was used by the women of Reykjavik to do the
laundry many years ago.

After that, we rode back, dropped our bikes off, ate some food and ice
cream, and hopped on a bus to Keflavik airport.

Related Links
=============

* `Alan's Flickr album <https://www.flickr.com/photos/alantrick/albums/72157655937946063>`_
* `Dorothy's Flickr album <https://www.flickr.com/photos/134577085@N05/albums/72157656352262024>`_

Footnotes
=========

.. [#f1] In old Germanic/English, “thing” also meant “assembly/meeting”
.. [#f2] Kirkjufell is probably the most photographed peak in Iceland
