=================================
 Wedge Mountain, Northeast Arête
=================================

:trip date: 2015 May 2–3
:party: Alan Trick, Mauro Perizzolo, John Gill, Lauren Brown, Jason Metcalfe
:cover: /trips/2015/wedge_cover.jpg
:tags: Skiing, Trip, 2015, Outdoors
:summary: Cute mouse & cool mountain, but oh-god-these-skis-are-heavy

For the life of me, I can’t remember how I got roped into this trip, but
somehow I did. Mauro and I hauled our skis, and Jason his split-board up the
“luxurious” trail that makes its way to Wedgemount Lake. John and Lauren,
being much wiser, travelled on foot.

Around lunch time we arrived at the lake and found a cute little mouse in the
cabin there. We thought the mouse might not appreciate 5 smelly guys crowding
into his home for the evening, and there was plenty of day left, so we decided
to continue and camp at the base of the glacier instead.

Early the next morning we set out and hiked up to the base of the arête. The
snow was crusty, firm on the outside, but softer underneath. It would be a
nightmare to protect, but it seemed to hold us well enough,


.. figure:: /trips/2015/wedge_traverse.jpg
   :alt: Traversing along the arête

   Would you like it firm, soft, or pleasantly exposed?


.. figure:: /trips/2015/wedge_sunrise.jpg
   :alt: Arête in the sun

   Arête in the sun

After a while we saw the ACC team hot on our tail and catching up as we reached
the end of the arête. From there, there’s a little col where you can access the
glaciers, and then the arête goes steeply up to the summit again. We left our
skis there and climbed the last little bit to the summit, arriving just before
8:00.

.. figure:: /trips/2015/wedge_ne_arete.jpg
   :alt: The full arête

   The full arête

After that we walked back to the col, put on our skies and
half-skied/half-skidded down the icy and chunky Wedgemount Glacier. I’m not
sure that skiing Wedge in the morning is something I could ever really
recommend to anyone. Then we packed up camp and hauled our heavy gear back
down the trail again.
