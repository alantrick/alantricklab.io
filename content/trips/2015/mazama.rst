======================
 Mazama Climbing Trip
======================

:trip date: 2015 May 29–31
:party: Alan Trick, Dorothy Trick
:cover: /trips/2015/prime_rib_climbing.jpg
:tags: Climbing, Trip, 2015, Outdoors
:summary: Our first climbing trip after getting married. It was pretty
    touch-and-go, unlike our marriage.

Our honeymoon had been pretty short, so we wanted to go on a bit of a longer
trip to celebrate our marriage. I had Friday off, so Friday morning we packed
up the car and headed to Washington Pass to climb South Early Winters Spires.
Unfortunately, when we arrived at the trailhead, I realized that the rope was
missing. I’d gotten distracted at home while packing the car and left it on
the floor of underground parking. So we drove down to Mazama instead.

Our first stop was the climbing store, were we bought a new rope and a guide
book. We got settled in at the hotel and then went out to the local crag to
try some climbing. That lasted for about 30 minutes before the afternoon
showers came in and we got a complete downpour. We scurried back to the hotel
and hung out for a bit until the weather cleared up an hour later and we went
back to climb for the rest of the afternoon.

The next morning we woke up early to go climb Prime Rib. Unfortunately, we
weren’t quite early enough—there were already four teams ahead. Fortunately
they didn’t go too slow at first, so it wasn’t a bit deal. The climbing was
pretty fun and easy, Dorothy even lead a few pitches.

.. figure:: /trips/2015/prime_rib_au_cheval.jpg
   :alt: Dorothy on rocks

   Dorothy sitting on an “au cheval” move part way up

.. figure:: /trips/2015/prime_rib_climbing.jpg
   :alt: Dorothy climbing Prime Rib

   Dorothy climbing Prime Rib

As we got further up the crowds started to bunch up though. Unfortunately,
with only one bolted route and no other protection options it’s not really
feasible to pass other parties or anything. We arrived at the summit about
16:00.

.. figure:: /trips/2015/prime_rib_crowds.jpg
   :alt: Crowds on Prime Rib

   Crowds on Prime Rib


At this point we probably should have realized the folly in rapping,
but we really didn’t want to have to walk all the way back to our car, so
we went ahead an rapped anyways. To speed things up we tried simul-rapping,
but simul-rapping on terrain with lots of ledges is just downright miserable,
and probably just slowed us down. We got back to the car about 20:40.
We raced back to the hotel were we had dinner reservations, but the restaurant
had already closed. They recommend another restaurant to us. We got there
about 20 minutes before it closed, which was good, because the next source
of food was about a 2 hour drive away. We ate some of the greasiest burgers
we’ve ever had, but when you need calories anything is good.

Sunday we slept in, did some climbing again at the local crag, and went home.
