====================================
 So, you give a mountaineer a canoe
====================================

:date: 2013-3-17
:trip date: 2012-9-7–2012-9-9
:party: Alan Trick, Greg Levitsky
:summary: In which Greg Levitsky and Alan Trick carry a canoe up to
          a mountain lake in search of some metal scrap (an old WWII
          bomber crash).
:tags: Outdoors, Trip
:cover: /trips/2012/bomber_camp.jpg

Email from Greg Levitsky:

    ...That would involve canoeing up
    Widgeon Creek, hiking to Widgeon Lake, crossing to the west side of
    Widgeon Lake and then bush whack and scramble to the peak north of
    Widgeon Lake where the bomber is...

“That sounds like fun” I replied. I had never carried a canoe before, so
when Greg mentioned that we’d be bringing one up to cross the lake, I
thought little of it. I’d also never done the hike up to Widgeon Lake
before.

Greg had thought of doing the trip on a long weekend, but I’d spent that
rock climbing so we ended up with only two and a half days to complete
the expedition. We had decided to do the trip alpine style, or at least,
as much as carrying a canoe can count as “alpine style”. We brought no
tents, no climbing gear or crampons, my only decadence was a lightweight
ski mountaineering ax. Friday afternoon at 5:30, immediately after work,
we met in Langley, drove together to Pitt Lake and unloaded the canoe.

By 7:00 we were paddling across to Widgeon Creek. It was calm and
pleasant, a few motor boats remained in the lake, taking advantage of
their last hour of daylight. The boating was pleasant and easy, except
for a short bit of shallow water where I had to get out and push the
canoe along. I was glad to have flip-flops on for the boat ride. We
arrived at the end of Widgeon Creek around 8:00, just as the sun sets.

.. figure:: /trips/2012/widgeon_first_camp.jpg
   :alt: image of canoe and half-packed bivy site

   First Bivy


From there we load the canoe on our backs, and I slowly start to realize
what I’d signed up for. It soon became apparent that carrying the canoe
together didn’t really work, so each of us took turns with it. We
carried it for about 10-15 minutes at a time, until it became too
painful to bear, and then gave it to the other person. At about 12:00, I
decided I was running low on energy, so we stopped in the middle of the
trail, laid out our sleeping bags, and slept.

.. figure:: /trips/2012/widgeon_tree_canoe.jpg
   :alt: canoe and trees

   The canoe getting stuck in trees

We woke up with the sun at 6:00, packed our sleeping bags and carried
on. Last night we had been on a wide road which except for a few
sections of disrepair made relatively easy travel. As we continued on
the trees got thicker and the canoe required careful manoeuvring to get
through in some places. We stopped for a small breakfast around 9:00, at
this point the trail got much steeper. We lifted, pulled and scraped the
canoe along dirt, roots, fallen trees, and slimy rocks. The slimy rocks
made carrying the canoe particularly difficult. We finally reached the
lake around 2:00 pm, the hike up had taken us a total of 12 hours.

The lake was pristine and incredibly beautiful. The weather was clear
and you could see clear to the bottom of the lake for a while until it
sunk down into darkness. I don’t think it’s possible to understate the
contrast between the peaceful lake and the back-breaking work we’d just
finished. I pumped lake water through the filter while Greg paddled. We
were both happy to hydrate. On the other side there is a lovely beach.
We carried the canoe off the beach and had a bit to eat.

.. figure:: /trips/2012/widgeon_lake_at_peace.jpg
   :alt: widgeon lake

By 3:30 we were off again. This time no canoe and no trail. We made our
way through a mixture of bog, forest, and large boulders. While the
going was not terribly rough, it took a bit of effort not to end up shin
deep in mud. We came out into a large cirque with cliffs on every side.
I had noticed this on the topo map and had worried about it, but a
previous group had been up here years ago and hadn’t noted any
particular difficulty with it so I thought it would be fine. Neither of
us were able to see any obvious way up. Perhaps it would have been
different had we had all day to find our way up, but after a bit of
walking around to look for options, it was already 5:00 pm and daylight
was running short. I was tempted to give up, but it would be such a pity
at this and Greg probably wouldn’t have let me anyways. I picked a line
straight up on the north side of the cirque, and crossed my fingers that
we wouldn’t get cliffed out.

The bottom of the cirque: creek, bush, thorns, bog, boulder, forest, it
pretty much has everything. Even in the open areas it's a definite
route-finding challenge.

In the end, it worked out surprisingly well. While it certainly wasn’t
easy, every 50 m or so we’d run into a cliff and there just happened to
be a narrow ledge or steep ramp up one side or another. All in all, the
climbing wasn’t any more difficult than Blanchard Needle after the rap
stations, with at least as much vegetation. The crux was an exposed
climb over a two tree trunks. We topped out just as the sun set.

.. figure:: /trips/2012/bomber_parts.jpg
   :alt: bomber wreckage

   The bomber in the night

Above the cirque is a beautiful alpine wilderness with endless slabs of
granite and patches of snow. Travel was fairly easy and would be easier
at midday when the snow was soft. We walked around to the northeast side
of the peak and stumbled across the bomber’s right wing at 10:00 pm. We
were both exhausted and extremely happy to have made it so far. We had a
much needed dinner and bivied down on a ledge behind the wing.

.. figure:: /trips/2012/bomber_camp.jpg
   :alt: campsite

   Our bivy in the morning (you can see a propeller sticking up behind
   some rocks in the back)

We woke up in the morning to a thick haze, and obvious signs of a storm
approaching. After breakfast we tried looking for any of the rest of the
bomber, but it was pretty futile in the mist. Around 8:00 am, we began
descending. The snow was very packed and we were very glad to have an
ice ax to share between us. At first I try to see if I can find an
easier way down but quickly realize that will most likely get us lost
and stuck. Greg had brought his GPS which was a lifesaver in the fog. We
end up taking the same tracks up as we did down (give or take a few
meters). A keen memory was indispensable here, because even a dozen
meters made a huge difference on these cliffs, and the way down was not
at all obvious.

We end up arriving back at the lake at 11:00, and on the other side by
noon. By this point the lake is gloomy and it has started to rain.
Picking the canoe back up again was a difficult thing to do, but at
least I had the consolation that we would be going downhill. The
downhill was still quite difficult (lots of being jerked over roots and
slimy rocks). There was a rope attached to one end of the canoe, and I
gave the it a hip belay at the two roped sections on the trail. The most
important thing was to make sure that the canoe stayed intact and that
our bones stayed in tact. We just kept going, and going until we finally
reach Widgeon Creek at 8:00 pm. At this point I was greatly relieved.
The parking lot gets locked at 10:00 and 2 hours was plenty of time to
make it back there; and, although we had left a bit of canoe paint back
on the trail, our canoe was still intact and sea worthy. I was miserably
soaked and bedraggled, but also as happy as ever.

The canoe ride back was thankfully uneventful, after loading the gear
onto the truck we went immediately to Mcdonalds to stock up on calories.
Afterwards I vowed never to carry a canoe up there again, but the beauty
of the area is inspiring and I keep thinking of the possibility of
returning.

.. note:: This trip was originally reported on `ClubTread <https://forums.clubtread.com/27-british-columbia/44475-so-you-give-mountaineer-canoe-sept-7-9-2012-a.html>`_
