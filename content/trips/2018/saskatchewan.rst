==============
 Saskatchewan
==============

:date: 2018-7-6
:trip date: 2018 June 28–July 3
:party: Alan Trick, Dorothy Trick
:summary: In which Dorothy and Alan spend far too much time in a car
:tags: Trip, 2018, Vacation
:cover: /trips/2018/dorothy_regina_hill.jpg

Driving there
=============

The drive there was mostly uneventful. We stopped Lake Louise because
Dorothy had never been there. I think Dorothy was a little disappointed
because it was basically a pile of tourists milling around a dock by the
edge of a lake. The lake was nice enough, but I guess Dorothy might be
turning into me a little more, and starting to despise the presence of
other humans.

.. figure:: /trips/2018/dorothy_lake_louise.jpg
   :alt: Dorothy on the shore at Lake Louise

   Dorothy finding the single spot on the deck not covered with tourists

We had planned to eat a fancy meal, but the restaurant at the Fairmont
was completely overrun, so we went over to the Deer Lodge and a $80
worth of steak and tomato soup. Quite pricey, but the steak was sublime.

That night we stayed at Tara’s place (Dorothy’s friend). On the way up
the 30-ish floor elevator, Dorothy got gifted a lava cake from someone
random guy who got one with his pizza. The lava cake tasted fine, and it
helped with Dorothy’s heartburn a bit, so it all went well.

On Friday morning, we met Kevin & Joy at Cora’s in western Calgary. We
chatted for a few hours and caught up on how things were going with
them. From there we went to Caronport and stayed with another friend
Saryna. For dinner that night, Saryna fed us tacos which I think was the
only home-cooked food we ate the entire trip.

In Saskatchewan
===============

On Saturday morning we drove into Regina
for the wedding. I ended up parking our car in Douglas Park, across the
highway from the ceremony. For a moment, Dorothy was giving me a bit of
a stink-eye, thinking that I had taken her to some to some strange place
and that we would be late for the wedding. Fortunately, she saw the
church and the bridge over the highway and I was let off the hook—this
time.

.. figure:: /trips/2018/dorothy_regina_hill.jpg
   :alt: Dorothy walking up a small hill

   Dorothy scaling the slopes of Wascana Hill

After the reception we walked around Douglas park a bit and up to
Wascana Hill. It’s not a huge hill, but you actually get a pretty nice
view of the park and the city from the top. After that that, we settled
into our lodgings; and then went to the reception, where we ate entirely
too much food.

On Sunday, we went to Kipling. All the church schedules were really
wacky, so we walked around a little bit, visited the World Largest
Paperclip, before the first service started. We went to the People’s
Church first, where people sung the first few lines of every song over
and over and over and over ... and over again, met some relatives or
something, and then went to the Baptist Church where they were having a
combined service of almost all the other churches in Kipling. That
service was kind of interesting, the average age was probably close to
60. We met another relative there.

After church, we went to the Canada Day festivities at the ball parks on
the south side of town. There was a petting zoo there, which Dorothy
enjoyed.

.. image:: /trips/2018/dorothy_donkey.jpg
   :alt: Dorothy petting a donkey
.. image:: /trips/2018/dorothy_chick.jpg
    :alt: Dorothy holding a chick
.. image:: /trips/2018/dorothy_rooster.jpg
    :alt: Dorothy holding a rooster
.. image:: /trips/2018/dorothy_ice_cream.jpg
    :alt: Dorothy holding an ice cream cone

After that we went back to Regina and had dinner with another one of
Dorothy’s friends.

Driving back
============

Unfortunately, the way back was a little more interesting. We bailed on
our plans to hike in Canmore because the weather was bad and just went
home instead. When we arrived in Roger’s Pass the traffic was lined up
for serveral kilometers (there had been a vehicle collision). We weren’t
sure what the deal was, so we tried to find a traffic station on the
radio. As it turns out, the only radio station available in Rogers Pass
is an Indian music station, so we just listened to that instead.

On our way through Sicamous, we stopped at D Dutchman Dairy and the
adjacent Fruit World for ice cream and fruit. At Fruit World, Dorothy
showed me a bar of soap. I smelled it and it smelled like someone had
mixed up soap and thyme. Dorthy told me to look at the label: Sexy
Beast. Apparently “sexy beast” smells like thyme.
