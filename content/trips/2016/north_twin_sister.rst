===================
 North Twin Sister
===================

:trip date: 2016 July 2
:party: Alan Trick, Dorothy Trick
:grade: PD+
:cover: /trips/2016/north_twin_sister_hangout.jpg
:summary: Another fun day on one of my favorite mountains
:tags: Trip, 2016, Outdoors

The first time I actually did this ridge was in 2013 with Julio and I
had a great time, with the exception of some flat tires on the way down
that put a bit of a damper in things. This time, I had a tire pump with
an actual pressure gauge so I knew I could get it pumped up properly.

The North Twin Sister is such a fun trip because you get to do half
the elevation on a bike, and much of that is on pretty nice roads and
single track.


.. figure:: /trips/2016/north_twin_sister_push.jpg
   :alt: Alan pushing bikes

   I get the lucky job of pushing the mountain bikes up the road

The road up is pretty uninteresting. It keeps going and going, but the
going is pretty easy and you can even ride a little bit of it. Up nearer
to the top the road is a little overgrown, and has cross-ditches and then
large, chunky gravel at the very top. It probably took us about 3 hours
to make it up the road. At the end of the road, there’s a little bivy site
and a trail that heads into the scrubby cut-block and eventually the
forested base of the West Ridge. After about another hour you arrive at
the start of the actual scrambling.


.. figure:: /trips/2016/north_twin_sister_ridge_start.jpg
   :alt: Climbing at the bottom of the ridge

   Easy scrambling at the bottom of the ridge

.. figure:: /trips/2016/north_twin_sister_fog.jpg
   :alt: Climbers on a foggy ridge

   Climbing the ridge in the fog

.. figure:: /trips/2016/north_twin_sister_crystals.jpg
   :alt: crystals on rock

   What makes the rock so sticky

The climbing on this ridge is just so fun. The rock almost feels like
sandpaper and it has a lot of grip. It’s also really blocky and so even
though the ridge is quite steep in places, there’s still lots of features
to jam your hands in and (more importantly) rest your feet.

.. figure:: /trips/2016/north_twin_sister_dance.jpg
   :alt: Dance, Dorothy, Dance

   Dorothy practicing her dance moves on a false summit

About 2/3rds of the way up we ran into a crux. It was short but fairly
tricky and quite exposed. I thought it was fun, but Dorothy wasn’t so
pumped about it. [#f1]_

.. figure:: /trips/2016/north_twin_sister_crux.jpg
   :alt: Alan climbing steep-ish rock

   The crux of the route we did, it felt like 5.3 or so.

A little while later the ridge eases off as approach the summit. There’s
a large rocky perch right around the area that you normally descend from.
From here the route to the summit is pretty easy, just a few short tricky
moves. We reached the summit about 14:00.

.. figure:: /trips/2016/north_twin_sister_hangout.jpg
   :alt: Alan on a rocky perch

   A prominent ledge juts out right around the top of the descent

We descended of the Northwest Face which is a really nice descent. It
starts off a little steep, and in our case it was a little icy too, but
after a while it eases off and you can boot-ski for most of the way.
Once you get of the snow, you head southeast and meet up with base of
the ridge, just before it goes back into the forest.

.. figure:: /trips/2016/north_twin_sister_descent.jpg
   :alt: descending moderately steep snow

   Dorothy cruising down the descent

And of course, the ride down is great, except that Dorothy crashed on
an awkward cross-ditch, and landed her leg on a large rock, giving her
a spectacular bruise. We got back to the car a little after 18:00.

.. figure:: /trips/2016/north_twin_sister_ride.jpg
   :alt: the ramp from below

   Relaxing on the ride back

.. [#f1] I’ve since figured out you can bypass this on class 3-4 terrain
         to the north (climber’s left).
