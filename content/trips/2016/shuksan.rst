===================================
 Mount Shuksan via Fisher Chimneys
===================================

:trip date: 2016 July 22–25
:party: Alan Trick, Dorothy Trick, Mike VanWerkhoven, Christian
:grade: AD-
:cover: /trips/2016/shuksan_summit_return.jpg
:tags: Trip, 2016, Outdoors
:summary: 2.5 long days & fantastic scenery

After our success on `Harvey’s North Ramp </trips/2016/harvey_north_ramp>`_,
I figured Dorothy & I were ready to do Shuksan together. Mike had been
itching to do it, so one weekend we made plans to drive up on Friday night
and hike into Lake Ann and then climb the mountain on Saturday & Sunday.

It had been raining on Friday and we arrived at the parking lot at about
21:00 to the sight of some bedraggled climbers returning from a jolly
day of getting drenched in the rain. While it was dark and cold, and still
drizzling a bit, at least we weren’t as bad off as they were. We strapped
on our packs, headlamps, and headed off.

About 45 minutes into the hike, I realized I’d forgotten my sunglasses in
the car. Hiking all the way back up there has a pretty dismal thought, but
there was no way I was going to last on the glacier without sunglasses, so
I gave Dorothy the tent and Christian and I returned to the car to get
my sunglasses. In the end, we got to the campsite at about 2:00.

We woke up again about 8:00 and packed camp and headed back out. It wasn’t
raining anymore and you could start to see some breaks in the clouds.
The first bit of the trail snakes its way up some steep scrubby slopes
before reaching a short rocky gully. At the top of the gully we crossed
a large snow slope and came across a large, tricky moat that we had to
carefully hop across in order to reach the rock at the base of Fischer
Chimneys.

.. figure:: /trips/2016/shuksan_chimneys.jpg
   :alt: Dorothy sticking her head out from behind some rocks

   Dorothy in the chimneys

.. figure:: /trips/2016/shuksan_chimneys_end.jpg
   :alt: A rocky gully

   Upper part of the chimneys

The Fischer Chimneys themselves are mostly class 2 and 3 with a few
short sections of class 4. I thought the route-finding in them was
easier than other sources on the internet had suggested, but perhaps
its worse in other conditions. After the scrambling was over, we
traversed a snow slope an arrived at the campsite about 12:30.

At camp, we hummed and hawed about what to do next. Dorothy felt
tired and wanted to stay at camp. I wanted to stay with Dorothy,
and Mike and Christian wanted to go to the summit, but not alone.
In the end, Dorothy decided to keep going and we slogged up, into
the fog.

We climbed up Winnies Slide, and slogged our way up and across
the Upper Curtis Glacier, and Hell’s Highway. Once we got on the
Sulphide Glacier we were stuck in the clouds, and we couldn’t see
very far. As we reached the summit pyramid, the clouds started
to lift and you could see all the little climbers below.


.. figure:: /trips/2016/shuksan_sulphide.jpg
   :alt: The Sulphide Glacier

   Tiny climbers on the Sulphide Glacier

.. figure:: /trips/2016/shuksan_summit_climb.jpg
   :alt: Climbing on some rock with Mt Baker in the background

   The summit climb

The summit pyramid itself was probably easy 5th class. We roped
up a bit on the way up. The view from the top was amazing as it
was mostly just a sea of clouds below with Mount Baker sticking
out to the west. We reached the summit about 18:00, ate food and
took selfies. On the way down we did a few rappels and then walked
down the Sulphide Glacier and back into the clouds.

.. figure:: /trips/2016/shuksan_clouds.jpg
   :alt: Walking into fog

   Descending back into the clouds

At this point though, the cloud layer was rapidly dropping and by
the time we got to camp is was well below us again. It was getting
dark by the time we reached camp and we ate a hardy dinner and got
ready for a nice, very long sleep.

.. figure:: /trips/2016/shuksan_camp_night.jpg
   :alt: Sun setting on clouds

   Our campsite at twilight

We slept in the next day and woke up at the late hour of 7:30. This
day we had it pretty easy, which is good, because we were pretty
knackered. We did a few rappels down the chimneys. And then walked
down to Lake Ann where we met a boatload of normal hikers and families
out for their Sunday stroll. From there we hiked back up to the parking
lot and drove home.

.. figure:: /trips/2016/shuksan_dorothy_ann.jpg
   :alt: Dorothy with Shuksan behind

   Dorothy smiling at Lake Ann, Shuksan behind
