=======================
 Lillooet Ice Climbing
=======================

:trip date: 2016 January 23–24
:party: Alan Trick, Mike VanWerkhoven
:cover: /trips/2016/lillooet_cover.jpg
:tags: Ice Climbing, Trip, 2016, Outdoors
:summary: Two days of fine ice climbing with plenty of gaping holes.

I went ice climbing in Lillooet this year with Mike for the first time in a
while. The first day we climbed at Marble Canyon. It was crowded like usual.
We climbed the first & the second pitch of Icy BC and then tried the third
but bailed. The second pitch had a nice little hole in it.

.. figure:: /trips/2016/lillooet_icy_bc.jpg
   :alt: Me on pitch 2

   Me on Icy BC, pitch 2

The second day we figured we’d try out Honeyman Falls. We drove to the
trailhead on Hunt Road and were met by a dog that travelled with us down
the service road to the creek.

.. figure:: /trips/2016/lillooet_dog.jpg
   :alt: Mike and the dog

   Dog wants to go climbing

Once you leave the service road, the approach is a little steep and then
turns into a short scramble to get around a rocky bit that juts out. I
believe sometimes the creek is frozen over further down and you can try to
walk on it, but I’ve heard stories of people making holes and falling
through.

.. figure:: /trips/2016/lillooet_honeyman_approach.jpg
   :alt: Mike on scrambly terrain

   Easy scrambling on the approach

I lead the first pitch of Honeyman Falls. It was pretty exciting because
my skills were pretty rusty. I climbed up to where it starts to ease off
and then built an anchor.

.. figure:: /trips/2016/lillooet_honeyman_lead.jpg
   :alt: Alan leading the first pitch

   Me leading, pumped up and ready to screw

.. figure:: /trips/2016/lillooet_honeyman_mike.jpg
   :alt: Mike climbing

   Mike climbing up behind me

Mike led the next pitch and he was over a lip, so I wasn’t able to
see what happened, but about 5–10 minutes into the climb I heard an
explicative and then saw a refrigerator sized chunk of ice come down.
Fortunately, Mike was not attached to it. At any rate, Mike came
climbing back down. Apparently the the top of the climb was mostly an
open, flowing river and trying to get around it was too dangerous.
We bailed and rapped down.

On the way down, I accidentally set my ice tools down on a portion of
the scramble. Once we reached the road, I realized that they were
missing. Fortunately, we were able to retrace our steps and find them
easily.
