========================
 Howe Sound Crest Trail
========================

:trip date: 2016 March 12
:party: Alan Trick, Dorothy Trick, Greg Levitsky
:cover: /trips/2016/hsct_ridge_dorothy.jpg
:tags: Skiing, Trip, 2016, Outdoors
:summary: How a nice little 5 hour day becomes a 16 hour epic.

“It’ll be easy, maybe 5–6 hours” I said when Dorothy asked me about how long
it would take to ski the Howe Sound Crest Trail. So, on Saturday morning we
shuttled cars between Lion’s Bay and the Cypress Ski Resort. We happened to
have ski passes that year, so at 9:00 we took a lift up the Lions Chair
and road down to the bottom of Top Gun, where it meets with the trail.
I don’t think it really saved us any time, but at least we got some
skiing in.

The first bit of the trail was pretty uneventful. It’s flat, so that’s nice
if you like flat, but the trail goes through the forest so there’s not too
much variety. After a while, we finally entered a little meadow-y area and
then started the climb up toward St. Marks.

.. figure:: /trips/2016/hsct_ridge_dorothy.jpg
    :alt: Dorothy on a snowy ridge

One thing that I hadn’t expected was that once we got up into the sub-alpine
the ridgeline becomes quite narrow and exposed to the wind. The upshot of
this is that if there been any harsh weather recently, you end up with
large shoulder-height snow drifts and scoured wind crust make skiing
quite difficult. We finally reached the top of St. Marks about noon, and for
a moment the clouds opened up and we got a nice view of the Howe Sound,
which is nice because I think the scenery is supposed to the the highlight
of this route.

.. figure:: /trips/2016/hsct_st_marks.jpg
   :alt: View of Howe Sound from St. Marks

After St. Marks we descended a bit and then continued up back up on the
other side toward Unnecessary Mountain. There were more drifts and the
slopes were steep and crusty which made travel a little tricky. Once
we came out into the alpine around Unnecessary it became even more
difficult.

We topped out on Unnecesary about 14:30, a little slow, but it’s basically
downhill from here. We looked over towards the Lions and the ridge looked
pretty narrow. Normally this wouldn’t be a problem, but all the snow and
the wind had made things really touchy and we figured the avalanche danger
was at least “considerable”. There are 100m high cliffs on the NW side of
Unnecessary and it was hard to gauge if there were cornices. The visibility
was only about 50 meters, so it was hard to tell what was a short slope and
what was giant avalanche trap. Greg tried to make his way over there anyway,
but eventually got stuck in an icy gully and turned back. About this time as
well, someone heard thunder, which is really no good at all on the top of a
mountain. So with that we decided to bail, and ski down the west ridge of
Unnecessary.

The first few turns down the ridge were very exciting. There's very
little to protect you and it’s quite steep an all sides, after a few
minutes though we were in the trees and the skiing was quite fun for
a while. Once we reached about 1300 m elevation, we figured it should
be safe to cut right and ski through the large bowl and over to the
trail on the other side. This was at about 15:30. We shortly debated
continuing down the ridge, but we didn't know if the trail went through,
(turns out it does) and our car was on the north side of Lions Bay, so
we figured we'd half to walk back to get it, which would be about 40–60
minutes of walking in ski boots on pavement.

So we went right. After about 200 meters we hit our first gully. About
2 body lengths deep with a little creek at the bottom, there was no
skiing across this thing. We took off our skies, climbed around it,
and up on the other side. Skis back on. 100 meters later, another
gully, skies off, climb around, skis on. 100 meters later, another
gully. This continued. At one point we ended on top of a little 5 meter
cliff We scouted around but it looked like there was no easy way around
it. At the bottom of it there was a creek, and the top, thick bushes,
so we had to carefully lower each other down and then the skis and I
came barrelling down last. Eventually we make it out and onto the
Lion's trail.

I had originally expected this to be an enjoyable snow-covered ski. It
was nothing of the sort. We tried skiing for a minute or two, but
constant fallen logs, protruding rocks, and holes in the snow made it
nearly impossible. We strapped our skis to our packs and clompped our
way down the mess of slipper rocks and fallen logs that made for a
trail. At least if you slipped on the trail the skied on your pack were
likely to get snagged on a tree.

We finally cross Harvey Creek make it onto the old road. At points we
can see the lights of Lions Bay glimmering though gaps in the trees
and reminisce of the long-forgotten comforts of civilization. The walk
down seems to last forever, but we finally arrive at the car about
1:00—a 16 hour day.

After driving back to the ski resort to get the other vehicle, we end
up in McDonalds in North Van about 2:00. For some reason, it seems like
all the folks who just got kicked out of the bar were also there to, and
a couple guys were trying to pick up a girl with all the gracefulness of
a hippopotamus. Interesting crowd.
