=========================
 Mount Harvey North Ramp
=========================

:date: 2016-4-11
:trip date: 2016 April 9
:party: Alan Trick, Dorothy Trick, Neil Kirk,
        Helen Habgood, Pei-Yin Lew, Gordon Bosch
:grade: PD+, M3+ 40° snow
:cover: /trips/2016/harvey_ramp_rock.jpg
:tags: Trip, 2016, Outdoors

For the first time in my memory Dorothy asked me to go on a
mountaineering trip with her. Normally the conversation goes something
like this (from me to her):

    Hey dear, we should go climb such-and-such. It'll be fun ... No, it's
    not *that* hard. It should only take about 8–9 hours [#f1]_
    ... Do you mind if we wake up at 3:30?

At any rate, I don't know what came over her, but this time I didn't
have to do any of that persuasion. It may have had something to do with
the fact that the stroll up the West Ridge of Mount Harvey appears in
the “103 Hikes in Southwestern British Columbia” book she has.

We woke up at 3:30 and met up with Neil and Gordon at Saint David's just
before 5:00. A short drive north took us to the trailhead at Lions Bay,
where we took up the last 2 parking spots—the parking was now full at
5:15! The next hour or two involved the long slog up the FSR to the
beginning of the ramp. On the way there we spotted the ramp and noticed
that it looked a little patchy, but figured we'd try our luck anyways.

.. figure:: /trips/2016/harvey_ramp_beginning.jpg
   :alt: the ramp from below

   The start of the ramp

Neil had recently cleared the brush off the approach trail, making it
much more enjoyable. By 8:00 we had arrived at the base of the ramp, put
on our climbing gear, and finished our requisite pee breaks. The start
of the ramp went pretty easy. There was a few moats, but they were
easily by-passed or easy to step over. We were about 1/3 of the way up
the ramp before we bothered to do any belaying. In some places the snow
was a little patchy and sketchy, but mostly there was enough.

.. figure:: /trips/2016/harvey_ramp_belay.jpg
   :alt: Waiting at a belay

   Waiting for space at the belay


The snow itself was quite nice: soft enough to get a good boot step
without much work, and good enough for deadmans and mid-clipped pickets,
but not much good for top-clipped pickets. The worst of the cornices
seem to be gone too.

About 2/3 of the way up the ramp we came across an unavoidable rock
band. From below it didn't look all that bad, but the rock itself proved
to be quite difficult. It was wet and slimy [#f2]_ and pretty slabby. Most of
what had appeared to be “holds” were down-sloping and not very
confidence inspiring. Neil led through this section and the rest of us
were given a belay. Fortunately there was pretty good tree/bush at the
top for a belay, because the snow above was quite thin and soft, and not
much good for a snow anchor.

.. figure:: /trips/2016/harvey_ramp_rock.jpg
   :alt: The rock band

   The rock band

From here we gained the shoulder on the left side of the top of the
ramp. The top of the ramp had another moat in it which I was not to
eager to climb over. After our difficulties at the rock band, the
traverse at the top went without any real difficulty. We then
short-roped our way up through the krumholtz to the summit ridge.

We arrived at the summit just before 14:00 and had a nice long lunch
break. We took summit photos, chatted about the nearby mountains, and
worked on our skin cancer. The descent was pretty uneventful. We had
pretty nice snow to walk down in until about 1000m, and then we bashed
our toes the rest of the way to the car, arriving there about 17:00.

.. figure:: /trips/2016/harvey_ridge_descent.jpg
   :alt: The ridge descent

   Descending the West Ridge

Related Links
=============

* `Flickr album <https://www.flickr.com/photos/alantrick/albums/72157666948041526>`_

Footnotes
=========

.. [#f1] In other words: 12 hours
.. [#f2] One person tried to climb it without crampons; she was not very happy
