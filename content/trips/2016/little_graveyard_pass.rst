=======================
 Little Graveyard Pass
=======================

:trip date: 2016 October 1–2
:party: Alan Trick, Greg Levitsky
:cover: /trips/2016/chilcotins_frozen_mud.jpg
:summary: Mud, bikes, mud, a little camping, more mud ...
:tags: Biking, Trips, 2016, Outdoors

This year Greg finally got me to go to the Chilcotins with me. It wasn’t
until October that we were able to make it out there, but we were finally
able to squeeze it into a weekend.

We left home in the mid-early afternoon and drove up the Sea-to-Sky highway.
On the way, we decided to get a little touristic and stopped in the village
of Bralorne, an almost-ghost town that was was revived a little while back
by better mining technology, and is now trying to subsist off of tourism.
While we were there we went and looked for the museum, but it was in the
process of being moved, and everything was locked up. There was a bunch of
old machinery outside and a little maze though.


.. figure:: /trips/2016/bralorne_maze.jpg
   :alt: Little rock maze

   The maze at Bralorne

After that, we continued on to the Chilcotins. Because we had a lot of
time that day, we decided to drive all the way to the end of the provincial
park, up on Relay Creek, with the plan to bike up to Little Graveyard
Pass the next day. The road just kept on going and going, and we finally
arrived at the campsite just before sunset.

.. figure:: /trips/2016/relay_campsite.jpg
   :alt: campsite

The next day we got up early, had breakfast, and got on our bikes. It had
rained the night before but fortunately things had stopped by the time we
woke up. We rode out on a little trail which eventually met up with the
double-track path that went above the campsite area. After a couple
kilometers of flatish up & down, we reach the Relay Cabin which looked
quite nice for being in the middle of nowhere.

.. figure:: /trips/2016/relay_cabin.jpg
   :alt: nice cabin

After that the trail forks and we went up alongside Little Paradise Creek.
While the creek itself I’m sure was quite nice and deserving of the name,
the trail itself was not so fun. It started out as a grueling steep climb
and turned into a short, narrow and bushy descent back into the valley.
From there, it started crossing the creek back and forth a few times before
heading back into the bush. After that it becomes mud, and then bush and
mud, but mostly mud.

.. figure:: /trips/2016/chilcotins_creek_crossing.jpg
   :alt: biking through a small creek

.. figure:: /trips/2016/chilcotins_frozen_mud.jpg
   :alt: bike stuck in frozen mud

Eventually there was a fork in the trail and we took a right over to Little
Graveyard Pass. The riding here is actually pretty nice, albeit steep in
parts, but we were also pretty sacked by that point. When we finally reached
the top we had a nice long break.

.. figure:: /trips/2016/graveyard_pass_bike.jpg
    :alt: bike at the top of Little Graveyard Pass

.. figure:: /trips/2016/chilcotins_coming_down.jpg
   :alt: riding down the trail

.. figure:: /trips/2016/graveyard_pass_frozen.jpg
   :alt: Greg with a snow covered peak behind him

The ride down was fun while it lasted—about five minutes. The Little
Graveyard Vally is fairly open, and quite beautiful, but it quickly
turns into thick chest-level bush, and mud, and mud and bush.


.. figure:: /trips/2016/graveyard_pass_mud_again.jpg
   :alt: more muddy trails

Eventually we meet up with the main Graveyard Valley, which is supposed to
have been a popular battleground for the regular fights between the
Tsilhqot’in (Chilcotin) and St’at’imc (Lillooet) tribes. There is apparently
a `war memorial`_ here, but unfortunately we weren’t able to find it.

.. _war memorial: http://trailventuresbc.com/southern-chilcotin-history/

Eventually the trail enters the forest again and at this point it becomes
a half-decent ride again. Unfortunately there was still a lot of deadfall
and whatnot around so there was still a lot of dismounting that had to be
done. After a while the trail meets up with the one from big creek and
there's a nice big rock to lie down on. About a kilometer later, we reached
the Graveyard Cabin, which unlike the one by Relay Creek actually looks like
it belongs in a graveyard.

.. figure:: /trips/2016/graveyard_cabin.jpg
   :alt: an old cabin

The final bit of our trip was mostly unintereting to us. It probably would
have been a lot nicer, but it was getting cloudy and we were really tired
and just trying to get back to the cars before nightfall. At one point,
I thought I’d lost Greg for a few minutes, but then he turned up again. We
made it back to the car just as it got dark and started the 7 hour drive.
We arrived hoping for some dinner in Lillooet at midnight, and the only
place that was open was a little Asian convenience store. We bought some
rather expensive snack food and continue on our way. We got home about 3:00.


