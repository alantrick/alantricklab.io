=====================
 California Roadtrip
=====================

:date: 2016-9-1
:trip date: August 25–September 4
:tags: Trip, Outdoors

August 26
=========

After a late night of :strike:`partying and reverie` cleaning up and
packing, Dorothy arrived home from work and we ported the large pile of
worldly belongings from the living room floor into the back of her car.

An hour or so later we were on our way. At the Sumas border crossing we
came across a suspiciously cheerful and friendly border guard. It was
almost as if he wanted us to enjoy our time in the US. This, of course,
made absolutely no sense, but during our brief conversation I was unable
to discern his true motivation.

The rest of the time was quite uneventful. A few stops for ice cream and
coffee and fuel. In Portland we went to the Cilogear factory to see if
they could repair my backpack, but they had all left work early. After a
short stop at REI we went to Washington Park to cook and eat dinner. We
made a bit of a racket running our stove, but nobody seemed to mind.

From there, we headed off to the Oregon Coast, to Florence specifically.
On the way there we passed a burnt-out semi-truck. It was in a pretty
bad way, the cab was mostly flattened. Fortunately it was on the other
side of a divided highway because the traffic was completely stopped.
Most of the people were chatting outside their cars, enjoying the warm
evening, but one bored car driver was doing loops around a bridge post.

At 22:30 we finally arrived at our campsite—a casino parking lot.

August 27
=========

In the morning we saw it was overcast. Overcast and raining appear to be
the two forms of possible weather on the Oregon coast. We went to visit
the dunes, but a $5 parking fee was required and lacking the cash to pay
it we were only able to stop and look for a short time.

We drove on, and the rest of the day passed by with out much ado. We
drove up along the Umquat River which looked like it would make an
awesome canoe trip. We also came across a semi-trailer labeled “UTI
Distribution” and saw the 4200 meter high choss pile that they call
Mount Shasta.

August 28
=========

Driving through central California seems like driving through the
Prairies. Flat, with farms and cows and trees, but mostly flat. Last
night I came across the discovery that I had left all my clothes in
Abbotsford, so our first stop was a Goodwill in Stockton where I
acquired much of a small wardrobe for $15.

The road going to Yosemite seems endless, but fairly shortly after
leaving the I-5 you enter the foothills of Sierras, and the boring flat
road is not so boring or flat anymore.

Yosemite Valley itself, however, is breathtaking. After what seems like
endless driving through open forests, you come out into this gigantic
canyon with huge granite cliffs on each side. As you drive up the canyon
the cliffs get larger and larger, dwarfing anything I have ever seen
before. In the middle of the canyon lies the gentle Mecred River and a
mix of meadows and light forests, and at the very end towers the Half
Dome.

Having arrived a little late, we quickly scampered off to Sunnyside
Bench to climb the Regular Route. Three pitches of fun climbing plus a
little roped scramble lead to us topping out just before the sun set. As
we hiked down in the dark we noticed (much to our delight) that the
evenings here were surprisingly warm.

We had ratatouille for dinner and crawled in to our cozy sleeping bags.

August 29
=========

Today we visited Manure Pile Buttress and climbed After Six. We shared
half of the route with another couple, but they bailed half way up and
we cleaned their rap sling and returned it to their campsite later,
since they were in the same campground as us. Since we ended early, we
went and took a quick dip in the river and got ice cream from the
grocery store in Curry Village.

Dinner was spaghetti with loads of Parmesan cheese, and we packed our
bags for the next day and went to bed early.

August 30
=========

I had successfully persuaded Dorothy that we should climb the Southwest
Face of the Half Dome today. We woke up about 5:20 after snoozing the
alarm a few times. A little bustling about and a quick breakfast of
granola, and we were on our way at about 6:00.

The approach to the Half Dome is quite beautiful, which is good because
it is quite long and arduous too. You first hike out of the Yosemite
Valley via a narrow corridor containing two majestic waterfalls, and
countless stairs. Once out of the valley the trail contours around a
small mountain. Up until this point, the trail is nicely built and
fairly smooth considering the wild terrain it goes through.

From here we took the climbers trail that goes off to the southwest face
of the mountain. This trail was much rougher, and we got off route
several times. We were hoping to get water at Lost Lake, but it turned
out to be more of a marsh and while we were able to get some water, it
was only a little bit and took a fair bit of effort. At one point there
are a several misleading cairns, and you have to ignore them and climb
across a fairly exposed 4th class ledge system. Once it was all said and
done the approach took about 5 1/2 hours.

The route we went up is called Snake Dike, and the climbing really does
make up for the long approach. While the bottom 30 meters or so of the
first pitch offer no protection whatsoever, the climbing is quite easy
if you keep a good head on your shoulders. The third pitch has a move
that is quite a bit harder than everything else on the route.
Fortunately the leader is well protected by a bolt, but being along a
traverse, it can really suck for the follower. We brought two ropes, so
that Dorothy was still able to have a rope protecting here from above
for the traverse.

From there, the climbing was super fun and easy, albeit with 20–30
meters of run-out. You go up along a large spine-like dike, almost like
climbing the backbone of a dinosaur skeleton. We finished the climbing
around 17:00 and topped out by 18:00. Unfortunately, by this point we
had run very low on water and energy. We descended the cables route
(scary enough with a harness and a biner for clipping, I don't know how
normal people do this), and walked down. Some gracious soul offered us
some water on the way down which we happily accepted. We slowly dragged
ourselves down to the campsite at Little Yosemite Valley where we
restocked on water and ate food, and then fighting the urge to go to
sleep, we hiked back down into Yosemite Valley and arrived at camp
almost exactly at midnight.

August 31
=========

Today was our well-earned rest day. We slept in and ate french toast for
breakfast. We went to Curry Village and ate ice cream and hung out at
the lounge to read and journal. In the afternoon we hung around and then
went for a swim and a shower (showers are not included in the campsites
here). In the evening we went to check out the museum, but it was
closed, so we walked around and the Indian village installment instead.

September 1
===========

We had Cajun scrambled eggs for breakfast today. Re-hydrated eggs just
aren't as good as the real thing, but fortunately the spices and
vegetables were able to cover up the bland taste and funny texture of
the eggs.

We got back to climbing, getting up Munginella in the morning and then
heading off to Swan Slab to practice a little bit. I accidentally left
my climbing shoes at the top of Munginella so I had to run back up to
the top of the climb and fetch them.

In the afternoon we went to the Ansel Adams Art Gallery, Dorothy was
hoping she could get a nice framed photo or something, but the photos on
sale there were about $10,000–$20,000 (and weren't even that amazing).

Dinner was Coconut Cream Curry. We probably had enough coconut cream and
noodles to feed 3 or 4 people.

September 2
===========

On our last day in Yosemite, we went up to Tuolomne to climb Cathedral
Peak (via the Southeast Buttress). We had a long drive ahead of us, so
we woke up really early to pack up camp and drive up. The drive is about
1 1/2 hours long, and so we didn't reach the trailhead until about 8:00.
We quickly got going, however, and made our way up a nicely built trail.

About 9:45 we were at the base of the buttress, and took a break so some
climbers could get out of the way. After hanging out for 20 minutes we
got going. After 2 shortish pitches it became obvious that there was a
pile up of at least 3 slow rope teams on the route. Fortunately a bit of
maneuvering was able to allow us to climb around them without getting in
the way (first on the left below the chimney, and then on the left
around and above it). The climbing was a little harder (5.7 with maybe a
little 5.8) but perhaps even more fun and fantastic.

By 15:30 we had made it to the summit—a large block of rock that drops
down on each side and the top of which is about the size of 2 coffee
tables. The summit, while small, fortunately has a nice crack for
building a good anchor, so I was able to lower Dorothy off and then she
gave me a rather exposed belay down to where she was. A wee scramble,
one rappel, and a little wandering to find the descent trail, and we
were on our way down.

We drove down to Lee Vining, ate dinner at a comfort-food style
restaurant, and stayed the night at an RV park.

A final word about Yosemite: it is one of the funnest places I have ever
visited. The amount of stuff to do there (whether climbing, hiking,
lying on the beach, wandering aimlessly in meadows, or sitting down on a
computer and bragging about how cool your life is) is huge, and all of
it is so nice. I hope I can go back again some time.

September 3
===========

Today was Dorothy's birthday. Unfortunately we had to drive for most of
it. In the morning we stopped for coffee at some place in the middle of
nowhere and the barista ended up being a total gaming nerd. We drove
back through Nevada, and made lunch (French Onion Soup) at a park in
Susanville, California.

This evening we stayed at Green Springs Inn in Oregon. Dorothy had been
here before with her friends and had suggested we stay here. It slightly
out of the way, but after a week of camping in a tent, having a room and
a bed was heavenly. We ate dinner at the restaurant there and the food
was delicious.

September 4
===========

In morning we made and ate some delicious oatmeal by mixing instant
oatmeal packs with trail mix. After packing up we headed off to Crater
Lake. As it turned out, there was a toll to enter the park there, and so
we had to wait in a line-up of cars for a bit. Later, when we exited on
the other side, the line up was a lot worse so I was glad we weren't any
later.

The lake itself was really beautiful, and we took a number of pictures;
however, the weather was cold and we wanted to get home, so we didn't
stay there too long.

The rest of the day was spent mindlessly driving the 11 hours back to
Abbotsford.

Photos
======

`Alan's Flickr Album <https://www.flickr.com/photos/alantrick/albums/72157672269831561>`_
