====================
 Wendy Thompson Hut
====================

:trip date: 2016 February 9–11
:party: Alan Trick, Dorothy Trick
:cover: /trips/2016/wendy_thompson_cover.jpg
:tags: Skiing, Trip, 2016, Outdoors
:summary: Wherein Alan and Dorothy spend “Reading Week” freezing in a hut

Dorothy was jonesing for some camping, but being winter and all, that sounded
a bit cold, so we made plans to stay at the Wendy Thompson Hut. For some
strange reason, our skis were locked up in our friend’s house in Burnaby, so
weren’t able to get them until lunch time and by the time we got to the Duffy
it was mid-afternoon.

At first I thought the pullout we wanted was the first one after Joffre Lakes,
so we stopped at the pullout just west of Cayoosh Creek and started skiing up
the road on the other side. A little bit later, I realized that something was
amiss. We were on the wrong side of Cayoosh Creek and it didn’t look like we
were bound to be crossing it any time soon. I checked the map and we went
back to the car and got to the correct trail-head.

.. figure:: /trips/2016/wendy_thompson_sunset.jpg
   :alt: The sun setting over the hills

   The sun setting over the hills

By the time we finally got started on the right trail it was about 16:00.
A little bit after we passed the summer trail-head, we noticed the orange
rays of sunset starting to appear on the horizon. We huffed it up along the
steep-ish slope that leads to the lake. By the time we got to the lake, it was
pitch black, and the pepperoni that I'd given Dorothy to stave off her hunger
was not being very agreeable.

At some point, we finally made it to the hut. It was empty and cold (the
previous residents had left early on account of an avalanche). We had a
late dinner and hit the sack. The next morning we slept in. I was pretty cold
in the cabin without any other bodies to warm it up, so getting out of our
sleeping bag wasn’t very nice. The stocks of firewood were pretty low, and I
didn’t want to use them all up just to warm the two of us.

That day another guy showed up. He taught us how to play golf with cards and
Dorothy and I went on a little ski trip to some saddle to the north, but it was
windy and the visibility was poor. The ski down was difficult, with a mix of
big piles of powder and wind crust. We decided that was enough for the day.

.. figure:: /trips/2016/wendy_thompson_sunrise.jpg
   :alt: Sunrise

   Sunrise at the Wendy Thomson Hut


The next morning we woke up and skied out. This time I didn’t sleep in and was
lucky enough to see a nice sunrise.

.. figure:: /trips/2016/wendy_thompson_ski.jpg
   :alt: Skiing out

   Skiing out
