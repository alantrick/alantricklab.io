================================
 Biking to the Pico de Mulhacén
================================

:trip date: 2013 July 11–12
:tags: Trip, Outdoors, 2013
:cover: /trips/2013/climbing_the_a-395.jpg

Getting Started
---------------

So I found myself in Granada, after about two and a half weeks of touring
around Spain. At that point I hadn’t climbed a mountain in almost a month,
and was starting to get itchy. I had heard that the Sierra Nevadas were nearby;
and indeed, when I when out the first morning, the Pico del Veleta was clearing
visible in the distance. One of my hostel-mates tried to convice me on the
wonders of the local tourist-trap: the Alhambra, but I would have nothing of it.

The first problem was figuring how to get up there. Apparently there is a bus
that drives up to a ski resort up there (Pradollano), but you had to be there
at the right time because it only went up once, and then down again. That is,
if it whent at all, and this was a great uncertainty. If it did go, it was
quite uncertain about how far it went. There wasn’t much information available
on the Internet, so I decided to rent a mountain bike.

I searched for rental shops and found
`Mountain Bike Granada <http://www.mountainbikegranada.com/>`_. It seemed like
a good option, so I emailed them and found out they were in Monachil. I took
the bus there, and eventually found the place (it was basically somebody’s
house), and no one was there. I guess they were out biking or something, but
after a few hours of waiting I took the bus back to Granada and when to
`Bicicletas la Estacion <http://www.bicicletaslaestacion.com/>`_ instead. They
didn’t speak any English, so I had to depend on hand gestures and repeating
in broken Spanish: *alquiler de bicicletas de montaña*. After a little
confusion I ended up with a mountain bike and helmet and was off on my way.

.. figure:: /trips/2013/bike_store.jpg
   :alt: bicycle leaning beside a building

   Got a bike

The Approach
------------

By this time it was already past 1pm and I had a day an a half left. I was
a little worried by the amount of time I had, but I figured I would at least
give it a solid try. Having been in Monachil, I new there was a little
grocery store there so I figured I’d go there to stock up on food and water.
As I rode my bike, I started to realize just how hot it was. It was siesta
time. And to make matters worse, the grocery store had decided it was siesta
time too. So, no food and low on water. I looked around and saw a map that
indicated there might be a store/restaurant further up the road.

The next three hours were absolutely gruelling. The road, Carretera de El
Purche, switchbacks its way up the side of the valley. The region is dry and
hot. It is gorgeous, but it is not a good place to be out of water. The
valley is full of little farming operations, and there a numerous irrigation
pools. After a while I began to greadily eye the pools of water. I kept
pushing, and fortunately stumbled across an inn in the middle of nowhere,
around El Purche. I mumbled something about *agua* to the guy, and a euro
and change, and I got two litres of water which I immediately drunk.

.. figure:: /trips/2013/above_monachil.jpg
   :alt: bicycle on the side of the road on a hill

   Above Monachil

Shortly after that, the road joined the main highway, A-395. As it was getting
later in the day, and I was getting higher up the mountain, the surroundings
had cooled significantly and I was now able to bike at a solid pace. I came
across a gas station and gobbled up some ice cream and picked up some more
liquids, a sausage, and a mystery sandwich.

.. figure:: /trips/2013/climbing_the_a-395.jpg
   :alt: bicycle on the side of the road further up a hill

   Higher up

With the extra calories and water, I was feeling much better. I saw a few
road bikers coming down the road, biking up to Pradollano seems to be a
popular activity among skilled road cyclists. I was surprised to find little
houses and farms sprawled throughout the area. When you get to Pradollano,
stay on the A-395. At this point, the road switches back to gain the ridge
line and provide some great views.

The road goes back behind the ridge and back towards the ski resort. The sun
was beginning to set, and I continued on with the little light that existed.
Being the summer, the ski resort was mostly uninhabited, but there was a little
activity, and a few lights here and there.

.. figure:: /trips/2013/dusk_in_sierra_nevadas.jpg
   :alt: sunset

   Sunset

The road continued above the ski resort, switching back to gain the ridge
before. After a few buildings there is a gate, at which point the road
becomes broken and rough. The wind on the ridge was significant, and I
decided to go to bed and continue when I found some light. I found a nook
behind some rocks, at the base of a small hill with a small observatory on
top; and I set up my sleeping bag there there. There were some goats around
and some dogs which barked for a while upon my arrival, but after about 10
minutes they gave up.

.. figure:: /trips/2013/hoya_bivy.jpg
   :alt: bivy site

At first light I got up ate some food, and went on my way. I had hoped that
the wind would have calmed down, but I suppose that is just a common feature
of the area. From this point, the road continues winding up the ridge towards
Pico del Veleta until it reaches the Refugio de la Carigüela: a small stone hut.

A Snow-covered Road
-------------------

I went to go take a break at the shelter and a found a cyclist already waiting
there. It had been his plan to bike over the Nevadas and down to Almería.
Unfortunately, as was now evident, the route from the shelter onward was
covered with snow, and he decided to turn around and make his way around
another way. I waited around for a bit, and ate the sausage. I have no idea
what the sausage was made of, but it was probably one of the worst sausages
I have ever allowed myself to digest. I was hungry though, and beggars can’t
be choosers.

From there on, I continued along carrying my bike, or to be more precise,
sliding down the snow slope with my bike. At this point, the road was
completely covered by a 30 degree snow slope, punctuated by bits of rock
here and there. I kind of awkwardly made my way down the slop to where the
“road” began to traverse across here. The road here was still covered in
many spots and so I was half riding my bike, and half carrying it.
Unfortunately the slope continued to steepen and carrying the bike became
very difficult, and I had to proceed on foot.

.. figure:: /trips/2013/road_to_mulhacen.jpg
   :alt: steep snow covering the mountain road

   The snow covered road


To be continued ...