================================
 Mount Rainier (Emmons Glacier)
================================

:trip date: 2014 August 30–September 02
:cover: /trips/2013/rainier.jpg
:party: Alan Trick, Bala Kumar, Clayton Matthews;
        Oudi Cherfi, John Spence, Andre Pipas
:summary: We climbed Rainier, but then some of our friends fell into
          a crevasse.

Friday
======

We set off from Vancouver early in the morning on Friday. After a long,
and uneventful drive we stop at the ranger station on Sunrise Park Road,
nab some permits, and park at the White River Campground. After loading
up our packs, we start hiking up the trail. It is generally a pretty
good trail, and along the way there are nice sightings of Little Tacoma
& Rainier. One we passed the campsite at Glacier Basin, the trail became
a little more difficult, and we arrive to the base of the Inter Glacier
pretty tired.

After a short nap we started to walk up the glacier. The Inter Glacier
has lots of bare ice and patchy visibility but we make it up with little
difficulty. We get little off track and take the first trail we see
which heads up to Steamboat Prow. We are rewarded with good views, but
then have to take a crumbly trail down shitty class 3 rock.

Camp Shurman is a welcome sight. One advantage of climbing Rainier in
September is that fewer other people are doing it, and you don't have to
fight for camp site. The regular campsites all have rock wall about
30–50 cm high around them, which was greatly appreciate since the wind
never really got below 30 km/h while we were there and was often a fair
bit higher.

I would like to make a particular mention of the outhouse there. It is
built entirely out of stone and (presumably) mortar. This makes it very
solid and strong, and here is a story about why that is a particularly
good thing.

Friday evening turned out to be quite windy—windy enough to blow
someone right over if they weren't a little bit careful. I also had to
go to the bathroom on Friday evening. I made my way down from the tent
area, around the ranger station, and to the outhouse, which stands right
beside a sort of precipice which drops down to the glacier about 20
meters (3 stories) below it. The door itself is latched shut by the
sheer strength of the wind blowing across it. A bit of a tug and I get
inside the outhouse. The door slams behind me.

It never really struck me how much sensory deprivation there was in a
lighthouse. I couldn't see a thing, and all I could hear was the howling
wind outside. For all I knew, this outhouse could actually be an orbital
re-entry vehicle and it would have all seemed the same. I was very glad
that the outhouse was solid and that I didn't have to worry about it
accidentally flying off the mountain with me in it.

Saturday
========

After about 5 hours of sleep, we woke up at midnight to head for the
summit. Most people would probably refer to this time as “Friday
evening”, but it is technically Saturday, and it makes my section
headers easier that way.

We headed straight up from the camp, not really sure where the route
went. Eventually, we went up a little tricky slope that took us to the
“Causeway” (the big ramp just to lookers left from camp). We continued
up to the shoulder at the top of the ramp. At this point our teams split
up, at first we thought they were probably turning around, but then
later we saw them trailing behind us. [#f1]_

The rest of way up could certainly be described as a slog, if you are
prone to use such words. The crusty layers of ice & snow made the plodding
a made the going slow, and I found myself chopping footsteps to make it go
faster for the people behind us. The large number of crevasses also made
route-finding tricky, but Clayton did a good job of finding a way around them.

.. figure:: /trips/2013/rainier.jpg
   :alt: Climbing Rainier

   The view down to Camp Shurman from the bivy site just above the Causeway

Eventually we made it to the summit. It was a pretty solitary place, that
day. I’m not sure if any teams had made it up from any of the other routes,
but if they had, there was no sign of them. The winds were unfriendly though,
and it was already past noon, so we quickly turned around.

As we were coming down we met the other team, just beyond the bergshrund—at
the start of the vast summit dome. Much to our surprise, they seemed
to be in good spirits. I had been a little concerned by how late they
were up there, but two of the people in the team were strong,
experienced mountaineers so I figured everything was good. ...

We headed down the mountain and tried to set an obvious path on the way
down so that they wouldn't have difficulty on the way down. There was
one point on the way down were we had an exposed traverse about 10m over
a large crevasse and the snow quality was pretty shitty (thin crust &
6cm of powder on top of ice). I remember thinking that it would be a
really bad place to fall, but it wasn't that steep so I didn't really
think too much of it. The tiny snow bridges elsewhere on the mountain
seemed a lot more terrifying.

It wasn't until we got near to camp, about 17:00, that we spotted our
friends again. They were much further up the mountain than they should
have been (about ⅔ of the way). We got back to camp and started
eating dinner and boiling water and keeping a watch on them. They made
slow, but steady progress down. We would loose sight of them every now
and then as they moved behind a serac or some such feature. Around 19:30
we lost sight of them again, just above that traverse. After about 1
hour we started to get worried. We began looking at our options and
preparing to climb back up and look for them. The ranger who was
supposed to be at the camp wasn't there, and no one was listening on the
emergency radio. We called 911 and they connected us to the parks staff
who said there was a ranger over on the other side of the mountain who
would come over as soon as possible.

About 22:00 we were all suited up again with some emergency gear and
left camp. A couple other guys who had climbed Rainier that day say
stayed back to keep an eye on us (they were more tired and we didn't
want to endanger too many people). We got up to about 100 meters short
of where these guys were and one of the guys on our team completely ran
out of energy. I was really frustrated and wished I could continue on my
own, but obviously that's not how it works. We ended up having to
short-rope him down back to camp.

Sunday
======

On our way down, at 1:00, we ran into a party climbing the mountain. We
told them about the accident and where the group was. They continued up
and found the party at around 4:00. [#f2]_ They sent out a 911 call,
but were unable to reach the group because they were on the other side of
the crevasse. This time, 911 notified search & rescue.

Around the same time, a ranger gets into camp. I woke up and talked to
him shortly, but told him our group had already been up the mountain 1½
times and were probably still too tired to reach them. About 6:00,
another party started going up, and the ranger began to build a helipad.
The second party got there around 9:00, and one of them was able to be
lowered into the crevasse and bring them some extra jackets and water.

Slowly, but surely, the rescue effort was organized and at about 13:00,
they started long-lining our friends out of the crevasse. We waited in
camp as the ranger had instructed us, finally, about 17:00 after
everyone had come off the mountain, we were told we could leave and took
off with our friend's camping gear strapped to our packs. We reached the
car about 21:00 and then visited our friends in the hospital. [#f3]_ We
finally got back to Vancouver about 7:00 on Monday, having had about 6
hours of sleep in the last few days.

.. rubric:: Footnotes

.. [#f1] From what I gathered later on, Oudi was suffering from a bit of
         AMS, and Andre gave him some dexamethasone, and then they continued.
.. [#f2] As it turned out, the fallen party was quite lucky. The crevasse they
         fell into was about 40m deep, but they bounced off one side onto a
         small ledge onto the other side 20 meters down.
.. [#f3] One guy was pretty much fine, just bruised. Another one had a few
         bruises to his brain and no recollection of the entire trip, but was
         otherwise find. The third guy had a broken collarbone/dislocated
         shoulder, broken ribs, some minor head trauma, was suffering from
         bad hypothermia, and had limited feeling in one arm for about a year.
