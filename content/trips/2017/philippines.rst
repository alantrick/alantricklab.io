=================
 The Philippines
=================

:date: 2017-5-15
:trip date: 2017 May 11–23
:party: Alan Trick, Dorothy Trick
:tags: Trip, 2017, Vacation

or *Showing Dorothy my homeland*

Prelude
=======

After the biting cold winds of Iceland, and the moderate climbs of
California, it was time to go somewhere hot. Somewhere hot enough that
whenever Dorothy is stuck shivering at a belay on a frozen waterfall,
she can remember “at least I’m not in ”.

Taking Dorothy to the Philippines was pretty high on our bucket list,
and so Dorothy had been watching Skyscanner for cheap deals. We finally
settled on a fortnight near the beginning of May.

After a very delicious dinner at Luke and Lisa’s and a short train ride
to the airport, followed by a some less delicious meals and a much
longer fight across the Pacific we arrive bright and early Manila at
9:30 on May 12.

Faith Academy
=============

One way to not have jet lag is to simply not give yourself the chance to
be jet-lagged. So we set straight off, hiring a taxi to Cainta. We had
him drop us of at the golf ball, below Faith on Ortigas Extension. The
area is a lot more crowded now, boasting a McDonald’s and a Jollybee. In
fact, it’s difficult to even see the golf ball.

We got our ice cream and hopped on the Valley Golf shuttle bus to the
bottom of Penny Lane and then walked up the hill to Faith. After a bit
of wandering around, we found Tad Wulliman.

The high school had the day off on account of the Junior-Senior, so the
place was pretty much dead. Tad took us around around and showed off the
new developments: an impressive concert hall, a beautiful prayer room
with a large stain glass window, a new chapel/meeting hall, the science
rooms that the old chapel hall half turned into, and the pool. We also
ran into Mr. French who was happy to hear about how Lisa was doing, and
told us how she was his star pupil; and Rita, the office secretary, who
said she remembered Phyllis because of how well she spoke Tagalog, and
how she would chat with the Filipino staff.

At the end of the day, the Wullimans took us out for a most delicious
dinner at Max’s before dropping us off at the LRT.

Corregidor Island
=================

The next day we set of at 6:00 for the Corregidor Ferry. Fortunately, we
took a taxi, since we arrived at the ticket office with about 5 minutes
to spare. Unfortunately, we were only able to get “The Package” which
cost 5000 PHP. I guess Sun Cruises has a bit of a monopoly on transit to
the island. By the ferry terminal there was a cafe and I ordered some
tapsilog to go, which Dorothy really enjoyed. The boat ride was uneventful,
except that apparently “Bawal manigarilyo” means “Smokers go to the back
of the boat.”

Once on the island we hopped on a trolley bus. On the way up we saw a
few monkeys and visited Battery Way, Grubbs, and Hearn, which have all
seen some restoration in recent years. For lunch, there was this unusual
thing called screwpine juice. It tasted kind of like a mixture of
coconut juice, sugar, and some intense mystery flavoring.

After lunch we visited the main memorial on Topside and the museum. Then
we drve down to Tailside, glancing at Battery Geary along the way.
Tailside was mostly boring, except for the Japanese Memorial with its
religious poles and AA guns.

The Filipinos were unabashed in their use of selfie sticks, and one girl
was even one girl was taking selfies with a tablet. Haven’t they learned
how uncool this is? [#f1]_ Dorothy also saw a her first
Filipino toad roadkill and was surprised by how large it was. We ended
up hanging out at the docks for a while, listening to the coastguards
sing karaoke.

After returning to the mainland we walked around the Mall of Asia for a
while, grabbed a mango shake, and ate tacos & calamaris at Chillis.

Sunday
======

On Sunday, we went to GCF Ortigas. We went down to Sen. Jose O. Vera
road, expecting to find a jeepney to take us to Ortigas Center, but
after watching them all turn right just before getting to us, we gave up
and did taxi.

GCF had their Mother’s Day service, and Dorothy got to hear the “Welcome
to the Family” song. Afterward, we took a short bus to Cubao where we
found a market and bought mangoes and bananas. We took the jeepney back
to Horseshoe Village and ate the mangoes in the back yard of the
guesthouse, under the mango trees.

Thanks to the Corregidor trip we were starting to run low on cash, so we
went to Robinsons Magnolia to get cash and ended up sharing a halo-halo
at Adobo Connection. We went to get some food at the supermarket there.
We stumbled upon two adjoining samples booths: the first was selling
Pocky (a sugary candy), and the second was giving blood glucose tests
and selling special low-sugar foods for diabetes patients.

Going to Puerto Galera
======================

QOTD: *The nice thing about sweating is that you don’t need to pee as
much*—Dorothy

On Monday, we got a chance to eat breakfast at the Guesthouse. Bill
Hall [#f2]_ happened to be there, so we reminicsed about Nasuli, and Bill
told us a little about how things were going on down south.

After settling our account at the guest house we took the jeepney to
Cubao to get mangoes and a bus to Batangas. Unfortunately, the bus
turned out to be about a kilometer away from the mangoes. We picked up a
“guide” along the way, which did save some time asking for directions.

The bus terminal was actually just a shuttle bus to the actual bus and
the shuttle bus tried to pack as many people into the possible. He had
to shut the back door three times before it would stay shut.

The bus ride and boat ride were pretty forgettable. We arrived at
Talipanan beach about 16:00, ate our mangoes, and lounged around. The
resort has a bunch of games. One of them is a Chinese deck of cards
called `Red Years` dedicated to the Cultural Revolution. The cards all
have paintings of smiling Chinese and captions such as “Young
intellectuals go countryside to accept reeducation of pesant living in
or below poverty level” and “Guard against atom, against chemistry, and
against bacteria.”

Talipanan Falls
===============

Tuesday was mostly a pretty lazy day. After breakfast we did the hike up
to Talipanan Falls. We got a few guide requests, but they were
successfully avoided. At first I was worried that the falls might be
dried up, because the river seemed empty, but as it turned out
everything was in great shape. We were fortunate enough to have the
whole place to ourselves. I’d forgotten about the slide, which was a lot
of fun. I guess Filipino tourists like to sleep in, because as we were
leaving a bunch of people were coming.

We left by a different way as to not have to interact with the
basket-weavers in the village below who were trying to get us to buy
things earlier. Back at the road we went to a sari-sari store and I
taught Dorothy to say “Magkano ng Coke”. It turns out that Coke is still
very cheap here if you buy it from the right places.

That evening we went to Luca’s restaurant and shared a pizza. The pizza
was delicious, but we got swarmed by termites; the butikis in the roof
were having a heyday.

Anniversary Leeches
===================

The next day was our second wedding anniversary, and I had persuaded
Dorothy to hike up to Mount Talipanan or Malasimbo. To escape the heat
we got up and left at about 5:00. We had gotten some food at the
sari-sari store consisting of chocolate soda crackers and sugar-coated
butter crackers: a true breakfast of champions.

We walked to the trail, about half way between Talipanan and Aniuan.
While hiking up though the village on the hillside, one intrepid man
tried to be our guide, he didn’t even have any water, apparently the
trail was unsafe. We soon learned why, as we came upon a squad of
chickens lying wait in the bushes. The chickens mobilized, aiming to
disoriented us with frenzied movements. We prevailed; however, and since
the chickens are poor mountain climbers with their stubby legs, they
were soon left in the dust.

The hike up follows a prominent ridgeline, which makes for spectacular
scenery, it also means you’re in the jungle less, and in the sun more.
About a quarter of the way up we, meet our second trial: a cattle
pasture. Due to the early hour, the cattle were off sleeping, and only
real danger was the cow pies.

Further up, the jungle and cogon grass got thicker and taller. At one
point the grass was about 2 1/2 meters tall. There were all sorts of
birds and insects about. I saw something that was the shape of a bee,
but was completely orange. I’m pretty sure it wasn’t actually bee, maybe
it was a hoverfly. Later, Dorothy got something else on her leg that
looked somewhere between a caterpillar and a millipede. She was pretty
sure it would tunnel in and devour her from inside, but I think it was
just looking for a leaf to chow on.

Eventually we arrived at the base camp site for Mount Malasimbo—a small
tuft of ground where the grass was short. We sat down for a well
deserved break. While I was checking our progress on a map, Dorothy
noticed half a dozen little black guys on her legs and shoes. She does
the usual “get this thing off me.” I go to work, only these guys don’t
come off easily, and they’re slimy, and Dorothy’s ankle is bleeding.
They were obviously everyone’s favorite mollusc: a leech. I went to work
pulling them off her legs, and then off my hands (they stick to
everything). A few guys were on Dorothy’s socks and one in her shoe. Due
to my taller socks and hairy legs, none had actually managed to get on
my skin, and the few stragglers on my socks and shoes were easily
removed.

The leeches were enough for us, and as soon as Dorothy’s ankle stopped
bleeding, we headed down the mountain. The rest of the day was spent
eating food and having a very long siesta.

That evening, on the way back from dinner, Dorothy saw some perfectly
normal sized frogs. She commented on how large these perfectly normal
sized frogs were. [#f3]_

White Beach
===========

On Thursday, we went over to White Beach by hiking along the beach. I
had fun climbing the rocks in between Talipanan and Aninunan Beach.
Aninunan Beach is quite nice. Most of it is backed by a large wealthy
property, so it hasn’t seen the frenzied development of Talipanan Beach.

White Beach is much like it used to be: crowded and covered with little
stores. We went swimming and had lunch at a place called “foodtrip
chicken inasal.” It was quite good, and the prices were a fair bit
cheaper than at Talipanan Beach.

We ended up taking a tricycle back. At first the guy wanted 150, I guess
he figured that if I at least met him half way, he would have done well,
and I would have thought I got a good deal. At any rate, we bartered
(read, I picked my price and stuck to it) down to 50. Not sure if that’s
a good deal or not, but it was palatable.

As had become custom, we spent much of the afternoon doing siesta again.

The Road to Batad
=================

On Friday, we left Talipanan Beach. We took a tricycle into the city
where we came upon a sign listing standard tricycle fares. Apparently
even with our bartering, we were getting horribly overcharged. Oh well.

We walked to the market, bought two mangoes, and then went for lunch at
the unusually named LubHer. It was nice to hide from the tricycle
drivers who had been trying to court us like Italian men and a beautiful
girl, with tired pickup lines like “Where are you going? White Beach?
Sabang?”

The rest of the trip to Batad was fairly uneventful, except for 30
frenzied minutes in Cubao switching buses and grabbing calories at
Chow-King.

Batad
=====

QOTD: *We swim, but with our sweat*—Batad tour guide returning from
Tappia Falls

We arrived at the saddle overlooking the Batad Valley at about 6:30 on
Saturday. The tricycle didn’t want to go any further and it was all
downhill anyways, so we lazily walked down.

We eventually found our lodging, the Batad Transient House, and the view
was absolutely tremendous. The building is perched at the bottom of the
upper village giving an unobstructed view of the “amphitheatre”.

After setting down and eating breakfast, we watched them slaughter four
pigs in preparation for a feast. We left part way through though,
because we were worried that they would try to stuff our faces with more
food.

We set out to explore the terraces, first heading along the trail to
Calumbo. By happenstance we ended up going lower down in what was sort
of the middle of nowhere. We asked one shop owner what was the shortest
way to the other side of the valley. I’m not sure if the way he told us
was the shortest, but it was certainly quite interesting: large,
balance-y movements with a wet and muddy rice paddy to catch your fall.

We finally made it to the far side, which cuts steeply down a sharp
ridgeline. We stopped at a store to eat a fresh, young coconut.
Unfortunately, I realized that I’d forgotten to pack the wallet just
after she had cut the coconut open. So I scuttled off back across the
terraces, this time staying high upon the Batad-Calumbo trail which went
much faster.

After the coconut we made our way down to Tappia Falls. The falls are
large and majestic, about 50 meters tall by rough estimation. The walls
surrounding the falls are covered with foilage and shrubbery which
quiver with the violent force of the falls. After a quick dip in the
fridge waters of the pool, we found a nice flat boulder to nap on a
little downstream.

On the way back to the lodging we went through the lower village
instead, which turned out to be a bit easier. The Batad locals along the
way were very friendly and helpful, and not in just a “here is some
advice you didn’t ask for now please give me money for my ”, but
genuinely helpful.

That evening we ate a large helping of chopsuey and a somewhat unique
pizza. Some local girls had taken to assembling the straws end-on-end
into long segments and then using them to blow air on our faces. The
roof happened to be mostly covered with moths, but the creatures were so
stationary most of the time, that they were hardly noticed. One fell on
Dorothy and girls realized that she didn’t like the moths, so the girls
tasked themselves with picking them off the ceiling with their
drinking-straw spears and introducing Dorothy to the “bird butterflies”.

Later that evening we ended up talking to Francis, who turned out to be
and old care-taker of Len Newell’s. Dorothy showed him Len’s
autobiography, and Francis went through all the pictures in the book,
telling us who everyone was, and who they were related to. Irene, the
lodge owner, invited us to go to the church that Len founded.

The next morning, we met Margaux [#f4]_ and Vicente. [#f5]_ They had come
in late the night before, in the dark and in the middle of the rain.
They offered to give us a ride back later in the day which meant we could
go to church.

The church was a good 15 flights of stairs [#f6]_ from the lodge, so we
were plenty warm by the
time we got there. The church itself is in a very simple concrete
building, with simple wooden benches. They all sing from translated
songbooks, and they sing quite well. Throughout the service (and
particularly the prayers) we kept hearing them say “apple juice” and at
one point even “pineapple juice”. This was a little confusing for a
while, but I eventually figured out that it was “Apo Dios” (English
“Father God”). After the service the congregation was very welcoming to
us.

One Margaux and Vicente returned from their hiking we hiked back up to
the road and drove out. We had a few hours in Banaue, so we went to the
museum there, and then went for an early dinner.

Resting in Manila
=================

The night bus from Banaue dropped us off in Cubao at 2:30 on Monday. We
hadn’t expected it to be this early, and ended up spending 4 hours in a
budget motel. Fortunately the bed and pillows were made with plastic.

Once we woke up, we wandered around Cubao getting breakfast and mangoes.
We had planned to hang out at Starbucks, but the mall it was in didn’t
open till 11:00, so we went to the SIL guest house instead, and checked
in early.

The rest of the day was mostly spent laying around, eating, and doing
laundry. We also visited the library, and I showed her the books
there had been made for the Southern Sama. On particular
translation, including some of the works of the Hebrew prophets, was on
display in the new publications section.

Return
======

On Tuesday, we packed up, did some last minute shopping, and took a taxi
to the airport. After buying a frap at Starbucks in the airport, we had
about two pesos left, so we left it as a tip.

Our layover at Taipei was actually kinda fun, they have a small gym by
the Adidas store with a strange mechanical bull type machine. There’s
also a pretty nice library. Dorothy found Plato’s `Timaeus` and I
stole it from her after she got bored. Turns out he had really strange
ideas on the origins of species.

Photos
======

We’ve posted `a bunch of photos in a Flickr
group <https://www.flickr.com/groups/3390553@N24/pool/>`__.
Unfortunately, I don’t think it’s possible to sort them.

Footnotes
=========

.. [#f1] Just kidding, mostly
.. [#f2] My generation may remember him as the cool adult who went barefoot
.. [#f3] To be clear, they were about the size of a large adult human fist,
         which is the size they are supposed to be.
.. [#f4] Pronounced [margo]
.. [#f5] Pronounced [βisente]
.. [#f6] many distances in Batad are best measured this way
