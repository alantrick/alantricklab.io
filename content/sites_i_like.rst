==============
 Sites I Like
==============

:summary: Sometimes I look at other websites on the Internet.

Personal
========

* `Froghat.ca <https://froghat.ca>`_
* `Timothy Trick’s Website <http://timothytrick.ca/>`_
