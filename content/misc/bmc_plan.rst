==================================
 Basic Mountaineering Course Plan
==================================

:tags: Outdoors
:summary: Some personal notes about the scheduling of the annual ACC Vancouver
    Basic Mountaineering Course.

Note to Instructors
===================

This plan is entirely unofficial. While it’s been pretty consistent over the
years, if we get bad weather we may change the plan significantly.

There’s typically somewhere around 25 students and 5 instructors. Given those
numbers, we’ll typically split into groups of 5 students & 1 instructor. As a
general rule, we usually have 1 instructor talk about a skill and maybe demo
it first, and then we have the students work on it themselves in groups, with
an instructor available to give them pointers or help them with questions.

Also, if someone has a 2 foot top-clip picket or a fluke it would be nice to
bring them for demoing anchors (I only have mid-clip pickets).


Saturday
========

8:00
    Meet at parking lot, basic introductions, make sure everyone has tent
    situation, getting lost, etc.
9:00
    Hike to where the trail crosses Scott-Goldie Creek, break & talk about
    some soft skills like pace setting, clothing management.
9:30
    Hike to where the trail meets up with the ski run. Assign small groups.
    Cover climbing up on low-angle snow.
10:00
    Hike to little lake west of Brockton Substation, cover putting on ropes,
    prussic rigging, and kiwi coils. Hike up to Brockton Point, practicing
    switchbacks and positioning.
11:00
    Switch rope positions, walk up to Pump Peak, eat lunch.
13:00
    Practice snow descent and self arrest.
16:00
    Go to campsite between Pump & Second Peak, talk about camping
    considerations.
17:00
    Set up campsite, have dinner.
18:00–19:00
    Optional time for whatever (like demoing a rappel off a cliff bar).

Sunday
======

4:00–4:30
    Get up & breakfast.
5:00
    In large group, assemble with mountaineering gear. Talk about crampon use.
5:30
    In small groups, practicing crampon use and whatever skills are necessary.
    Optionally hike to peak. Slopes on north side of second peak make for good
    moderate snow practice.
8:00
    Take down camp & second breakfast.
9:00
    Anchors & crevasse rescue
11:00
    Belaying.
12:00
    Eat lunch.
12:30
    Climbing on steep snow (if exists).
13:00
    Practice falls on rope. Return to parking lot.
14:00–15:00
    Arrive at cars.

Other Didactic Notes
====================

Coiling
-------

The way we teach a kiwi coil should involve 1 locker with a clove hitch
between the exiting rope and the belay device. Extra lockers are unnecessary
and a clove hitch makes rope length adjustments safer.

Group Work
----------

In my experience, splitting up into groups, and doing practice within each
group works well. I’d like to be more intentional about this practice. Each
instructor should assess their group members and make sure they have a
sufficient level of competence with the taught skills.

References
==========

The `official course page <http://accvancouver.ca/courses/basic-mountaineering-course/>`_.
