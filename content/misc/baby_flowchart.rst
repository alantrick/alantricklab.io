==========================
 Universal Baby Flowchart
==========================

:summary: An exhastive description of how to treat all your baby's
          woes.

.. image:: /misc/baby_flowchart.svg
   :alt: Is the baby hungry? -> Yes -> Feed him/her
