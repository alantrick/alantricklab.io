=====================
 My Engagement Story
=====================

:trip date: 2014-12-7
:party: Alan Trick, Dorothy Filpula, Dan Klassen, Serena Klassen,
        Herman Sawatsky, Jesse Seymour
:tags: Trip
:summary: She said yes
:date: 2014-12-13


For those who are not familiar with the Coquihalla it might be hard to
appreciate the significance of this peak. Ottomite is a mountain
often overlooked, even though the cliffs of its south face dominate the
runaway lane that immediately follows the Great Bear Snowshed. [#f1]_

.. figure:: /engagement_splash.jpg
   :alt: Snowshowers looking onto a large hole in the ice

   As it turns out, snowshoes do not aid with swimming

An adventure would not be a true BC adventure without bushwacking, and
Ottomite did not disappoint. The first creek crossing had a few cms of
water flowing over an icy log structure, and then much of the road to
the the next crossing was still inhabited by uncovered alder and fallen
logs.

The second crossing was our first real excitement. The area around the
bridge was covered by a sheet of ice. Dorothy and I crossed the bridge
successfully, but for reasons I don’t understand, three of our group
congregated on a section of ice that was a few meters removed from solid
ground. There was a big splash; and once all was said and done, one of
my friends (who shall not be named) had to slither into his wife’s pants
[#f2]_ to avoid hypothermia.

.. figure:: /engagement_clouds.jpg
   :alt: Clouds floating over a distant saddle

   Beautiful clouds

After having lost more time than we would have liked, we set off up the
road. The road here was mostly covered with snow which made travel
significantly faster. On the way up we saw what could best be described
as a river of clouds flowing over the Zupjok-Zopkios ridge pass. It was
quite beautiful. On reaching the fourth switchback we broke off from the
road and made our way up the eaet ridge to the summit.

.. figure:: /engagement_photo.jpg
   :alt: Dorothy & I upon engagement

   Me, Dorothy, and a box of Tim Tams

We had made plans to do a Tim Tam Slam at the summit, and the
chocolate had been packed up in a plastic bag in my backpack. I gave
Dorothy the bag and she reluctantly opened it. At first it wasn’t too bad,
but then there was a piece of old webbing in the way, and then another
plastic bag, and more old webbing, and so on. She was just about to
make me open the whole contraption for her when I told her to read the
words on the webbing

.. figure:: /engagement_webbing.jpg
   :alt: webbing with words on it

   Doesn’t look very subtle anymore

She found one; it said "will", then another which
said "Dorothy", and then "you". "marry me" was buried somewhere
in there, but at that point I had already gotten the ring out and was
proposing. She said yes, of course (after all, what lady would not say
yes to the sort of man who could climb such an indomitable peak).

After that we skied and snowshoed down. The skiing was crappy, and
it got dark, and cold, but we were happy.

.. image:: /dorothy_ring.jpg
   :alt: Dorothy’s ring


.. rubric:: Footnotes

.. [#f1] For those unfamiliar with the area, I am being tongue-in-cheek.
         The mountain is lowest in the Coquihalla by a good margin. There is
         disagreement amount some mountaineers as to whether this is really a
         mountain, or just a bump that somebody accidentally named.
.. [#f2] She had been wearing two pair.
