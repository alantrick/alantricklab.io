============================
 Dorothy and Alan’s Wedding
============================

:date: 2015-3-11

If you’re looking at this page, it’s probably because you’ve received an
invite to our wedding. This page is here to give you information about our
wedding, and things surrounding it.

Reminder: RSVP by April 26. You can contact_ us by phone, email, or in person.

The Wedding Day (May 17)
========================

Ceremony (3 PM)
---------------

We will be having the ceremony at Grace Church in Abbotsford.

.. figure:: /wedding_church.png
   :alt: map of grace church

   Map of the church, building in orange

   `See full map <http://www.openstreetmap.org/#map=17/49.03996/-122.26165>`_

Intermission (4 - 6 PM)
-----------------------

Between the ceremony and the reception, the wedding party will be taking
photos. If you are looking for something to do in the mean time, we’ve
provided a few suggestions here:

* Visit Fort Langley: A town full of first-rate tourist traps

  * `The historic fort <http://www.pc.gc.ca/lhn-nhs/bc/langley/index.aspx>`_
  * `A directory of the charming local businesses <http://www.fortlangley.com/directory>`_

* `Visit Derby Reach Regional Park <http://www.vancouvertrails.com/trails/derby-reach-regional-park/>`_:
  While it is a little bit out of the way, this park is a great place to get
  out and away from the city, without actually going too far from the city.
* `Wine tasting <http://www.thefortwineco.com/>`_
* Dorothy’s Picks:

  * `Wendel’s <http://www.wendelsonline.com/>`_ in Fort Langley
  * `Little White House & Co. <http://littlewhitehouseco.com/>`_ in Fort Langley

* Alan’s Picks:

  * `Indoor Climbing <http://www.projectclimbingcentre.com/>`_ in Abbotsford

Reception (6 PM)
----------------

The reception will be held at the Fort Langley Golf Course. Please arrange
transportation from the ceremony to the reception. There is no reasonable
public transit between the two places. If you need help finding a carpool,
let us know.

.. figure:: /wedding_reception.png
   :alt: map of fort langley golf

   Map of the reception, building in orange

   `See full map of golf course <http://www.openstreetmap.org/#map=16/49.1780/-122.5984>`_


Registry
========

If you want to contribute towards our success as a married couple, we have two
registries that you can use to help us.

`Online Registry <http://www.myregistry.com/wedding-registry/Alan-Trick-Dorothy-Filpula-Abotsford-British-Columbia/789250>`_
    This registry is for general things from many different stores.
    The registry will show you where you can buy the items, but if you can find
    a better deal somewhere else, you can get it there instead, just remember
    to check it off. Some of the items on here are pretty expensive, if you want
    to get something expensive, we’d recommend trying to split the cost with
    someone else.
`Bed, Bath, & Beyond Registry <http://www.bedbathandbeyond.ca/store/giftregistry/view_registry_guest.jsp?pwsToken=&eventType=Wedding&registryId=541886885&pwsurl=>`_
    This registry has a number of household items in it, and it is a good option
    if you don’t want to do online shopping and would rather go to a brick &
    mortar store. (Update, it looks like a lot of their options are online only,
    or something. Sorry!)

Contact
=======


Email
    Alan Trick <me@alantrick.ca> and Dorothy Filpula <dorothy.filpula@gmail.com>

Dorothy’s Phone
    604-226-1802
Alan’s Phone
    604-866-5424 (warning: I get a fair bit of phone spam and
    often don’t pick up)
Mailing address
    215-2551 Willow Lane
    Abbotsford, BC
    V2S 5Z9
