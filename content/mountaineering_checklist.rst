===========================
 Mountaineering Checklists
===========================

:tags: Outdoors


Basic
=====

-  Water, Snack food
-  Sunglasses, Hat, Sunscreen, Lip Balm
-  Rain clothing, Warm clothing
-  Gloves, Socks
-  Headlamp, Spare batteries
-  Pack
-  First aid
-  Navigation aids (map/compass, gps, etc)
-  Toilet paper

Hiking
======

-  Basic List
-  Footwear (shoes, boots, flip-flops)
-  Gaiters
-  Poles

Mountaineering (Basic)
======================

-  Hiking List
-  Rope
-  Harness
-  Helmet
-  2 slings, lockers, and non-lockers

Mountaineering (Rock)
=====================

-  Mountaineering (Basic) List
-  Rock shoes or mountaineering boots
-  Alpine rack (stoppers/cams)

Mountaineering (Snow & Ice)
===========================

-  Mountaineering (Basic) List
-  Boots, light footwear (optional)
-  Ice rack (screws/pickets/hooker/tat)
-  Crampons
-  Ice ax

Overnight
=========

-  Shovel (when snow is involved)
-  Tent/shelter
-  Sleeping bag
-  Sleeping pad (and coupler)
-  Stove/fuel/pot/spoons
-  Booties

Skiing
======

-  Basic List
-  Ski boots (and stiffeners), skis, skins, ski poles
-  Avi gear (beacon/probe/shovel)
-  Goggles/clear glasses

Other
=====

-  Passport (International)
-  Pillow (Car camping)
-  Medication
-  Change of clothes to leave at the car
