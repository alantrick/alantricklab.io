===========================================
 Completely Unauthentic Paneer Tawa Masala
===========================================

:tags: Recipe
:date: 2021-02-09
:summary: Fried cheese with spices. My wife told me I was allowed to
    make this again.


As far as I can tell, paneer tawa masala, is a Hindi/Punjabi term that
translates roughly as “Fried, spiced, cheese”. The general idea with
tawa masala appears to be: protein + tomato + cream + spices. It’s similar
to butter chicken, but more frying and less cream.

This variant is somewhat simplified to make it easier for someone without
a walk-in closet worth of spices, and a little cauliflower has been added.
You can also replace the cheese with chicken if you approve of animal murder.


Ingredients
===========

* 350 g paneer cheese, in 1–2 cm cubes
* 30 ml + 30 ml oil
* 1 onion, chopped
* 200 g cauliflower, chopped
* 5 ml cumin, powder
* 10 ml garlic, crushed
* 5 ml coriander, powder
* 250 ml crushed tomato
* 15 ml garam masala
* 30 ml dairy cream or yogurt
* Handful of cilantro, chopped

Procedure
=========

* In a large frying pan, heat 30 ml oil, and then fry cheese until brown.
* Set the cheese aside.
* In the same frying pan, add the chopped onion, cauliflower, and the
  remaining 30 ml oil. Fry until the cauliflower is soft.
* Add cumin, garlic, and coriander. If your crushed tomatoes are no/low
  salt, you could supplement salt as well. Mix for 1 minute.
* Add crushed tomato.
* Add fried panner, garam masala and cream/yogurt.
* Garnish with cilantro.

This pairs well with `naan <naan>`_.
