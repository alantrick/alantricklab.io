==========
 Tapsilog
==========

:summary: A popular Filipino breakfast that consists of marinated beef,
    fried egg, and rice.
:servings: about 3
:overall time: overnight
:cook time: 45 minutes
:tags: Recipe, Breakfast, Filipino
:date: 2017-08-16

Ingredients
===========

* Tapa

  * Marinade

    * 500 g beef [#]_
    * 100 ml soy sauce
    * 60 ml vinegar
    * 30 ml sugar
    * 30 ml minced garlic
    * pepper

  * 500 ml water
  * 1/2 an onion

* Cooking oil
* Eggs
* Rice

Procedure
=========

1. The night before

   1. Cut the beef into thin slices, about 3-5 mm wide if possible. Make sure
      to cut along the grain. Ideally you want a cut of meat that has a well
      defined grain.
   2. Combine marinade ingredients in a container and store in the refrigerator.

2. In the morning

   1. Start cooking rice
   2. Put tapa ingredients in a frying pan and simmer till the water is
      boiled off (about 45 minutes)
   3. Fry some eggs with a bit of oil, you probably want to start when the
      water from the Tapa dish is getting low.
   4. When the water is gone, add a bit of oil and fry for a short minute
      on low heat
   5. Serve

.. rubric:: Footnotes

.. [#] I like round steak due to being reasonably cheap, easy to slice,
       and its general lack of fat