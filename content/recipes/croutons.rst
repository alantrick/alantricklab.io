===================
 Croutons
===================

:tags: Recipe
:date: 2019-01-25
:summary: It’s like home-made potato chips, but with stale bread
:cover: /recipes/croutons.jpg

Ingredients
===========

* 250 g stale bread
* 80 g oil (at least some olive oil)
* Spices

  * 10 ml oregano/rosemary/basil
  * 10 ml parsley
  * 3 ml pepper
  * 5 ml salt/onion salt
  * 5 ml paprika
  * 5 ml garlic powder

Procedure
=========

1. Cut bread into 1-2 cm cubes.
2. Put cubes in mixing bowl with oil, add spices.
3. Put on baking sheet or in casserole, and place in
   heated oven at 100°C (225°F) for 2 hours.

Note that many recipes suggest a shorter cook time with a hotter temperature.
It’s possible to do this, but it’s much harder to avoid burning croutons if
they’re unevenly placed.

