===================
 French Onion Soup
===================

:tags: Recipe, Soup
:date: 2016-03-08
:summary: What to do with too many onions.
:cover: /recipes/french_onion_soup.jpg

Ingredients
===========

* 2–4 onions
* 2 clove garlic
* 1/2 a stick (60 ml) butter/margarine
* 500 ml chicken broth [#]_
* 500 ml beef broth
* Shredded gruyère cheese
* A few slices of bread
* Spices (optional)

  * 2 bay leaves
  * A sprinkling of thyme

Procedure
=========

Add onion, butter, and garlic in saucepan on stove-top. Add in spices.
Cook on low-medium heat for about 20 minutes. Remove bay leaves.
Add broths. Bring to a boil and simmer for a few minutes.

Place in bowls with bread and top with cheese. Produces 4 servings.

.. rubric:: Footnotes

.. [#] If you have broth powder, it typically uses a 1:50 ratio, or 10 ml
       powder for 500 ml broth
