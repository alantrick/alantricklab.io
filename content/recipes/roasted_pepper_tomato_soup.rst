================================
 Roasted Pepper and Tomato Soup
================================

:tags: Recipe, Soup
:date: 2018-02-20
:summary: Tomato soup with a sweet little twist.

.. warning:: If you’re prone to an overly “acidic” response to foods, this
    might not be the best for you.

Like most soup recipes, the quantity and kind of ingredients can be altered
(often drastically) and still produce a perfectly fine food, but this is how
I like this recipe.

Ingredients
===========

* 2–3 bell peppers
* Saute

  * 50 ml butter
  * 1/4–1/2 head of garlic, minced
  * 1 onion, chopped

* 800 ml canned tomato
* 100–200 ml heavy cream
* 500 ml chicken broth

Procedure
=========

1. Broil peppers 10 cm from element, rolling them over as each side chars
   (about 4 minutes per side).
2. While roasting the peppers, add saute ingredients to a medium/large pot.
3. Once peppers are done, remove the skins, seeds, and stems.
4. Add roasted peppers, tomatoes, cream, and broth to pot.
5. Blend mixture.
6. Warm (don't boil) and serve.
