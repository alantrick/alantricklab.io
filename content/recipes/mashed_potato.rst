===============
 Mashed Potato
===============

:tags: Recipe
:summary: A simple recipe so I don’t have to look things up.

Note: this recipe has been intentionally made small. I’ll typically make
somewhere between a “times 4” and “times 10” portion. A “times 10” potion requires
a 6 liter pot to work well.

Ingredients
===========

* 2 potatoes (250 g)
* 1 clove of garlic, minced
* 75 ml broth
* 10 ml oil/fat [#]_
* 3–5 ml dill, paprika, or oregano (or some combination)
* Salt & pepper to taste

Procedure
=========

1. Put a pot with water on the stove to boil. [#]_
2. Slice potatoes into 1 cm wide slices for quicker cooking.
3. Once the water is boiled, put the potatoes in, and boil until the potatoes
   get soft.
4. Drain pot (you can use some of the water from the pot to make the broth
   if using dried broth cubes).
5. Add remaining ingredients: garlic, broth, oil, spices, salt, and pepper

.. rubric:: Footnotes

.. [#] I like olive oil, any good fat source will do though.
.. [#] Size wise, you need 400 ml of water per serving, and you want the pot
       to hold 600 ml per serving. So a “times 4” portion will require 1.6
       litres of water and a 2.4 liter pot or larger.
