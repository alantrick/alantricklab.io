======================
 BBQ-style Oven Roast
======================

:tags: Recipe
:date: 2018-12-1
:cover: /recipes/bbq_roast.jpg
:summary: This is a really delicious “southern” style roast. It has a lot
          of ingredients, but they’re all pretty basic.

Ingredients
===========

* 1.5 kg meat [#f1]_
* Rub [#f2]_

  * 10 ml brown sugar
  * 15 ml paprika
  * 5 ml black pepper
  * 5 ml salt
  * 5 ml mustard
  * 5 ml cumin
  * garlic (minced/mashed/powder, about ½ clove worth)

* Sauce

  * 1 onion, chopped
  * 80 ml apple cider vinegar
  * 50 ml brown sugar
  * 70 ml ketchup


Procedure
=========

1. Cooking the roast

   1. Preheat oven to 135°C (275°F).
   2. Mix rub ingredients together, and rub over meat.
   3. Put meat fat-side down in an oven-safe dish, covered (I use the large
      ceramic dish from our slow-cooker and cover it with aluminum foil).
   4. Cook in oven for 4 hours and then flip fat-side up and cook for 1 hour.
   5. Remove from oven.

2. Preparing the sauce

   1. On the stove-top, place onion and juices from roast in a sauce pan on
      medium-high heat.
   2. Once the juice starts boil off (before the onions burn), add vinegar
      and brown sugar.
   3. Continue boiling off steam until the mixture is reduced to a syrup.
   4. Add ketchup.

3. Finishing touches

   1. Brush about half of the sauce on the roast, and place it back in the
      oven, uncovered.
   2. Broil until top is lightly browned. [#f3]_

.. rubric:: Footnotes

.. [#f1] I typically get some prepared “roast” beef from the store. As a rule,
         the more money you pay, the tenderer it is.
.. [#f2] You can also just use a prepared rub from the store.
.. [#f3] I find it hard to tell, because the color of everything is
        already a dark brown. I just broil it for 4–5 minutes.
