==============
 Nanaimo Bars
==============

:tags: Recipe, Dessert
:date: 2015-10-03
:summary: This is my mother’s recipe. It has been altered to be less sweet &
          greasy.

.. note:: You need a spiral flat bottomed whisk (preferably) or a roux whisk.

Ingredients
===========

* 250 ml pecans, chopped
* 300 ml fine unsweetened coconut
* 300-400 ml graham wafer crumbs
* 1 egg
* 75 g + 25 g butter
* 125 ml granulated sugar
* 150 ml cocoa
* 5 ml + 5 ml vanilla
* 40 ml vanilla custard powder
* 30 ml evaporated mill or half & half cream
* 400–450 ml icing sugar
* Chocolate

  * 150 g unsweetened chocolate
  * 150 g semi-sweet or dark chocolate



First Layer
===========

Chop pecans, add fine unsweetened coconut and 300 ml graham
wafer crumbs.  Set aside. In separate bowl whisk egg until well mixed,
no bits of white showing. Set aside.

In stove-top type pot:

* Melt 75 g butter on low heat.
* Add granulated sugar, stir with flat whisk when sugar is mixed in
* Add cocoa.
* Heat and stir with whisk until smooth, no granules of sugar.
* If too dry, add a little cream, maybe 15 ml.
* Remove from heat and add 5 ml vanilla. Stir and then add the whisked
  egg slowly, whisking continuously.
* If you add it too fast and don’t whisk, you get bits of egg cooking due to
  the heat of the mixture.
* Add the previous mixture of graham wafer crumbs, pecans, and coconut; and
  stir well with a fork.
* If it seems runny add more graham crumbs, 50 ml at a time, until it has a
  good texture for pressing into the bottom of the cake pan—not dry, not runny.
* Press with hands to cake pan bottom, making as even as possible.

Second Layer
============

Beat together:

* 25 g butter, softened
* evaporated milk or half & half cream
* vanilla custard powder
* 5 ml vanilla
* 350 ml icing sugar, add another 50-100 ml at end as necessary to make it
  spreadable.

Spread over first layer and put in fridge to chill. Wait at least 20 minutes
before starting the third layer.

Third Layer
===========

* Unwrap and put chocolate into microwave-proof casserole bowl. Don’t allow the
  squares to touch liquid
* Microwave on power level 4 for about 5 minutes, until blocks can be stirred
  (by dry fork or knife with no liquid)
* Immediately spread on cake. After a few minutes, chill in fridge. After
  chilling for about 20 minutes, take out pan to cut the pieces before the
  chocolate gets too hard.  Note that liquid causes chocolate to seize up
  and it goes hard and can’t be spread. thus the need to handle it with care.

Good luck, it’s a lot of work but it’s worth it.
