=================
 Fried “Bulgogi”
=================

:tags: Recipe, Korean
:date: 2016-01-21
:summary: Korean marinaded meat dish

Ingredients
===========

* 400 g flank steak, thinly sliced along grain
* Optional: Replace steak with shiitake mushrooms for vegetarian option
* Optional: Chopped carrot/yellow onion
* Marinade

  * 75 ml soy sauce
  * 30 ml white/brown sugar
  * 60 ml chopped green onion
  * 15–30 ml minced garlic
  * 15–30 ml sesame seeds
  * 15–30 ml sesame oil
  * 3 ml ground black pepper
  * Optional: 5 ml finely minced ginger

Procedure
=========

1. Mix marinade together in bowl
2. Pour marinade over steak and other ingredients in a dish and cover
3. Refrigerate for at least 30 minutes, but ideally 3+ hours
4. When ready, cook in frying pan