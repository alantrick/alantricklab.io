================
 Chicken Tinola
================

:date: 2016-12-21
:tags: Recipe, Filipino
:summary: Filipino chicken-ginger soup.

Ingredients
===========

* 1 chicken, chopped into sections
* 1 medium-large onion, chopped into bits
* 10 ml minced garlic
* 2 thumbs of ginger, sliced thin
* 1.5 liters water
* 15 ml fish sauce
* spices
  * salt and pepper to taste
  * 1 bay leaf (optional)
* 1 green papaya/chayote, peeled and cut into bite-sized pieces
* large fistful of spinach

Procedure
=========

1. Saute chicken, onion, garlic, and ginger. Use oil if desired.
2. Put saute ingredients in slow cooker with water and fish sauce. Add spices.
3. 3 hours before serving, add the papaya/chayote.
4. 15 minutes before serving, add the spinach.

Troubleshooting
===============

If the papaya/chayote is difficult to skewer with a fork when you go to add
the spinach, then the pieces were probably too big. To fix this put the pieces
in a saucepan and boil it on the stove top for a while and then add it back.
