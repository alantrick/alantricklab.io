============
 Lemon Cake
============

:tags: Recipe
:summary: I have to hide this from my family

Ingredients
===========

* 240 g wheat flour, mostly white
* 100 g sugar
* 2 lemons, zested and juiced
* 15 g poppy seeds
* 6 g (5 ml) salt
* 8 g (10 ml) baking powder
* 1 g (1.5 ml) baking soda
* 230 g sour cream
* 2 (100-120 g) eggs
* 80 g oil
* 4 g vanilla
* 75 g powder sugar
* about 10 g corn starch

Procedure
=========

1. Cake

   1. Preheat oven to 350°F.
   2. Grease a 9x5 inch loaf tin.
   3. In medium bowl:
   
      1. Mix flour, sugar, zezt of 2 lemons (~ 5g).
      2. Whisk aggressively for 1 minute to incorporate zest.
      3. Add poppy seeds, salt, baking powder, and baking soda.

   4. In large bowl:

      1. Combine sour cream, egg, oil, vanilla, and 20-50 g lemon juice.
      2. Mix well.
      3. Add in dry ingredients and mix till even.
      4. Pour into greased pan.

   5. Bake for 40-60 minutes (until a toothpick comes out clean).
   6. After the bake is done, let it cool for a bit, and put the cake in the fridge to cool.

2. Icing

   1. In a small bowl, combine about 20 g juice of lemon & powder sugar till smooth.
   2. Add a little water or powder sugar to achive good consistency (like a ribbon slowly dissolving).
   3. Remove cake from fridge and drizzle the icing over the cake.
