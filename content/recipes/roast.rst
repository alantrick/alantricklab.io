===========
 Pot Roast
===========

:tags: Recipe
:date: 2017-12-29
:summary: Really low effort way to make a tasty roast

Ingredients
===========

* Main

  * 250 ml water or broth [#]_
  * 1–1.5 kg "vegetables" (potatoes, carrots, onions, mushrooms), in 2–3 cm
    cubes
  * 5 ml pepper
  * 1 kg roast
  * 1/2 head (about 15 g/15 ml) garlic
  * 15 ml Worcestershire sauce

* Gravy

  * 60 ml butter
  * about 80 ml flour/corn starch
  * pinch of salt


Procedure
=========

1. Rub pepper on roast, and put all the main ingredients in the slow cooker
   for 6-8 hours on low.
2. Remove roast to serving platter, cover with foil.
3. Get a pot and colander that fits over it. Melt butter, salt, and mix in
   about 40 ml of flour.
4. Put the colander over the pot and pour the slow cooker contents in it.
   The juices should flow into the pot while the remaining content can be
   dumped back in the slow cooker.
5. The gravy will now be very liquid. Add in more flour, bit by bit, until
   you reach the desired consistency. Be careful not to over-do it, as the
   gravy will thicken over time as well.
6. Serve

.. rubric:: Footnotes

.. [#] I've had problems with too much water when adding lots of vegetables,
       it might be a good idea to reduce/omit this.