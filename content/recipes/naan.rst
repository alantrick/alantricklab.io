======
 Naan
======

:tags: Recipe
:date: 2021-02-09
:summary: Indian yogurt flatbread, and a recipe so short you'll wonder if
    I'm running out of storage space.

Ingredients
===========

* Dough
 
  * 300 g flour
  * 150 g yogurt
  * 7g salt
  * 50 g milk
  * 250 g active sourdough starter, 80% hydration

* 30g garlic butter, melted

Procedure
=========

* In a dough pail, mix & kneed all the “Dough” ingredients.
* Rise.
* Divide into 8 pieces, with each piece:

  * Shape into a flat ball.
  * (Optional) Proof for 30 minutes.
  * Roll flat with a rolling pin.
  * Spread garlic butter on top.
  * Fry in a cast-iron pan with butter side down.

Postscript
==========

This recipe is bit “draw the rest of the fucking owl”, because there’s
thousands of internet folk who are better at explaining the process
of making bread than I am, and the climate in my kitchen resembles the
surface of Venus, so my proof times are likely to be wildly different
from yours.

Also, I think this recipe is normally made from white wheat flour, either
the all-purpose or bread variation. However, we eat an ungodly amount of
bread here, so I usually try to use in a decent amount of whole grain
flower.

