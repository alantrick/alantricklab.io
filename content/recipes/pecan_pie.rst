===========
 Pecan Pie
===========

:tags: Recipe, Dessert
:date: 2016-08-06
:summary: Sweet, delicious sugar and nuts in a pie.

This is my mother's recipe. While it involves a fair bit of work checking
the "done-ness" of the pie, it is a reliable way to make a rather tricky pie.


Ingredients
===========

* 80 ml butter (or margarine)
* 160 ml brown sugar
* 240 ml corn syrup
* 2 ml salt
* 3 eggs

Procedure
=========

1. Preheat oven to 200°C.
2. Melt butter in saucepan. Remove from heat
3. Add sugar, corn syrup, and salt. Stir.
4. Add eggs and stir.
5. Cover bottom of crusts with pecans [#]_
6. Pour mixture over the nuts.
7. Place pie in oven and turn heat down to 190°C.
8. Check every 5 minutes. When crust is browned, turn heat down to 150°C and
   cook until a toothpick (or fork) inserted in the center comes out clean
   (40–80 minutes).

.. rubric:: Footnotes

.. [#] Avoid too many pecans, as you might overflow the crust in the next step.
