=====================
 Cherry Cheese Balls
=====================

:tags: Recipe, Dessert
:date: 2014-10-04
:summary: This was based on my grandma's recipe, a good christmas tradition.
:cover: /recipes/cherry_cheese_balls.jpg

Ingredients
===========

* 500 g (2 regular size blocks) cream cheese
* 40 g (120 ml) unsweetened coconut
* 70 g (80 ml) white sugar
* 100 g (24) red glace cherries, finely chopped
* 80 g (180 ml) chopped pecans
* 120 g (200 ml) graham crumbs, mixed with 35 g (40 ml) melted butter

Procedure
=========

* Remove cream cheese from foil, place in bowl, and let sit at room
  temperature to soften for a few hours.
* Gently fold in cherries with a rubber scraper.
* Refrigerate 1–2 hours.
* Shape into balls, about 2–3 cm diameter, and roll in graham crumbs
  mixture. [#f1]_
* Keep cold till set. Pack with wax paper between layers.
* Freezes well.

.. rubric:: Footnotes

.. [#f1] Best done with two people, one doing each job.
