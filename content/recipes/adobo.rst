=======
 Adobo
=======

:tags: Recipe, Filipino
:date: 2016-10-27
:summary: A classic Filipino vinegar & soy sauce marinade.

Ingredients
===========

* Marinade

  * 1 kg meat (typically beef, pork, or chicken)
  * 60 ml (1/4 cup) white vinegar
  * 90 ml (1/3 cup) soy sauce
  * 1/2 head garlic, minced or crushed
  * half-a-dozen peppercorns or about 5 ml (1 tsp) black pepper
  * a bunch of bay leaves

* 120 ml (1/2 cup) water
* 1 onion
* (Optional) 2 medium sized potatoes, quartered


Procedure
=========

1. Through all the marinade ingredients together ahead of time. If you
   don’t have enough time to do a marinade, it can be skipped without
   too much loss.
2. Put a large saucepan on the stove-top on medium-high heat. If your
   meat is lean, you may add a little cooking oil to the pan if desired.
3. Once the pan is hot, pick the pieces of meat out of the marinade
   using a slotted spoon or a fork. Cook on stove top until the outside
   has browned a bit.
4. Pour in the remaining marinade, water, and onion.
5. Cover and simmer for 20 minutes, then add the potatoes.
6. If the water gets too low, top it up to prevent burning. Alternatively,
   if it seems too soupy, remove the lid to reduce the sauce faster.
7. Simmer again for 10 minutes or until the potatoes are cooked and soft.
