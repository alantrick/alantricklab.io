============================
 Bruschetta Chicken Burgers
============================

:tags: Recipe
:servings: 5–6
:date: 2016-06-01
:summary: Like beef burgers, but for hipsters.

Ingredients
===========

* Topping

  * A small box of grape tomatoes (200–300 g), sliced
  * 45 ml basil
  * 1 cloves of garlic (minced)
  * .5 ml salt
  * .5 ml black pepper

* Patties

  * 1kg ground chicken
  * 60 ml basil
  * 2 clove of garlic (minced)
  * 1 ml salt
  * 2 ml black pepper
  * 60 ml chopped onion
  * 1 egg
  * 15 ml dried oregano
  * 15 ml sesame seeds
  * 10 ml dried parsley

* Mozzarella slices
* Buns
* Aluminum foil

Procedure
=========

1. In a medium bowl, mix topping ingredients & set aside in fridge.
2. In large bowl mix patty ingredients. Form into patties and place on
   aluminum foil. Cook patties on barbecue, the foil can be removed part
   way through.
3. To serve, put patty on bun, mozzarella slice on patty, and 60ml of
   mixture on top.
