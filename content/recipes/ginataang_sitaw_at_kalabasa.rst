=============================
 Ginataang Sitaw at Kalabasa
=============================

:tags: Recipe, Filipino
:summary: The name basically means “Squash and beans done in coconut”.
    It is yummy and colorful.
:cover: /recipes/ginataang_sitaw_at_kalabasa.jpg

Ingredients
===========

* Saute

  * 15 ml oil
  * 3–4 cloves of garlic
  * 1-2 onion
  * 600 g meat [#]_

* Main Mixture

  * 600–800 ml coconut milk
  * 15 ml fish sauce or shrimp paste
  * 600 g squash, peeled and cubed [#]_
  * 300 g yard long beans, cut into 5 cm pieces [#]_
  * 100 g green leaves (normally spinach)
  * Salt & pepper

Procedure
=========

1. Prepare the meat, squash, and beans beforehand. In particular, squash can
   be a lot of work to peel. If you try and to it after the cooking has started
   you will fail.
2. Saute oil, garlic, and onion on medium heat (5 min). Add shrimp till cooked
   (1–2 min). Set aside.
3. Put coconut milk and fish sauce in a large pot and bring to a boil while
   stirring regularly.
4. Add squash and simmer for 8–12 minutes.
5. Add saute (which had been set aside), beans, and leaves, and simmer for
   3–5 minutes.
6. Remove from heat, add salt and pepper to taste. Serve with rice.

.. rubric:: Footnotes

.. [#] A neutral tasting meat works best. I like shrimp a lot. Chicken would
       work fine too. The meat can be left out too as well.
.. [#] Traditionally this is calabasa squash, but I substitute acorn squash
       because calabasa isn’t available here. You want to make sure it’s ripe
       or you will have a hell of a time peeling it. Make sure there’s at
       least a little orange on it.
.. [#] Regular green beans work fine too.

