=============
 Apple Crisp
=============

:tags: Recipe
:date: 2021-02-23
:summary: Yummy apple food

Ingredients
===========

* Filling

  * 1 kg apples, peeled & chopped
  * 15-30 ml thickening powder [#]_
  * 30-60 ml sugar
  * 10 ml ground cinnamon
  * 15 ml lemon juice
  * 15-30 ml chia/poppy seeds (optional)

* Topping

  * 100 g/250 ml oats
  * 100 g/250 ml sliced almonds
  * 50 g/100 ml flour or meal (course flour) [#]_
  * 50 g syrup
  * 60 g oil/melted fat [#]_
  * 5 ml ground cinnamon
  * 2 ml salt

Procedure
=========

* Preheat oven to 190 ℃/375 ℉.
* Pour a little oil/melted fat in a 2.5 liter (9x11") baking dish.
  Smear it evenly with a scrapper.
* In a large bowl, mix together filling ingredients. Pour into dish.
* In the same bowl, mix together topping ingredients. Pour over dish.
  Use the scrapper to clean out the bowl and smear the topping flat.
* Bake for 40 minutes covered with foil, and then 15 uncovered.

.. rubric:: Footnotes

.. [#] You can probably just replace this & the sugar with powdered
       sugar, if you have that on hand.
.. [#] I typically use whole grain wheat flour, but alternatives like
       semolina, cornmeal, or even almond meal/flour work well.
.. [#] I usually use butter because we have that on hand, but other
       oils/fats should work fine as long as you like the flavor they
       provide.

