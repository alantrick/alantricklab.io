==============
 Rice Pudding
==============

:tags: Recipe, Dessert
:date: 2019-2-14
:cover: /recipes/rice_pudding.jpg
:summary: A drier type of rice pudding with lots of flavor. My grandma used
    to make this all the time.

Ingredients
===========

* Wet Ingredients

  * 3 eggs (150 g)
  * 120 ml (90 g) brown sugar
  * 500 ml (500 g) milk [#f1]_
  * 5–10 ml vanilla
  * 5–10 ml cinnamon
  * 5 ml nutmeg/allspice

* Dry Ingredients

  * 150 ml (70–90 g) yellow raisins
  * 700 ml (700 g) cooked rice [#f2]_

Procedure
=========

1. Preheat oven to 175°C (350°F).
2. Beat eggs in a large bowl.
3. Add remaining wet ingredients, and mix. Make sure that the brown sugar
   isn't too clumpy. If it is, break it up before continuing.
4. Fold in dry ingredients
5. Add mixture to a baking dish. Bake in oven for 1 hour.
6. Serve hot or cold.


.. rubric:: Footnotes

.. [#f1] For extra creamy results, substitute some cream for milk. Non-dairy
         milk products like almond milk also work here.
.. [#f2] 240 ml (240 g) uncooked rice + 450 ml (450 g) water should produce
         the right amount of cooked rice.
