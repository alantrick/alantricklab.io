==============
 Chicken Soup
==============

:tags: Recipe, Soup
:date: 2016-04-22
:summary: A good way to use up an old bird carcase if you have the time.

Ingredients
===========

* Leftover chicken carcass (with no wings, see below)
* 1.5 liter water/broth
* 4 chopped carrots
* 4 chopped celery
* 1 chopped onion
* Optional: spinach/parsley
* Seasoning

  * Poultry seasoning
  * Italian seasoning
  * 3 bay leaves
  * Garlic
  * Salt & pepper

Procedure
=========

Place all ingredients in a slow cooker on low for about 7 hours or high for
about 3 hours. De-bone the chicken and remove bay leaves. Reheat.

De-boning the Chicken
=====================

Depending on the the particular method, the consistency of the carcass, and
the desired purity of the product; a great deal of suffering can be hid behind
the innocent phrase "de-bone the chicken". Based on several attempts, this is
what I believe to generally be the best strategy:

1. Turn the slow cooker off.
2. Wash your hands.
3. Get a large bowl and transfer the solid parts of the soup into the bowl
   with a slotted spoon. [#]_
4. Preferably by hand, carefully return the vegetables and meat back to the
   slow cooker, separating and discarding the bay leaves, bones, cartilage,
   and other undesirable parts.

   * The meat around spine can be particularly problematic, and you may want
     to avoid it altogether and just discard it.
   * The wings are really no good at all. They are basically little tiny
     bones with skin hanging off of them. It's highly recommended to lop
     them off or eat them beforehand.
   * It can be a little difficult to differentiate the cartilage from the
     onion.

5. Turn the heat back on for a while to sterilize the soup.

.. rubric:: Footnotes

.. [#] I don't have a slotted spoon, but a colander on top of a large bowl
       works too. In this case, excess liquid goes into the bowl and can
       just be poured back into the slow cooker.
