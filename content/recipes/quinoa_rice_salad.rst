============================
 Warm Quinoa and Rice Salad
============================

:tags: Recipe
:servings: 2–4
:date: 2016-03-04
:summary: A super delicious potato salad our friends introduced us to.

Ingredients
===========

* 120 ml sprouted rice or quinoa (or blend)
* 1 medium sweet potato, diced into bite-sized cubes
* cooking oil
* 60 ml of red onion, chopped small
* 1/2 avocado, diced
* 30 ml pomegranate seeds (optional)
* 30 ml pumpkin seeds
* 60 ml orange juice
* 15 ml apple cider vinegar
* 5 ml mustard
* 2.5 ml cumin
* Salt & pepper to taste

Procedure
=========

1. Preheat oven to 215°C.
2. Dice sweet potatoes and chop onions, onions can go in large bowl.
3. Lightly coat potatoes with cooking oil and sprinkle with salt.
4. Roast potatoes until outside is crispy (25–30 minutes). Stir potatoes
   after 15 minutes to avoid burning.
5. Cook rice/quinoa. [#]_
6. In the large bowl containing onions, add combine rice/quinoa, sweet
   potatoes, pomegranate seeds, and pumpkin seeds.
7. Stir in liquids, spices, and avocado.
8. If not serving immediately, leave avocado out and add just before serving.

.. rubric:: Footnotes

.. [#] add 240 ml water, boil for 15 minutes or til water is absorbed and
       quinoa is fluffy
