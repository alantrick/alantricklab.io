==========
 Pancakes
==========

:tags: Recipe, Breakfast
:date: 2022-07-03
:summary: Like `crepes <./crepes>`_, but fatter.

This recipe is intentionally written small. The amounts here are
probably good for about 1-2 people. I usually triple the recipe
for my family. The left-overs are popular snacks.

Incidentally, this is also a good way to use sourdough discard.
If you have a 100% hydration sourdough, just weigh the amount
you want to use, and subtract half the weight from the milk
and half of it from the flour.

Ingredients
===========

* Dry

  * 90 g flour (I like it about 60/40 whole grain/white)
  * 2-3 ml salt
  * 3 ml baking powder

* Wet

  * 1 egg
  * 115 g milk
  * 20 g oil/melted butter
  * Optional: sugar, vanilla, poppy seeds

Procedure
=========

1. Mix the dry and wet ingredients separately, any sourdough
   discard can be put in with the wet ingredients.
2. Mix dry and wet ingredients together.
3. Cook on the stove-top, spooning on about 50-100 ml of batter at
   a time. I use a low-stick pan with an electric
   range set to 2.5. Be aware that it's easy to burn pancakes with
   too much heat.