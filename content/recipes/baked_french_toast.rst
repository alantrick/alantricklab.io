====================
 Baked French Toast
====================

:tags: Recipe, Breakfast
:summary: Half breakfast, half dessert; this recipe is a great way to get rid
          of old bread. It's easier than trying to do french toast on a
          stove-top, the only downside is you have to do a little prep work
          the night before.
:date: 2016-03-04

Ingredients
===========

* 120 ml butter
* 180 ml brown sugar
* 15 ml ground cinnamon [#]_
* 300 grams of bread
* Batter

  * 6 eggs
  * 120 ml milk
  * pinch of salt

* Optional Toppings:

  * Blueberries
  * Whipped Cream
  * Syrup
  * Crushed nuts

Procedure
=========

1. Melt butter, and use it to coat a 9x13 baking dish.
2. Mix brown sugar and spices and sprinkle evenly over melted butter.
3. Tear bread into bite-sized pieces and arrange evenly in the baking dish.
4. Beat batter ingredients in a bowl, pour over bread.
5. Cover and refrigerate for about 8 hours (or overnight).
6. Preheat oven to 175°C.
7. Bake in oven till golden brown, about 30 minutes.
8. Serve with optional topping.

.. rubric:: Footnotes

.. [#] Or optionally: nutmeg, vanilla, cardamom, or allspice.
.. [#] I recommend whole grain bread. It's healthier and tastes great
       in this recipe.
