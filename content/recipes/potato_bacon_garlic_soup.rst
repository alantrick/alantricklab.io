================================
 Potato, Bacon, and Garlic Soup
================================

:tags: Recipe, Soup
:date: 2018-03-07
:summary: If you want to get rid of potatoes, this is a good way to do it.

Ingredients
===========

* 500 g bacon
* 2 onions, chopped
* 1 head garlic, skin removed
* 1 kg starchy vegetables (mostly potato, but I'll usually add carrot too),
  chopped
* water

Procedure
=========

1. In a large pot, saute the bacon. [#]_
2. Once the bacon is done, remove it, leaving the bacon grease in the pot.
   Saute the onions & garlic in the pot for about 10 minutes.
3. Add starchy vegetables.
4. Add water in the pot until this are somewhat/mostly (but not completely)
   covered. Bring to a boil and cook for about 10 minutes.
5. Chop up the bacon into centimeter-sized bits.
6. Blend the contents of the pot.
7. Add in the bacon bits.
8. If you can let it sit a bit before serving, it thickens a bit, which is
   nice.

.. rubric:: Footnotes

.. [#] I like to put a little water in, just to make sure I don't burn it while
       I'm prepping other things
