========
 Crepes
========

:tags: Recipe, Breakfast
:date: 2016-04-13
:summary: A versatile recipe for a versatile food

Ingredients
===========

* Batter

  * 2 eggs
  * 120 ml milk
  * 120 ml water
  * 240 ml flour
  * 30 ml melted butter

* Extra butter for coating pan
* Savory variation: pinch of salt and spices or spinach
* Sweet variation: 30 ml sugar and 5 ml vanilla extract

Procedure
=========

1. In a large mixing bowl, whisk together flour and eggs. Stir in milk,
   water, salt, and butter; beat until smooth.
2. Optional: refrigerate batter to allow it to settle. This makes the
   crepes less likely to tear.
3. Heat lightly buttered frying pan over medium high heat.
4. Pour about 60 ml of batter into pan, swirling to spread evenly. Fry
   till lightly browned (1–2 minutes).
5. Repeat until batter is use up.