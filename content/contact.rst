===============
 Contacting Me
===============

Email
    me@alantrick.ca
XMPP
    alan@404.city
Phone
    +16048665424 [#f1]_

PGP Key
=======

You can use this to encrypt emails to me, or to make sure that
an email from me wasn't forged.

Key ID
    AB222D2E
Fingerprint
    3E35 6E9A A553 D670 21E3 6D6C 8240 29AE AB22 2D2E

.. code-block:: txt

   -----BEGIN PGP PUBLIC KEY BLOCK-----

   mQINBFRzXYsBEADiFnopfJnB77xtMa/yhoTNHGHrzLqMOXYEOJetLspA+7I6vTW8
   U7JjkYL7tEc79VPguzfixBXK6O2S9lib58Yc8hUDw7yracEI2SDtCf5VU6NCATE3
   VdAZWyPBS8Lleu5np1LB8EgDIGljY1sZyVFh6ZNndyFL4ZvJaf4d+xevDhRpKa2w
   q4mL4k87AwmS6zIoc/K8eQo4xeXIP5QXfOs9EDgQXAV+NZO/D840VaDM+9OupuSO
   mJP3MtrMraC8NGq5zSXKIb7KfOyNicQEJH/1/tjJv2O6BcOME83cs59qdVbXfy6c
   vnJVLowwDxpCORKdqLvt6b+IDNu7auKonIlQSSlU7UNGVnh42u+SorF/iAgxQ1B2
   NDrKxpAf48zkCOA/EQsb/HnTAwIuRjoymQEdeBzjuMm70kmTm0TgHOnoqyPgdxOJ
   U7v0G8qSwG+ZorjiHwwHD5RHtlCz3H4s4csXhfyo1gGv02jkj5woDp7WMMk7l+la
   ahk3P01kTip+zd4hlhjdGMZU4MT6JnTRW7LnGW5to87mM/zvrvfAoAZwMDxRiEz8
   7lTIQzMDLwgSpu9YTZA+bH3xuDuz2v6+1BemCQHe+aY9dPdBPnNbNK53/j4QVzIK
   ikeyiq/pumvU+rjDbg+I+ImKOWmxLr2ncR3+YW7OEAXh0LhRX6Cg5gNf+wARAQAB
   tBxBbGFuIFRyaWNrIDxtZUBhbGFudHJpY2suY2E+iQI/BBMBAgApBQJUc12LAhsj
   BQkJZgGABwsJCAcDAgEGFQgCCQoLBBYCAwECHgECF4AACgkQgkAprqsiLS4+JQ//
   XImYpxzeILEDHdwGnfvOOzPPGPNUcKLt2pTH5J62D/YQX8pPpQKDtnVay6wUXGG7
   QmASlaCeGeIZLENneW0wx1FWtzGSAU+J180oLtPhH65pUk/8JGPKcDkWaW0e/Pk6
   1MaBZhFi3NCUHmfyWhhse294Ij0wMXHP/ogiMoePIF1/xNla0SfN3TVkFGgR7ypq
   q+BrzxfGKyCQD7j9zpJg5MjCIUSyj0hnNt2CLSGZOavLQJnTgKABJOnwuzq10C27
   g3Rit+4E3T8smBMNp+nsoimiUUWt4C8NexaBxYLPCcDhP1cqQyWSOtilrsilzW68
   2yg9vdczqkwkzslVGa0hAgD4PISYqp3FrzfKPvh6oNE55CCMcHLlaQyTzTWOabv7
   PBJQm9AeYX1YLXggf6gGhLVaGlR/iJ/SnlQLwm8t7sPBco0oWHJ1xtkaDugKqLNc
   OHirBXRwrygorvB+fWN6RqySZwm8jQ0HcnB2INi5fvip+c/3MRy8bZLoQxsbCyub
   GiKdAID8WDdyONiEYmO8dXH+vpcmg3Ke4me8pn1SVIRSjl4qUCvXXk1+RN/SSlP+
   7qavWL3TT6QXWZWQTpbPHdbpL9gAtlpJXOQYl6YSvqsuvAd6OhXKOdMpv/D9Nkz8
   p3TE0Jm0yFoSBoGwHcRJ5BwPBF3ingSXtYyzNLZWmy65Ag0EVHNdiwEQAPV4v0m1
   Kp41K1JGENFYREtcmdi8CohNdLr0z7msBJhdJhu4Ha5TQ7IPBwlWWn2aahwPgBkO
   XN77H+P4vx0+MSktmEywXq6QZ9D7xNdNohY2M/mmKw/eVzeqdGakM/NO40j6iB58
   3/x5ewIKfBrJm/tVhZyVmW4Bq2m2/gYYLWevRg8DonES4Cw6OZOqhJmpltCUV75x
   126zpAwN0vCX+ds6M0wn+1xElgzfrxz41Y+kNVOjR/V+cC3FUYesTWwPIJPMoznF
   80Os15CG2ifUc3SyxvZZC1q0NB+WqjxEVajXo2Lhz/LNT8X992I11aShvv2Te3B6
   WF0vq/xlwARlHxO0pvXHKKySRrQryO9ioQw8v/uzAxmaBPHxhi/o1PFlcbEwLg5X
   1RHAg0OkYtynhcYZ9XLFP3y9r8gpQe6D5F1DXQSs72KhpkvUkp7xTVKLaYSbvD0Q
   aqRlUZcqlFheAOvNXRGTE2gELPvQZr5dsKhv2e5BRFQA6z9gK4r3L618rXo2yPHM
   sS4ili4e97iSYIVyaGD7RZ5cuEo3Lh+GQGzfvxEsq7fyFOuCI/Q5xTyztnqv+9TB
   M2Xg8mYUFGKAjfT0koLWQ4ZVUw8QNJEc9TCdjY/ij5P3b8K0qktSNpqXhWDVlYfV
   7Lkz7eDBqw0fkws71cO8MxhIT59F8WMwor73ABEBAAGJAiUEGAECAA8FAlRzXYsC
   GwwFCQlmAYAACgkQgkAprqsiLS6lAxAAhnSzYEuYNtrP3N28JKv2bUN8YBaLajCD
   xThPXZBob7MJ8zpLPptckm8DyTnSB9GIiKnqwIVdY0pYCLRQrAR5Spy/QzzFD0yo
   qHaMGqnshvyjJZzF/Tr3rucvLHRsnEBhnUaD0pNyssOZvzDAq4Lgix097AOnFtsy
   4ZKdhisJLAc+tjGPbjHe//Am8Wcaez1Pd+oMUCwJD79GAvRlJ1Lx/vwNQ/fCpjD6
   qe1FlmNRXUL593l9oIAzzFt35kVroTcOawsEPWFV6P0KrM2ZRwqIy3VliCchBWPE
   RfTR2Y9L5HhVvwt6bG8l5fUh8JYjxVhRAMpgMkDFly6/Ya3oMyZDKM2ayawBjLog
   e/yAo3t9TibEf34RUjKQDY1F0wbPvvPkEh6DLPvUuno11hB1RtI264nZln0eRs4d
   yUvDJQn87PLin/N+vBYwqHneG/X4rzYeGtcTUWigvqWxwpwatlL5F98eIoO1Dij0
   sU8DEksTwBio766JlYMlRREi8+jMwckWtgDL0jS7NEUHqR+xpjgkMVHGqolzRsO9
   RiBdw2hRWD6vLbgez4kRJFNGmk6uoaEJERS8EgtP3s242kNzFpTS6jdBxxFl69Ue
   8EWySf2MMElFk1htgmOPrwGywm+ZH/u1sm8a+LqN7Y4cRtaPET+VJ6Iq37AX+br7
   4n65tnn0j/8=
   =EAA8
   -----END PGP PUBLIC KEY BLOCK-----


.. [#f1] I often ignore calls from numbers that I don't have in my
         address list. If you want to phone me, I recommend you send
         me an email first and tell me what your phone number is.


