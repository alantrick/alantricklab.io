===================
 Personal Projects
===================

:summary: A collection of things I’ve worked on that might be useful to
          other people.

Software
========

August_
    An HTML to text converter for emails. It’s written in Rust and has
    `Python bindings`_.
`Django Vox`_
    A Django app that lets admins or staff to create, edit, and send different
    types of notifications using templates. It can send notifications via
    email, SMS, Slack, Twitter, and other methods.

Unstable
--------

`Django Admin Stats`_
    A Django app that allows admins to create and display charts of the
    site’s data using the Django admin.
`Django Agenda`_
    A Django app that allow you to create and book times in a schedule.

Inactive
--------

Terminate_
    A PHP/Python library for cross-platform terminal manipulation.
Remark_
    A plain-text/BBCode to HTML library.
`Redmine SQL Reports`_
    A Redmine plugin for creating reports with arbitrary SQL.
`Django Photocontest`_
    A Django application for photo uploading, and voting with OpenID.
`Moodle YUI Menu`_
    A Moodle 1.x dynamic Javascript course menu.


Other
=====

`Navigation Workshop`_
    I did a navigation workshop for the ACC. Here are the materials.

.. _Navigation Workshop: https://gitlab.com/alantrick/navigation
.. _Django Admin Stats: https://gitlab.com/alantrick/django-adminstats
.. _Django Agenda: https://gitlab.com/alantrick/django-agenda
.. _Django Vox: https://gitlab.com/alantrick/django-vox
.. _August: https://gitlab.com/alantrick/august-python
.. _Python bindings: https://gitlab.com/alantrick/august
.. _Terminate: http://terminate.sf.net/
.. _Remark: https://launchpad.net/remark
.. _Redmine SQL Reports: https://github.com/alantrick/redmine_sql_reports
.. _Django Photocontest: https://bitbucket.org/alantrick/django-photocontest
.. _Moodle YUI Menu: http://docs.moodle.org/19/en/YUI_menu

