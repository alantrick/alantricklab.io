=============================
Do spicy foods cause ulcers?
=============================

:date: 2016-3-29
:class: BIO 112 at UFV
:tags: Paper, Science

Introduction
============

The belief that spicy food causes gastric or duodenal ulcers is still
surprisingly common. Perhaps the easiest evidence for this is the large
number of pages on the Internet devoted to dispelling the idea, like the
CDC [C2005]_. Historically, a bland diet has been the recommended
treatment for those with ulcers [RA2004]_; and for most of the 1900's,
gastric acid was believed to be the chief culprit [KI1998]_ [#f1]_. In
recent decades, however, *Helicobacter pylori* and nonsteroidal
anti-inflammatory drugs (NSAIDs) have been implicated as the primary
causes of stomach ulcers, at least within the scientific and medical
establishments. Does there remain any role for diet as the source of
ulcers?

Research
========

The role of *H. pylori* in the creation of stomach ulcers as been well
studied in recent years. Within the stomach, the bacteria produces
urease: converting urea to ammonia and raises the pH. The less acidic
environment reduces the viscosity of the gastric mucin, allowing the
bacteria to move through it [CTA2009]_ and attach to the stomach
wall. Once there, it gains protection from the harsh environment of the
stomach [JW2008]_ and produces cytotoxic factors that lead to
the formation of ulcers [DS1997]_.

The function of NSAIDs in the cause of ulcers is also well understood.
Typical NSAIDs work by inhibiting cyclooxygenase, an enzyme responsible
for the production of the hormone prostaglandin. Without prostaglandin,
the stomach fails to produce sufficient mucus, and produces excess
acid [JW2008]_.

Apart from *H. pylori* and NSAIDs there are ideopathic ulcers—those
with no specifically known cause. A review by Iijima et
al. [IKK2014]_, noted that various studies recorded rates of
ideopathic ulcers ranging from 4–40%. It may be worthwhile to regard
the values with a grain of salt because the same report identified
deficiencies in the manner that *H. pylori* diagnosis was excluded,
and it is often difficult to make sure that participants don't secretly
take NSAIDs [HL1998]_.

While there are no clear causes for ideopathic ulcers, some studies have
identified risk factors. Physiological stress [KS2002]_ is
an important factor. Iijima et al. [IKK2014]_ identified age, serious
underlying diseases (especially liver cirrhosis), and physiological
stress; while Kang et al. [KSK2012]_ did not find a significant
difference in age between patients with *H. pylori* or NSAID caused
ulcers versus those with ideopathic ulcers. The Kang et al. study also
questioned its participants about salty and spicy food consumption, but
did not mention this at all in the results.

Aside from the commonly known and studied causes of ulcers, any specific
evidence for a relationship between spicy food and ulcers is murky at
best. Myers et al. [MSG1987]_ observed that red and black
pepper can cause short-term mucosal microbleeding similar to an adult
dose of aspirin but declined to speculate on any detrimental or
beneficial effect this may have. Graham et al. [GSO1988]_
performed a similar experiment with jalapeno peppers and reported no
observable mucosal damage. Mhaskar et al. [MRA2013]_ and Kang et
al. [KYC2995]_ observed that consumption of chili peppers
seemed to be protective against *H. pylori* infection and ulcers but
the mechanism is unknown.

Capsaicin is the principle ingredient responsible for hot peppers.
Capcasin-sensitive neurons in the stomach are responsible for initiating
a protective response. Sobue et al. [SJO2003]_ found that
low–moderate dosages of capsaicin protected rat stomachs against
ethanol, but also mentioned that excessive and neurotoxic doses of
capsaicin can make the stomach more susceptible to harm.

Discussion
==========

In this light, it seems difficult to implicate spicy food in the
production of ulcers. Within a clinical context, there is no clear
evidence that links diet with ulcer formation. There may be some bias at
work here: since only cases severe enough to warrant a visit to the
doctor would be observed. The work of Myers et al. [MSG1987]_
suggests that sufficiently spicy food may be responsible for short-term
irritation and possibly ulceration. Even if this is the case, it is not
entirely clear that this is actually a bad thing. It seems likely that
extremely spicy food—maybe something on the order of “It was so spicy
I couldn't taste anything for a week”—may be neurotoxic to
capsaicin-sensitive neurons in the stomach, leaving the organ vulnerable
to harm. Whether such a high dose is likely in the human diet and
whether this could lead to ulcers is unclear.

In surveys outside the laboratory setting, Mhaskar et al. [MRA2013]_
and Kang et al. [KYC2995]_ observed a negative association
between consumption of spicy food and ulcer formation. While this gives
no insight to the actual mechanism at work, it does support the idea
that spicy food in a normal the human diet is not ulcergenic.

Finally, ulcers that people blame spicy food for may not be ulcers at
all. Non-ulcerative indigestion (dyspepsia) is a differential diagnosis
for peptic ulcer disease [MS2002]_. Without laboratory tests it
is difficult to reliably confirm the existence of an ulcer. Saneei et
al. [SSF2016]_ found significantly higher rates of chronic
indigestion among those who regularly ate spicy food. It seems quite
possible that beliefs about ulcers actually originated from other forms
of indigestion.

Conclusion
==========

Spicy food seems unlikely to cause ulcers; or at least, it's just as
likely to prevent them. *H. pylori* infections and NSAIDs are the
predominant causes of ulcers. Spicy foods may play some harmful or
beneficial role in how the stomach protects itself from harm, but it is
certainly not a common cause. The Hippocratic method of consumption in
moderation—however dated—is probably the best here.

Footnotes
==========

.. [#f1] As a curious aside, many other etiologies were suggested for
         ulcers including overbearing mothers [MS1967]_.

References
==========

.. [C2005]
    .. bib::
        :authors: CDC
        :published: 2005
        :title: Home — CDC Ulcer
        :url: http://www.cdc.gov/ulcer/
.. [RA2004]
    .. bib::
        :authors: Milly Ryan-Harshman, Walid Aldoori
        :published: 2004
        :title: How diet and lifestyle affect duodenal ulcers. review of the evidence
        :journal: Canadian Family Physician
        :volume: 50
        :issue: 5
        :page: 727-732
.. [KI1998]
    .. bib::
        :authors: Mark Kidd, Modlin Irvin
        :published: 1998
        :title: A Century of Helicobacter pylori
        :journal: Digestion
        :volume: 59
        :issue: 5
        :page: 1-15
        :url: http://www.karger.com/Article/Pdf/7461
.. [CTA2009]
    .. bib::
        :authors: Jonathan P. Celli, Bradley S. Turner, Nezam H. Afdhal, Sarah Keates, Ionita Ghiran, Ciaran P. Kelly, Randy H. Ewoldt, Gareth H. McKinley, Peter So, Shyamsunder Erramilli, Rama Bansil
        :published: 2009
        :title: Helicobacter pylori moves through mucus by reducing mucin viscoelasticity
        :journal: Proceedings of the National Academy of Sciences
        :volume: 106
        :issue: 34
        :page: 14321-14326
        :doi: 10.1073/pnas.0903438106
.. [JW2008]
    .. bib::
        :authors: John L. Wallace
        :published: 2008
        :title: Prostaglandins, nsaids, and gastric mucosal protection: Why doesn’t the stomach digest itself?
        :journal: Physiological Reviews
        :volume: 88
        :issue: 4
        :page: 1547-1565
        :issn: 0031-9333
        :doi: 10.1152/physrev.00004.2008
.. [DS1997]
    .. bib::
        :authors: D. T. Smoot
        :published: 1997-12
        :title: How does Helicobacter pylori cause mucosal damage? Direct mechanisms
        :journal: Gastroenterology
        :volume: 113
        :issue: 6 Suppl
        :page: S31-34
.. [IKK2014]
    .. bib::
        :authors: Katsunori Iijima, Takeshi Kanno, Tomoyuki Koike, Tooru Shimosegawa
        :published: 2014
        :title: Helicobacter pylori-negative,non-steroidal anti-inflammatory drug: negative idiopathic ulcers in asia
        :journal: World Journal Of Gastroenterology
        :volume: 20
        :issue: 3
        :page: 706-713
        :issn: 2219-2840
.. [HL1998]
    .. bib::
        :authors: Basil I. Hirschowitz, Angel Lanas
        :published: 1998-5
        :title: Intractable upper gastrointestinal ulceration due to aspirin in patients who have undergone surgery for peptic ulcer
        :journal: Gastroenterology
        :volume: 114
        :issue: 5
        :page: 883-892
        :issn: 0016-5085
        :doi: 10.1016/S0016-5085(98)70307-5
.. [KS2002]
    .. bib::
        :authors: Kenneth P. Steinberg
        :published: 2002
        :title: Stress-related mucosal disease in the critically ill patient: Risk factors and strategies to prevent stress-related bleeding in the intensive care unit
        :journal: Critical Care Medicine
        :volume: 30
        :issue: 6
        :issn: 0090-3493
.. [KSK2012]
    .. bib::
        :authors: Jung Mook Kang, Pyoung Ju Seo, Nayoung Kim, Byoung Hwan Lee, Jinweon Kwon, Dong Ho Lee, Hyun Chae Jung
        :published: 2012
        :title: Analysis of direct medical care costs of peptic ulcer disease in a korean tertiary medical center
        :journal: Scandinavian Journal Of Gastroenterology
        :volume: 47
        :issue: 1
        :page: 36-42
        :issn: 1502-7708
.. [MSG1987]
    .. bib::
        :authors: B. M. Myers, J. L. Smith, D. Y. Graham
        :published: 1987-3
        :title: Effect of red pepper and black pepper on the stomach.
        :journal: The American Journal of Gastroenterology
        :volume: 82
        :issue: 3
        :page: 211-214
.. [GSO1988]
    .. bib::
        :authors: D. Y. Graham, J. L. Smith, A. R. Opekun
        :published: 1988-12
        :title: Spicy food and the stomach. Evaluation by videoendoscopy
        :journal: JAMA
        :volume: 260
        :issue: 23
        :page: 3473-3475
.. [MRA2013]
    .. bib::
        :authors: Rahul S. Mhaskar, Izurieta Ricardo, Azizan Azliyati, Rajaram Laxminarayan, Bapaye Amol, Walujkar Santosh, Kwa Boo
        :published: 2013
        :title: Assessment of risk factors of helicobacter pylori infection and peptic ulcer disease
        :journal: Journal of Global Infectious Diseases
        :volume: 5
        :issue: 2
        :page: 60-67
        :issn: 0974-777X
        :doi: 10.4103/0974-777X.112288
.. [KYC2995]
    .. bib::
        :authors: J. Y. Kang, K. G. Yeoh, H. P. Chia, H. P. Lee, Y. W. Chia, R. Guan, I. Yap.
        :published: 1995-3
        :title: Chili–protective factor against peptic ulcer?
        :journal: Digestive Diseases and Sciences
        :volume: 40
        :issue: 3
        :page: 576–579
.. [SJO2003]
    .. bib::
        :authors: Masashi Sobue, Takashi Joh, Tadayuki Oshima, Hideo Suzuki, Kyoji Seno, Kunio Kasugai, Tomoyuki Nomura, Hirotaka Ohara, Yoshifumi Yokoyama, Makoto Itoh
        :published: 2003
        :title: Contribution of capsaicin-sensitive afferent nerves to rapid recovery from ethanol-induced gastric epithelial damage in rats
        :journal: Journal Of Gastroenterology And Hepatology
        :volume: 18
        :issue: 10
        :page: 1188-1195
        :issn: 0815-9319
.. [MS2002]
    .. bib::
        :authors: Mark D. Schwartz
        :published: 2002-3
        :title: Dyspepsia, peptic ulcer disease, and esophageal reflux disease
        :journal: The Western Journal of Medicine
        :volume: 176
        :issue: 2
        :page: 98-103
        :issn: 0093-0415
.. [SSF2016]
    .. bib::
        :authors: Parvane Saneei, Omid Sadeghi, Awat Feizi, Ammar Hassanzadeh Keshteli, Hamed Daghaghzadeh, Ahmad Esmaillzadeh, Peyman Adibi
        :published: 2016
        :title: Relationship between spicy food intake and chronic uninvestigated dyspepsia in Iranian adults
        :journal: Journal of Digestive Diseases
        :volume: 17
        :issue: 1
        :page: 28-35
        :issn: 1751-2980
        :doi: 10.1111/1751-2980.12308
.. [MS1967]
    .. bib::
        :authors: Mervyn Susser
        :published: 1967
        :title: Causes of peptic ulcer: A selective epidemiologic review
        :journal: Journal of Chronic Diseases
        :volume: 20
        :issue: 6
        :page: 435-456
        :issn: 021-9681
        :doi: 10.1016/0021-9681(67)90015-X


