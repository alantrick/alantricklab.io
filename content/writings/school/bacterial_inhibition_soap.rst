=================================================================================
 Antibacterial inhibition of a hospital soap versus consumer off-the shelf soaps
=================================================================================

:authors: Alan Trick, Matthew Strauss
:class: BIO 112 University of the Fraser Valley
:tags: Paper, Science
:date: 2016-4-7


Introduction
============

Soap is an important staple of good hygiene but only recent years have shown
how necessary it is for good health. As a cleansing agent and purification
ritual, humans constantly seek better ways of achieving cleanliness
[EK2000]_. As a cleaning tool, soap was first written about by
Greek physician Galen (130–200 AD) and the chemist Gabiribne Hayyan in the
eighth century [PM2011]_. However simple physical cleanliness isn’t
the only effect of washing. Science has since learned the impact of washing
hands in regards to disease prevention. Simple washing can reduce diarrhea
risk by 47% and avert approximately 1 million diarrhea-related deaths
[CC2003]_. Even in unsanitary conditions, hand washing can have a positive
interrupting effect on diseases such as Shigella [MUK1982]_. In 1874,
Hungarian physician Ignaz Semmelweis discovered that washing hands could
reduce streptococcal puerperal sepsis from 22% to 3% [MLR1997]_.

The necessity of commercial antibacterial soap is debated and even so, 76%
of liquid soaps and 29% of bar soaps were found to contain antibacterial
agents [PWH2001]_. Recent advances in antibacterial products have
produced a phenomena where antibacterial resistant strains develop
from overuse [SB2004]_. This is a worrying trend as super-bugs
can develop in such conditions [DD2010]_. Not all bacteria are bad
but those that are tend to deal severe blows to infected individuals—thus
antibacterial resistant soaps are necessary for individuals who come into
close proximity of harmful bacteria. Doctors, patients, nurses, and staff
who work in clinics and hospitals are the most likely workers to contract
a bug due to their proximity.

Hospitals should be among the cleanest locations as delicate and susceptible
patients come into close proximity with potentially contagious bacteria from
sick patients. Surveys reveal almost 6–8% of infections are acquired in
hospital [MSMGZ1986]_. Infections acquired from hospitals can harm
patients, extend stays and consume resources [EEGKS1996]_. Even with
this knowledge, some studies find that doctors do not wash their hands as
often as is advised [LA1995]_. It appears staff believe
they wash between patients 73% of the time; the reality was observed to be
only 9% of visits had a wash [JT1996]_. The unfortunate prevalence of
negligent hand-washing only reinforces the need for effective soap in
dealing with the plethora of potentially harmful bacteria hospital staff
interact with.

When antibiotic soaps are used expansively, their effects can permeate
throughout the ecological and social system and find their way back to humans.
While daily use antibacterial soaps are debatable due to the potential
of creating superbugs, hospitals are surely a place where sterility is
desirable [DD2010]_. This experiment will not compare the primary
activity of soaps and surfactants in removing detritus and pathogens from
skin; rather, it will compare various soaps antibacterial resistances. Using
gram negative *Escherichia coli* and gram positive *Bacillus subtilis*
bacteria, soaps with varying active ingredients will be compared. If
preventing bacterial infections are of primary importance in the hospital
environment, then Deb Hygenipak, a hospital sourced soap, should have a
stronger bacterial resistance against both gram negative and gram positive
bacteria than the commercially available brands tested.

Full names of products and possible active ingredients can be found in
`Table 2`_ and `Table 3`_ in the appendix.

Procedure
=========

The soaps were tested using an agar disk diffusion protocol provided by
the University of the Fraser Valley [AA2016]_. Overall, 3 trials
were performed. Deionized water was used as a negative control and
chloramphenicol disks were used as a positive control in order to ensure
proper handling and execution of procedures. Chloramphenicol is a good
positive control because it has high activity against gram-positive and
gram-negative bacteria [SS2016]_.

On the first week, 800 mL of tryptic soy agar was prepared, autoclaved for
20 minutes, and then cooled down in a water bath at 55°C. Then the agar was
poured into 40 sterile Petri dishes using sterile technique, and then left
on the bench to dry overnight. 10 test tubes of 5 ml of tryptic soy broth
were also prepared and sterilized in the autoclave along with cotton swabs,
deionized water, and filter paper disks.

The three trials were then performed on the second and third week. Using
sterile technique, two bacterial cultures (*B. subtilis* and *E. coli*)
were transferred from starter plate cultures into 1 tube of broth each.
They were then incubated overnight at 30°C (for *B. subtilis*) and 37°C
(for *E. coli*).

After the bacteria had grown, they were transferred to Petri dishes along
with soap samples and controls. The bacterial samples were transferred with
sterile technique using cotton swabs. The swab was streaked across the whole
plate, the plate rotated 90°, and then streaked once more. Soap samples and
the negative control were applied to the filter paper disks separately in
small dishes. The negative control and the Deb Hygenipak Skin Cleanser were
applied using one drop from a Pasteur pipette. The remaining soaps were
deemed to be too viscous for a pipette. For these, the filter paper disks
were mixed, using flamed forceps, in a thin layer of soap placed inside
their dishes. The soap and control disks were then transferred to the Petri
dishes using flamed forceps. For each soap and control, three dishes of
*E. coli* and three dishes of *B. subtilis* were prepared. The Petri
dishes were then incubated overnight at 30°C (for *B. subtilis*) and 37°C
(for *E. coli*).

Finally, a gram stain was also performed on bacterial broths for the second
and third tests. This was to ensure that our bacterial samples had not been
swapped or mislabeled.

Results
=======

.. rubric:: Table 1: Zones of inhibition from disks on *B. subtilis*

================  ==================  ==================
Soap Brand        Mean Diameter (mm)  Standard Deviation
================  ==================  ==================
Deb Hygenipak     16                  7.4
Live Clean        11                  2.7
T\ :sup:`3`\ 6    19                  5.8
Equate            20                  2.9
Natural Concepts  50                  13
Chloramphenicol   26                  1.9
Deionized water   6                   0
================  ==================  ==================

Against both bacteria, Natural Concepts and the positive control
chloramphenicol produced the largest zones of inhibition. Negative
control deionized water produced either a minimal zone of inhibition
or an unexpectedly large zone of inhibition. Due to either faulty procedure,
the "tidal effect" of excessive soap sample bleeding into nearby zones
or contamination, certain negative control zones were substantially large.
Their relative closeness to Natural Concepts is theorized to be the cause
as Natural Concepts produced some zones of inhibition that included the
negative control disc. All soaps were less effective against *E. coli*
with Deb Hygenipak, Live Clean, T\ :sup:`3`\ 6 and Equate all producing
reliably minimal zones. Of note, the zones were visually smaller than those
of the negative control, indicating lack of any gram negative antibiotic
resistance. Against *B. subtilis* however, the four soaps had improved
effect.

Of deviations, as zones of inhibitions increased, consistency in size
decreased. Natural Concepts had a standard deviation of 13 at an average
zone of 50 while Live Clean had a standard deviation of 0.33 with an
average zone of 6.

The Deb Hygenipak produced modest zones against *B. subtilis* but had
nearly no effect on *E. coli*.

Discussion
==========

The results obtained did not support our hypothesis. This is both because
of the effectiveness of off-the-shelf soap products and the ineffectiveness
of the hospital soap against our culture of *E. coli*. On the outset, this
result may be a little surprising and possibly concerning because of the
risk of bacteria developing resistance to household antibacterial products
and the importance of an aseptic environment in a hospital. However, the
research done here is not conclusive, and further testing may be needed.

What our results do show is a significant variation in the inhibitory power
of soaps. Triclosan, in our tests, was an extremely effective antibacterial
agent. This should not be completely surprising since Assadian et al.
[AWH2011]_ reported triclosan having an MIC between 0.5 μg/mL
(for *E. coli*  ATCC 25922) and 64 μg/mL (for *E. coli* AGT 11K).  The
Equate Hand Soap was also surprisingly effective against *B. subtilis*
considering that the container did not market itself as an antiseptic. This
may be due to the effectiveness of DMDM Hydantoin, a formaldehyde releaser
[C1988]_.

Additionally, Live Clean gave the weakest results overall, despite having
ingredients with known antibacterial function—a review by Mendel Friedman
[MF2007]_ noted that epigallocatechin gallate (an important
component of tea extract) was more effective against gram-positive bacteria
from *Staphylococcus spp.* (MIC 50–100 μg/mL) than against several
gram-negative bacteria including *E. coli* (MIC 800 μg/mL). This seems to
indicate that there is a significant difference between antibacterial
substances with high activity, and those with low–moderate activity.

The varying standard deviations in our results are also worthy of mention.
Natural Concepts had the largest standard deviations, a fact which is
probably due to its large zone of inhibition, and the difficulty of
measuring a 50 mm zone on one quadrant of a 150 mm Petri dish. Another
test giving this soap its own Petri dish could probably produce better
results. In the presence of *B. subtilis* Deb Hygenipak and T\ :sup:`3`\ 6
also had large standard deviations. This may be explained by the difficulty
in interpreting unclear zones.

So, why wasn't the Deb Hygenipak soap effective against *E. coli*? To
be fair the product's web page does not contain any particular
antiseptic claims [H2016]_. Still, the soap lists ingredients with
known antibiotic activity. Shepherd et al. [SWG1988]_ observed the
MIC of bronopol for *E. coli* to be 13 μg/mL, and Croshaw [CGL1964]_
states that bronopol is has greater activity against gram-negative
bacteria than gram-positive [#f1]_. Possible causes can be put into
two categories: either the expected ingredients were not effective,
or the expected ingredients were not there.

The active ingredients of the Deb Hygenipak soap have a similar mechanism
of action: oxidation of thiol groups and the formation of disulphide bonds
[BS1972]_ [BBB2010]_. This action is limited by anaerobic conditions,
a low pH, or the presents of significant other thiol
groups (such as from proteins in agar). None of these should have been an
effect within our experiment. Additionally, a search of the literature was
unable to find any examples of bronopol resistance.

If the ingredients are effective, then the other possibility was that
they were not present (in sufficient quantity). Chemical degradation is
a possibility, however, the Deb Hygenipak soap was stored in a specimen
container, away from sunlight, for 2 weeks at room temperature. According
to Matczuk et al. [MOM2012]_ bronopol should not significantly degrade
in that time, particularly in the presence of citric acid (an ingredient
of the soap).

A related explanation is that perhaps there wasn't enough bronopol in
the soap to inhibit growth at the given concentrations. Since the
ingredients list do not specify any amounts, it is not really possible to
know without additional testing. In a related fashion, it is also possible
that the Deb Hygenipak soap adhered strongly to the filter paper and
poorly to the agar so that only a small portion of it actually effected
the bacteria.

In the future, there are several experiments that could be performed that
may help improve results, or shed light on the details of these soaps. One
change would be to dry out the soap disks before placing them on the agar.
This would reduce the possibility of a so-called "tsunami effect" where
the rapid exit of liquid from the filter paper to the agar actually pushes
bacteria away and results in a zone of inhibition. Another possibility
would be to grow bacteria in suspension and then add different amounts
of soap to test for the Minimum Inhibitory Concentration.

More involved tests could also be performed by modifying the soaps or the
bacteria. For example, Storm et al. [SRS1977]_ notes that EDTA and
polymyxin make the outer membrane of *E. coli* more permeable. Adding
one of these may have some effect on on how the Deb Hygenipak inhibits
*E. coli*.

The experiment performed here only tests one aspect of the several
important factors in soap hygiene. One limitation of our study was that
due to the laboratory environment and time only two non-pathogenic bacterial
strains were tested. In particular, bronopol is important for its activity
against *Pseudomonas aeruginosa*  [CGL1964]_, a multi-drug
resistant bacteria responsible for opportunistic infections in
immunocompromised individuals [STK2015]_. A larger study, with
bacterial samples that are more pertinent to what would be found in a
hospital setting, is important for establishing a clearer picture of the
situation.

Another limitation of our study was that we were not studying these soaps
in precisely the way they were intended to be used. The most important
action of hand soaps is to help reduce the ability of bacteria and other
pathogens to adhere to the skin. This was not tested in our experiment.

Also relevant, is the ability of soap products to trigger allergies and
cause dermatitis. Dermatitis is not just irritating—it also compromises
the skin's ability to keep out foreign pathogens. Larson et al. [LA1995]_
describe the problem in detail.

In conclusion, there is significant variation in the ability of soaps
to inhibit bacterial growth. Products that claim to be antiseptic may
not work as intended in small amounts or at low concentrations. Proper
hand-washing technique is critical to hygene, both in the hospital and
laboratory, and at home.

Acknowledgements
================

Deb Hygenipak, the hospital soap used in this experiment, was provided
by Fraser Valley Regional Hospital. The remaining soaps and
chloramphenicol were provided by the University of the Fraser Valley.

Appendix
========

.. rubric:: Table 2: Abbreviations of soaps used
   :name: Table 2

================  ====================================================
Abbreviation      Full name
Deb Hygenipak     Deb Hygenipack Pure Unscented Foaming Skin Cleanser
Live Clean        Live Clean (Sweet Pea) Moisturizing Liquid Hand Soap
T\ :sup:`3`\ 6    T\ :sup:`3`\ 6 Antiseptic Hand Sanitizer
Equate            Equate Original Hand Soap
Natural Concepts  Natural Concepts Antibacterial Liquid Soap
================  ====================================================

.. rubric:: Table 3: Possible active ingredients of soap products
    :name: Table 3

================  ====================================================
Soap Brand        Active Ingredients (in order of amount)
Deb Hygenipak     Bronopol, Citric Acid, Methylchloroisothiazolinone,
                  Methylisothiazolinone
Live Clean        Plant Extracts (Camellia Sinensis, Lavandula
                  Angustifolia, Chameinilla Recutita Flower,
                  Rosmarinus Officinalis, Calendula Officinalis Flower,
                  Viola Odorata Flower/Leaf, Lathyrus Odoratus Flower),
                  Sodum Benzoate
T\ :sup:`3`\ 6    Ethanol, Benzalkonium Chloride
Equate            DMDM Hydantoin
Natural Concepts  Triclosan, DMDM Hydantoin
================  ====================================================

Footnotes
=========

.. [#f1] It should be noted that this study was paid for by The Boots
         Company which invented bronopol.

References
==========

.. [EK2000]
    .. bib::
        :authors: K. Ertel
        :published: 2000
        :title: Modern skin cleansers
        :journal: Dermatologic Clinics
        :volume: 18
        :issue: 4
        :page: 561-575
        :publisher: Elsevier
.. [PM2011]
    .. bib::
        :authors: P. Mukhopadhyay
        :published: 2011
        :title: Cleansers and their role in various dermatological disorders
        :journal: Indian Journal of Dermatology
        :volume: 56
        :issue: 1
        :page: 2
        :publisher: Medknow
.. [CC2003]
    .. bib::
        :authors: Val Curtis, Sandy Cairncross
        :published: 2003
        :title: Effect of washing hands with soap on diarrhoea risk in the community: a systematic review
        :journal: The Lancet Infectious Diseases
        :volume: 3
        :issue: 5
        :page: 275-281
        :publisher: Elsevier
.. [MUK1982]
    .. bib::
        :authors: Moslem Uddin Kahn
        :published: 1982
        :title: Interruption of shigellosis by hand washing
        :journal: Transactions of the Royal Society of Tropical Medicine and Hygiene
        :volume: 76
        :issue: 2
        :page: 164-168
        :publisher: Oxford University Press
.. [MLR1997]
    .. bib::
        :authors: M.L. Rotter
        :published: 1997
        :title: 150 years of hand disinfection—Semmelweis' heritage
        :journal: Hygiene und Medizin
        :volume: 22
        :page: 332-339
        :publisher: mhp Gerlag GmbH
.. [PWH2001]
    .. bib::
        :authors: Eli Perencevich, Michael Wong, Anthony Harris
        :published: 2001
        :title: National and regional assessment of the antibacterial soap market: a step toward determining the impact of prevalent antibacterial soaps
        :journal: American Journal of Infection Control
        :volume: 29
        :issue: 5
        :page: 281-283
        :publisher: Elsevier
.. [SB2004]
    .. bib::
        :authors: Stuart Levy, Bonnie Marshall
        :published: 2004
        :title: Antibacterial resistance worldwide: causes, challenges and responses
        :journal: Nature medicine
        :volume: 10
        :page: S122-S129
        :publisher: Nature Publishing Group
.. [DD2010]
    .. bib::
        :authors: Julian Davies, Dorothy Davies
        :published: 2010
        :title: Origins and evolution of antibiotic resistance
        :journal: Microbiology and Molecular Biology Reviews
        :volume: 74
        :issue: 3
        :page: 417-433
        :publisher: Am Soc Microbiol
.. [MSMGZ1986]
    .. bib::
        :authors: M Moro, M Stazi, G Marasca, D Greco, A Zampieri
        :published: 1986
        :title: National prevalence survey of hospital-acquired infections in Italy, 1983
        :journal: Journal of Hospital Infection
        :volume: 8
        :issue: 1
        :page: 72-85
        :publisher: Elsevier
.. [EEGKS1996]
    .. bib::
        :authors: A Emmerson, J Enstone, M Griffin, M Kelsey, E Smyth
        :published: 1996
        :title: The Second National Prevalence Survey of infection in hospitals—overview of the results
        :journal: Journal of Hospital Infection
        :volume: 32
        :issue: 3
        :page: 175-190
        :publisher: Elsevier
.. [LA1995]
    .. bib::
        :authors: Elaine Larson, 1994 APIC Guidelines Committee and others
        :published: 1995
        :title: APIC guidelines for handwashing and hand antisepsis in health care settings
        :journal: American Journal of Infection Control
        :volume: 23
        :issue: 4
        :page: 251-269
        :publisher: Mosby
.. [JT1996]
    .. bib::
        :authors: James Tibballs
        :published: 1996
        :title: Teaching hospital medical staff to handwash
        :journal: The Medical Journal of Australia
        :volume: 164
        :issue: 7
        :page: 395-398
.. [AA2016]
    .. bib::
        :authors: Avril Alfred
        :published: 2016
        :title: The clean (or dirty?) truth: Evaluating the effectiveness of hand soaps or household cleaners against bacteria
        :publisher: University of the Fraser Valley
.. [SS2016]
    .. bib::
        :authors: Smita Sood
        :published: 2016
        :title: Chloramphenicol—A Potent Armament Against Multi-Drug Resistant (MDR) Gram Negative Bacilli?
        :journal: Journal of Clinical and Diagnostic Research
        :volume: 10
        :issue: 2
        :page: DC01
        :publisher: JCDR Research & Publications Private Limited
.. [AWH2011]
    .. bib::
        :authors: Ojan Assadian, Katrin Wehse, Nils-Olaf Hübner, Torsten Koburger, Simone Bagel, Frank Jethon, Axel Kramer
        :published: 2011
        :title: Minimum inhibitory (MIC) and minimum microbicidal concentration (MMC) of polihexanide and triclosan against antibiotic sensitive and resistant Staphylococcus aureus and Escherichia coli strains
        :journal: GMS Krankenhaushygiene interdisziplinär
        :volume: 6
        :issue: 1
        :publisher: German Medical Science
.. [MF2007]
    .. bib::
        :authors: Mendel Friedman
        :published: 2007
        :title: Overview of antibacterial, antitoxin, antiviral, and antifungal activities of tea flavonoids and teas
        :journal: Molecular Nutrition & Food Research
        :volume: 51
        :issue: 1
        :page: 116-134
        :publisher: Wiley Online Library
.. [H2016]
    .. bib::
        :published: 2016
        :title: Hygenipak Pure Unscented Foam Cleanser
        :publisher: Deb Group Ltd.
        :url: http://debgroup.com/ca/products/selector/skin-care/hygenipak-pure-unscented-foam-cleanser
.. [SWG1988]
    .. bib::
        :authors: Julia Shepherd, Roger Waigh, Peter Gilbert
        :published: 1988
        :title: Antibacterial action of 2-bromo-2-nitropropane-1, 3-diol (bronopol)
        :journal: Antimicrobial Agents and Chemotherapy
        :volume: 32
        :issue: 11
        :page: 1693-1698
        :publisher: American Society for Microbiology
.. [CGL1964]
    .. bib::
        :authors: Betty Croshaw, MJ Groves, B Lessel
        :published: 1964
        :title: Some properties of bronopol, a new antimicrobial agent active against Pseudomonas aeruginosa
        :journal: Journal of Pharmacy and Pharmacology
        :volume: 16
        :issue: S1
        :page: 127T-130T
        :publisher: Wiley Online Library
.. [BS1972]
    .. bib::
        :authors: W.R. Bowman, R.J. Stretton
        :published: 1972
        :title: Antimicrobial activity of a series of halo-nitro compounds
        :journal: Antimicrobial Agents and Chemotherapy
        :volume: 2
        :issue: 6
        :page: 504
        :publisher: American Society for Microbiology
.. [BBB2010]
    .. bib::
        :authors: Christina Burnett, Wilma Bergfeld, Donald Belsito, Curtis
                  Klaassen, James Marks, Ronald Shank, Thomas Slaga, Paul Snyder,
                  Alan Andersen
        :published: 2010
        :title: Final report of the safety assessment of methylisothiazolinone
        :journal: International Journal of Toxicology
        :volume: 29
        :issue: 4 suppl
        :page: 187S-213S
        :publisher: SAGE Publications
.. [MOM2012]
    .. bib::
        :authors: M Matczuk, N Obarski, M Mojski
        :published: 2012
        :title: The impact of the various chemical and physical factors on the degradation rate of bronopol
        :journal: International Journal of Cosmetic Science
        :volume: 34
        :issue: 5
        :page: 451-457
        :publisher: Wiley Online Library
.. [SRS1977]
    .. bib::
        :authors: D.R. Storm, K.S. Rosenthal, P.E. Swanson
        :published: 1977
        :title: Polymyxin and related peptide antibiotics
        :journal: Annual Review of Biochemistry
        :volume: 46
        :issue: 1
        :page: 723-763
        :publisher: Annual Reviews
.. [STK2015]
    .. bib::
        :authors: Natasa Stanković Nedeljković, Branislav Tiodorović,
                  Branislava Kocić, Vojislav Cirić, Marko Milojković,
                  Hadi Waisi
        :published: 2015
        :title: Pseudomonas aeruginosa serotypes and resistance to antibiotics from wound swabs.
        :journal: Vojnosanitetski Pregled
        :volume: 72
        :issue: 11
        :page: 996-1003
        :issn: 0042-8450
.. [C1988]
    .. bib::
        :authors: Cosmetic Ingredient Review Panel
        :published: 1988
        :title: Final Report on the Safety Assessment of DMDM Hydantoin
        :journal: Journal of the American College of Toxicology
        :volume: 7
        :issue: 3
        :page: 245-277
