=======================================
 Digital Rights Management and Society
=======================================


:date: 2008-11-27
:class: CMPT 480 at TWU
:summary: DRM technology and its respective laws are harmful to society in
          that they take advantage of honest and less savvy people and
          force them to relinquish control of their property to corporations.
:keywords: DRM, ethics, society
:tags: Paper, Technology

Introduction
============

DRM is an issue which has attracted much attention in recent years. The
abbreviation DRM stands for Digital Rights Management, a euphemism for a
mechanism that places technical restrictions on distributed content that
may control its use or distribution. This has led some to label DRM as
Digital Restrictions Management instead [F2006]_.

The Motivation for DRM
----------------------

The very point of copyright law was to provide a way to compensate
creators for their intellectual creations. In short, copyright grants
the creator a significant amount of control over how his creations are
used and distributed.

Recent developments have made the enforcement of copyright law extremely
difficult. In practice, copyright holders are often compensated by
royalties, a small charge for creating or distributing copies of the
work. Until recently, infringement was a civil matter and so it was the
responsibility of the copyright owners to find and bring copyright
violators to the court.

Traditionally, creating copies has been an expensive job. Because of
this, it has been left to a small number of large publishers who had the
resources to buy the necessary equipment. Since these copies were
created in centralized locations, enforcement of copyright was
straightforward.

The introduction of electronics and electronic data brought about an
crucial change: copies could be created at no cost. The importance of
this change should not be overlooked. On one hand, the rather powerful
publishing industry stood to be completely obliterated. One the other
hand, individuals could now create and distribute large numbers of
copies on their own; and on a practical level, it was very difficult to
enforce royalties.

To answer these threats, publishers used two responses. First, they
produced advertisements to convince people that making free copies of
software was illegal. Second, they introduced technical limitations in
the software to make it difficult to copy. These were the first
implementations of DRM; and while the details of modern DRM systems are
rather different. The idea is still the same.

How DRM works
-------------

The whole idea of putting roadblocks into software to make it impossible
to use sometimes is counter-intuitive. In the same way, these
restriction mechanisms are ofter quite obtuse and are generally not
technically sound.

Early attempts where rather interesting. They were widespread, and are
generally known to any old gamers. One common trick was to ask a
question about what word appeared at a particular place on a particular
line on a particular page in the user’s manual. This was rather
annoying, especially if the user lost their manual. Eventually, users
started photocopying manuals and other methods had to be sought.

Another common tactic was to require certain media to be in the computer
at startup. Usually this ended up being a CD. The success of this tactic
was short lived as CDs could be easily copied. Even without CD burners,
the contents of the CD could be copied to the hard drive, stored in an
“ISO image”1 and mounted as a CD drive. Moreover, these sort of checks
were trivial to bypass by modifying the executable and adding the right
JMP instruction in the right place, the modified executable is called a
“no-CD crack”.

Yet another mechanism is to prompt for a serial key (which is usually
shipped with the packaging of the software. Still this is (at least
theoretically) trivial to bypass, since the information of what
constitutes a valid serial is stored in the executable itself. A good
programmer and a handy debugger can write a program to generate random,
valid serials (such a program is called a “keygen”).

These different methods of copy restrictions all have one thing in
common. They all have straightforward and well understood ways of being
bypassed. Recent developments have done little to change this. In an
interview with Rolling Stones, Steve Jobs admitted that the DRM Apple
used was never expected to be secure. The DRM had been mandated by the
music labels, and Apple warned them:

    None of this technology that you’re talking about’s gonna work. We
    have Ph.D.s here, that know the stuff cold, and we don’t believe it’s
    possible to protect digital content. [JG2003]_

DRM Law
-------

Activity involving DRM has come on several fronts. In 1998 the United
States Congressed passed the DMCA. The DMCA has been the subject of
serious controversy. Similar bills have been brought up in other
countries as well: the DADVSI in France (passed in 200x) and Bill C-61
in Canada (proposed).

The Dangers of DRM
------------------

Because of how DRM systems work, they tend to rely on obscurity in order
to succeed. This can lead to bad practices if it is not controlled. In
one such case Song BMG sold so-called “protected audio CDs” which
contained root-kits that would install themselves onto computers with
Microsoft Windows. [BS2005]_ A root-kit is malware that is designed
to install itself onto a computer so that it is undetectable. Typically,
a root-kit will install a driver that modifies the kernel and gets the
kernel to hide any traces of it from the process lists or the file
system. In this case the root-kit exploited the Microsoft Window's auto
run feature with CDs.

To make matters worse, the root-kit was also spy-ware – reporting
various bits of personal information back to Sony. Mark Russinovich
discovered the malware in 2005. After it became public knowledge, Sony
offered a patch that didn't actually remove the spyware. Then they lied,
saying that the software didn't “phone home.” Later on they updated
their patch so that it removed the spyware, but left a gaping security
hole instead.

DRM and the People
==================

Keeping Honest People Honest
----------------------------

As it has already been demonstrated, DRM systems are not technologically
sound. As a result, legislation has been put into effect to legally
enforce that which is not technologically enforceable. The problem with
this is that legislation is only effective among those who follow the
law.

Cory Doctorow brought up this argument in a speech to the Microsoft
Research Group [CD2004]_. As he points out, DRM is not aimed at
organized criminals like “Ukrainian pirates who stamp out millions of
high quality counterfeits,” they can easily bypass the restrictions and
they couldn’t care less about the law. Instead, the DRM is targeted at
normal people.

The Gap between Law and Practice
--------------------------------

When DVDs started being produced, media corporations wanted control over
their usage. The result was a DRM system known as CSS (Content Scramble
System). It used a particular encryption algorithm which required the
DVD player to have a decryption key to work. The DCCA (DVD Copy Control
Association) who created CSS, licensed these keys with the requirement
that the DVD players were crippled in certain ways.

One rather infamous requirement was the DVD region code. In short, DVD
players from one region (such as Asia) could not play DVDs in made in
another region (such as North America). CSS, however, was not
technically sound. Since the encryption key is stored in the DVD player,
the only thing keeping a consumer from seeing the key was a small amount
of technical difficulty. In 1999, Jon Johansen and two anonymous
programmers on IRC cracked the encryption system and created DeCSS. The
DCCA subsequently sued Johansen under the DMCA for distributing tool to
bypass the DRM [C2001]_.

During the trial, one of the witnesses was asked by the defense about
the practice of buying cracked DVD players in Norway. The point of the
question was to get the witness to admit to the fact that many
Norwegians buy DVD players that have been cracked so they can play
American movies as well as Norwegian ones (America is in region 1,
Norway is in region 2). It seems that a significant section of society
does not agree with the laws that have been set in place.

A Stumbling Block for the Naïve
-------------------------------

Perhaps more disturbing is the actual people who are really effected by
DRM. At it has been shown, DRM systems are being constantly
circumvented.

DRM and Loss of Freedom
=======================

Violation of Fair Use
---------------------

Copyright Law in the United States has a number of limitations built
into it including something called Fair Use (a similar thing called Fair
Dealing exists in Canada). Fair Use is an integral part to many things
that are commonly done. For example, the citations in this paper are
covered under Fair Use.

Few people would argue that Fair Use is wrong; it has been the
foundation for many creative works which build off each other. However,
DRM Law threatens to make the practice of Fair Use impossible. Let’s say
Alice wrote a journal article about some topic (say, Constituent Order
in French Noun Phrases). Bob is writing an a article on a similar topic
(say, Noun Phrase Universals). Bob wants to cite a small section of
Alice’s journal article; however, the journal is published in a PDF
document that has restrictions set that make it unable to be printed or
have the text copied.

Now under Fair Use, there should be no problem with the citation. To do
so, Bob would have to circumvent the copy restrictions (i.e. read and
re-type the text). If it were a paper document, this would be perfectly
legal, but because it is an electronic document and because of the DMCA,
Bob can not legally use this quotation.

Other Licensing Problems
------------------------

A similar problem has arisen with public domain and Free Software.
Certain software is licensed with the intention that anyone who receives
the software should have the freedom to modify it and use those
modifications.

One current example is the digital video recorder TiVo. The software on
the machine runs Linux, an operating system licensed under version 2 of
the GPL. The TiVo hardware incorporates a system that ensures that only
software made by TiVo Inc. may be run on the device. As a result, TiVo
can use the software without providing its customers with the freedoms
that the license requires them to provide. Since TiVo was originally
released, the GPL has been updated to version 3 which makes this
behavior illegal. However, software that has not moved to the GPL 3
(including Linux at the moment) is still vulnerable to this sort of
abuse.

This problem also extends to the public domain. One tenet of copyright
was to give creators a temporary monopoly on their work. The idea is
that a copyright should only last for a particular amount of time and
then the work is put into public domain so that society as a whole can
benefit from it. However, work that is protected by DRM can be locked up
even though the work has passed into the public domain.

Control of Personal Data
------------------------

The potential danger of DRM goes further as well. Documents that are
created in software can be encrypted so that only other versions of the
same software can read them. In fact, by default, that is how DRM works.
Unless the particular technology is shared with others only the original
software is able to read the document. Furthermore, according to the
DMCA it is illegal to reverse engineer software with DRM. In the event
that the software stops working, the user is completely helpless. Even
if they knew enough to fix it, or could pay someone who could, it would
be illegal.

In essence, who ever controls the software, controls the documents. As
technology rapidly changes, software that once worked becomes
unsuported, and unless the support is available to transfer documents to
a format other software can read. In an essay on the preservantion of
written data, Dr. Brad Stone notes:

    If DRM becomes commonplace and widely used, I expect that it will be a
    serious impediment to preserving course papers, since access to
    documents protected by such a system depends on the continued existence
    and operability of the software and hardware under which they were
    created, on remembering or having recorded the passwords that secure
    them, and so on. Another possible scenario is moving from College A to
    University B and discovering that documents created on College A's
    computers cannot be opened or read on University B's computers, because
    College A claims to hold the copyright of all documents created by its
    employees and has programmed the DRM software on its computers to
    enforce this claim [JS2005]_.

Giving Publishers a Monopoly on Distribution
--------------------------------------------

Apple and Fairplay
~~~~~~~~~~~~~~~~~~

When Apple first went to the media conglomerates for permission to sell
music over the Internet, they came across an agreement contingent on
Apple protecting the content of the music with DRM. According to Apple
[JG2003]_, the media conglomerates were warned that the DRM would
not work. Nevertheless they plan went forward. It is difficult to say if
this was Apple's plan from the start, but Apple has taken advantage of
the control provided by the DRM to disable other companies from
competing on an equal footing.

In 2003, Apple opened its online store (then called the iTunes Music
Store). At that time access to all the music on iTunes was controlled by
FairPlay, a DRM system that encrypts AAC audio files. These files were
only playable within Apple's media player, iTunes, and their portable
media player, the iPod. Because of this, consumers who purchase music
from Apple are forced to use Apple's products and are unable to play
their music on third-party media players.

One media corporation, RealNetworks, saw the problem with this. At first
they tried to license the software from Apple, but Apple refused citing
fears that licensing the DRM software might make it easier for people to
find ways to circumvent the DRM.

Eventually, in 2004, RealNetworks reverse engineered Fairplay so that
music from their media player could be played on the iPod as well. Apple
retorted, claiming that RealNetworks had “adopted the tactics and ethics
of a hacker to break into the iPod®” [P2004]_. In the end
RealNetworks had to back down because DRM law did not allow them to
inter-operate without Apple's consent.

Concerns over this monopoly has caused several countries to raise their
eyebrows. In Norway, a group in charge of investigating the case has
declared Apple's DRM illegal [O2007]_. Since then, Apple has done
nothing to comply with Norway's mandates. In February 2008, Norway gave
them a deadline of November 3 to comply. As of November 7, however, they
have done nothing [KB2006]_.

Microsoft PlaysForSure/Zune
~~~~~~~~~~~~~~~~~~~~~~~~~~~

In response to Apple's Fairplay technology, Microsoft developed a
similar system called PlaysForSure. The chief difference from Fairplay
is that Microsoft licensed PlaysForSure to third-party vendors. In
theory, someone could buy music from any of the licensed vendors and
play it as long as they had a certified player.

Everything gets to work together. It sounds like a great solution – too
good to be true. It was. The same company who created PlaysForSure
abandoned it in 2006 when they came out with their new portable media
player, the Zune. For reasons which are unknown, the Zune uses a
separate DRM system that is incompatible with
PlaysForSure [GD2008]_.

Conclusion
----------

The primary argument in support of DRM has been economic: that DRM is
necessary in order to prevent copyright infringement and piracy.
However, the basis of this argument is rather shallow and it does not
appear to be backed up with solid empirical evidence. In fact, in the
past few years, several publishers have backed away from using DRM
because it hurt their bottom line.

Random House Audio is one such publisher. Their original foray into DRM
was motivated by preventing piracy. After a time, they tested the
assumption by releasing some of their audio books without DRM and then
monitoring file sharing networks. In the end, they came to the
conclusion “that D.R.M. is not actually doing anything to prevent
piracy” [BS2008]_.

In some cases DRM has caused significant backlash. EA's recent game
Spore is one example. The game requires activation over the Internet.
Once the game has been activated three times, it is unable to be
installed again. Unhappy users flooded Amazon with grips about the DRM
based restrictions [BK2008]_. The end result? Well, Spore may very
well be the most pirated video game to date [BJ2008]_ in
spite of the fact that significant parts of the game are inaccessible in
the pirated versions.

So what motive is there for DRM? Jim Griffin summaries the situation:

    Lawyers and technologists continue to sell this snake oil of control,
    whether it's from the court and the police [RIAA legal jihad], or
    whether it's coming from technology [DRM] ... When I was 14, I told
    girls I loved them to sleep with them too. It was a fiction. Steve Jobs
    just leaves a little money on the table. > > We see Jobs and Gates
    making promises to the content industry that they have no intention of
    keeping. It's the promise you make to move forward. The content owner
    wants to hear it [AO2004]_.

References
==========

.. [F2006]
    .. bib::
        :authors: Free Software Foundation
        :published: 2006-9-18
        :title: Digital Restrictions Management and Treacherous Computing
        :url: http://www.fsf.org/campaigns/drm.html
.. [JG2003]
    .. bib::
        :authors: Jeff Goodell
        :published: 2003-12-3
        :title: Steve Jobs: The Rolling Stone Interview
        :url: http://www.rollingstone.com/news/story/5939600/steve_jobs_the_rolling_stone_interview
.. [BS2005]
    .. bib::
        :authors: Bruce Schneier
        :published: 2005-11-17
        :title: Sony’s DRM Rootkit: The Real Story
        :url: http://www.schneier.com/blog/archives/2005/11/sonys_drm_rootk.html
.. [CD2004]
    .. bib::
        :authors: Cory Doctorow
        :published: 2004-6-17
        :title: Microsoft Research DRM talk
        :url: http://craphound.com/msftdrm.txt
.. [C2001]
    .. bib::
        :authors: CNN
        :published: 2001-1-25
        :title: Norwegian teen raided by police in DVD suit
        :url: http://archives.cnn.com/2000/TECH/ptech/01/25/dvd.charge/index.html
.. [JS2005]
    .. bib::
        :authors: John David Stone
        :published: 2005-4-15
        :title: Keeping stuff: Digital restrictions management and software patents
        :url: http://www.cs.grinnell.edu/~stone/essays/keeping-stuff/digital-restrictions.xhtml
.. [P2004]
    .. bib::
        :authors: PRNewswire
        :published: 2004-7-29
        :title: Apple Statement
        :url: http://www.prnewswire.com/cgi-bin/stories.pl?ACCT=109&STORY=/www/story/07-29-2004/0002221065&EDATE=
.. [O2007]
    .. bib::
        :authors: Out-law.com
        :published: 2007-1-24
        :title: Apple DRM is illegal in Norway, says Ombudsman
        :url: http://www.out-law.com/page-7691
.. [KB2006]
    .. bib::
        :authors: Kirk Biglione
        :published: 2006-7-27
        :title: Is Zune a PlaysForSure Killer?
        :url: http://www.medialoper.com/hot-topics/music/is-zune-a-playsforsure-killer/
.. [GD2008]
    .. bib::
        :authors: Guillaume Deleurence
        :published: 2008-11-7
        :title: Une procédure contre les DRM d'iTunes lancée en Norvège (A ruling against iTunes DRM initiated in Norway)
        :url: http://www.01net.com/editorial/391794/une-procedure-contre-les-drm-d-itunes-lancee-en-norvege
.. [BS2008]
    .. bib::
        :authors: Brad Stone
        :published: 2008-3-3
        :title: Publishers Phase Out Piracy Protection on Audio Books
        :url: http://www.nytimes.com/2008/03/03/business/media/03audiobook.html
.. [BK2008]
    .. bib::
        :authors: Ben Kuchera
        :published: 2008-9-8
        :title: Gamers fight back against lackluster Spore gameplay, bad DRM
        :url: http://arstechnica.com/news.ars/post/20080908-gamers-fight-back-against-lackluster-spore-gameplay-bad-drm.html
.. [BJ2008]
    .. bib::
        :authors: Ben Jones
        :published: 2008-10-1
        :title: EA Downplays Spore’s DRM Triggered Piracy Record
        :url: http://torrentfreak.com/ea-downplays-spores-drm-081001/
.. [AO2004]
    .. bib::
        :authors: Andrew Orlowski
        :published: 2004-2-11
        :title: Why wireless will end ‘piracy’ and doom DRM and TCPA – Jim Griffin
        :url: http://www.theregister.co.uk/2004/02/11/why_wireless_will_end_piracy/

