===============================
 Standard German Transcription
===============================

:class: LING 310 at TWU
:date: 2007-2-14
:tags: Paper, Lingustics


Introduction
============

Standard German is one of the larger languages of the world. According
to the Ethnologue [RG2005]_, Standard German had 95,392,978
speakers in 1990 and German 101 [G1994]_ reports it as being more
than 120 million people. Most of these people (75 million) reside in
Germany. Standard German is the official language of Germany,
Liechtenstein, and Austria; in addition to the multilingual nations of
Switzerland and Luxembourg where it shares that status. The ISO 639-3
code for Standard German is 'deu'. Standard German is also one of the
official languages of the European Union [EU2007]_.

From a diachronic perspective, German descended from the following
family line: Indo-European, Germanic, West, High German, German, Middle
German, East Middle German [RG2005]_. Historically, there were a
large number of German dialects throughout Western-Central Europe. These
dialects were mutually intelligible with adjacent ones, but not with
their further removed neighbors.

At some point, several of these dialects were affected by the High
German consonant shift. These dialects were classified as High German.
The dialects that did not follow the consonant shift became known as Low
German. Standard German grew out of High German, while Dutch, for
example, came from Low German.

During the 20th century, an increase in communication has caused many of
these dialects to dwindle or disappear. Moreover, nationalism and
political autonomy have caused a diverge between dialects from different
nations, such as Dutch and the Low German [G1994]_.

Standard German has much in common with Dutch and other languages from
the German family. It also has a smaller resemblance to English and
French (Ethnologue reports the lexical similarity of English [#f1]_ and
French with German as 60% and 29%, respectively).

The demography of the language varies from country to country; however,
Germany contains the most German speakers by far. According to the
Central Intelligence Agency [C2007]_, Germany has a literacy rate of
99% for both male and female. The country is predominately Christian
(Protestant and Roman Catholic) with a significant number of
unaffiliated or 'other' religious commitments.

.. figure:: /writings/school/german_map.svg
   :alt: image of map

   Map of Standard German usage (source missing)

Transcription
=============

(In this assignment were were tasked to transcribe certain phrases in
the target language and then make recommendations on how speakers of
that language may improve their pronunciation of English.)

.. rubric:: Hi, how are you today?

=============  =====  ==  =====  ===  ======
Transcription  ˈhalo  βi  ˈgeiʦ  di   ˈhʌite
Gloss          Hi,    as  goes   you  today?
=============  =====  ==  =====  ===  ======

.. rubric:: My name is (removed for privacy) and I come from Karlsruhe
            in Germany. What's your name?

=============  ==  ======  ===  ==  =====  ====  =========  ==  =========  ====  ======  ====
Transcription  iç  ˈheize  und  iç  coˈme  aus   ˈkarlsoi   ɪn  ˈdoiʧlʌnt  ve    hais    tʊ
Gloss          I   called  and  I   come   from  Karlsruhe  in  Germany.   What  called  you?
=============  ==  ======  ===  ==  =====  ====  =========  ==  =========  ====  ======  ====

.. rubric:: I would like to learn more about German and your country.

=============  ==  ======  ======  ====  =====  =====  ======  ===  ====  =====  ====  ========
Transcription  iç  ˈfʷʊɾe  ˈgeɾne  meiɾ  ˈlann̩  ˈøbe   doiʧ    und  aux   ˈøbe   dain  lantʰ
Gloss          I   would   like    more  learn  about  German  and  also  about  your  country
=============  ==  ======  ======  ====  =====  =====  ======  ===  ====  =====  ====  ========

.. rubric:: Could you please tell me where the bathroom is?

=============  ========  ===  ===  ======  =====  =====  ===  ========  ====
Transcription  ˈkœndʌst  dʊ   mɪr  ˈbɪdə   ˈsagn̩  wu     di   tʌˈlɛte   ist
Gloss          Could     you  me   please  say    where  the  toilette  is?
=============  ========  ===  ===  ======  =====  =====  ===  ========  ====

.. rubric:: I cannot understand or speak German, could you help me?

=============  ==  ===  ====  ======  ======  =====  ===========  =======  ===  ==  ======
Transcription  ɪç  kan  kein  doiʧ    ˈbrɛʃn̩  ˈoidɚ  faʃtein̩      ˈkʊnts̩t  du   mʌ  ˈhɛlfn̩
Gloss          I   can  not   German  speak   or     understand,  could    you  me  help?
=============  ==  ===  ====  ======  ======  =====  ===========  =======  ===  ==  ======

.. rubric:: Thanks for helping me learn about your language.

=============  =======  ====  ===  ===  =====  ======  =======  ===  ======
Transcription  ˈdaːnke  dʌs   du   mir  hœvs   ˈdainɛ  ˈʃpɾakɛ  stu  ˈlɚnʌn
Gloss          Thanks,  that  you  me   help,  your    speak    to   learn.
=============  =======  ====  ===  ===  =====  ======  =======  ===  ======

Recommendations for English Pronunciation
=========================================


* In all of the sentences, make sound, especially vowels, further back in the mouth. Move the tongue further back.
* In sentence 1, voiced alveolar approximants should be more rhotacized. Try bunching up the touch when making [ɹ] sounds.
* The first vowel in the word “name” should be a diphthong: [ei] not [e]. It may help to think of the words as being spelt “nayme”.
* The schwa vowel is slightly higher (the mouth is more closed). For example the “man” in “German” is pronounced [mәn], not [mʌn].
* In English, the [l] sound is more pronounced. Touch the tip of the tongue to the alveolar ridge.


Footnotes
=========

.. [#f1] Merely looking at the language's lexicon may be misleading,
         however, as German and English contain some significant grammatical
         differences. `Inflection, declension, use of gender, and long
         compound words <http://www.encyclopedia.com/doc/1E1-Germanla.html>`_
         are some features were German differs from English. Mark Twain's
         essay, `The Awful German Language
         <http://eserver.org/langs/the-awful-german-language.txt>`_,
         provides some examples (and humor) for the inquisitive reader.

References
==========

.. [RG2005]
    .. bib::
        :author: Raymond Gordon
        :published: 2005
        :title: Ethnologue report for language code:deu
        :url: http://www.ethnologue.com/show_language.asp?code=deu
        :edition: 15th ed
        :retrieved: 2007-2-12
.. [EU2007]
    .. bib::
        :published: 2007
        :title: Languages in the EU : German
        :url: http://web.archive.org/web/20070518121703/http://www.europa.eu/abc/european_countries/languages/german/index_en.htm?_de
        :retrieved: 2007-2-13
.. [G1994]
    .. bib::
        :published: 1994
        :title: German 101
        :url: http://www.101languages.net/german/
        :retrieved: 2007-2-10
.. [C2007]
    .. bib::
        :published: 2007
        :title: CIA - The World Factbook – Germany
        :url: https://www.cia.gov/cia/publications/factbook/geos/gm.html
        :retrieved: 2007-2-13
