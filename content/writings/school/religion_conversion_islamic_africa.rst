==========================================================
 Religion and Conversion in Islamic North and West Africa
==========================================================


:class: COMM 302 at TWU
:date: 2009-3-31
:tags: Paper, Religion

Cultural Profile
================

1. Islam

   1. Islam is probably the fastest growing religion in the world.
   2. Statistics vary widely but even a conservative estimate from the U.S.
      Center for World Mission  [GP1997]_ places its growth rate at 2.9%
      from 1970 to 1996, just above Christianity at 2.3%.
   3. According to the BBC [B2005]_ this growth this largely a result of
      higher birth rates in Islamic nations and immigration to the West.
   4. Of these, about 12.2% live in North Africa, making up 89.6% of the
      population there. [#f1]_

2. History

   1. Originally this area was largely animistic. Islam was first introduced
      with the military conquests of the Umayyad caliphate in the later half
      of the 7th century. At first, only the coastal areas where affected.
      However, as time progressed, Arabic merchants set up trade routes
      throughout the inland areas, bringing the message of Islam with them.
   2. Most of the North Africans submitted [#f2]_ to Islam. One significant
      group that did not was the Copts in Egypt who were already Christian.
   3. During the period of European colonialism, these countries where all
      colonized by various European countries (mostly France). The Europeans
      made various attempts at converting the Africans, but as current
      demographics show, they were largely unsuccessful.

3. Cultural generalizations

   1. African culture has always been one of the prototypical examples of
      high-context cultures. (Edward Hall).
   2. Kluckhohn

      1. Human Nature: Mixed
      2. Man-Nature relationship: Man subordinate to Nature
      3. Time: Past
      4. Activity: Being
      5. Social Relations: Collective [HP2004]_

   3. Hofstede [GF2009]_ does not have statistics for North Africa. The
      closest would be West Africa which has the following values (from 0-100)

      1. Power Distance: 82
      2. Individualism: 25
      3. Masculinity: 51
      4. Uncertainty Avoidance: 60
      5. Long-Term Orientation: 22

Introduction
============

In recent centuries, Westerners have often maintained a relatively large
distinction between religion and society. Religion has been relegated to
the position of personal opinion and is generally not considered to be
something that should restrict one’s public standing. However, this is
not so true in other parts of the world.

In the Arabic countries of North and West Africa, the issue of religion
and culture are much more deeply related. To them, religion is not
merely a matter of intellectual opinion. In fact, intellectual beliefs
and convictions may have little to do with it. Instead, religion is
something that defines one's cultural identity.

As a result, Muslims who adopt Christian beliefs are often faced with
difficult choices. To call themselves “Christian” means to reject their
society, while denying their “Christian” identity hinders potential
relationships with others Christians. Because of this Christian
believers in Muslim societies have taken on different identities
depending on their situations. Some have opted to remain within their
own culture, drawing a fine line between contextualization and
syncretism of their faith. Some have broken away and adopted Western
culture and habits. Most of them fall somewhere in between.

Being Muslim
============

Misconceptions
--------------

In order to understand religion and how to affect religious change in
this area, it is important to understand what religion means to the
people themselves. From a Western perspective it is easy to fall into
the trap of thinking that religion is merely a set of beliefs and rules.
This is especially true for those from a Protestant background.

In many ways the current view of religion in the West can be linked to
the Protestant Reformation. One of the common phrases from the
Reformation was called the Five Solas: *Sola Scriptura, Solus Christus,
Sola Gratia, Sola Fide, Sola Dei Gloria* [#f6]_. The importance of this
is that it lacks any reference to the community of believers (i.e. the
church).

The merits of Protestant or Roman Catholic theology are not to be
discussed here. What is important is that if Christian workers are to
have any chance in effecting change among the hearts of these people, it
must come to the people on their own terms. As Smith outlined in his
book, change requires several things. Agents of change must reinforce
the group's traditions and change must fit within he existing cultural
patterns. [DS1984]_

Religion as Part of a Cultural Identity
---------------------------------------

Among North and West Africa, the people’s religious identity is
influenced strongly by their cultural identity and their cultural
history. In addition, the people there have a significant colonial
history as well [MN2007]_. During the 15th and
19th centuries, the countries there where all colonized by the
Europeans, most of whom called themselves Christian. As a result, many
Africans perceive Christianity as a foreign, European religion, and
Islam as their own religion.

    “[The taxi cab driver] nodded for a while and then whispered, as if
    talking only to himself, “God is greater! But he does not speak French!”
    Dominated by the Christian French for decades, and having no native
    Christians, many of the Moroccans thought that every Christian was
    French, much as some people thing that every Muslim is
    Arab.” [SK2007]_

The Importance of Language
--------------------------

The significance of the Arabic language to the Muslim people is another
thing that may be difficult for those from a Western background to
understand. No one in these days would think about speaking about the
“Christian Language.” [#f3]_ Arabic, on the other hand, is very much the
language of Islam, and in many ways it is an important part of the
religion. In some cases it even goes so far as to be used as the
identifying mark for a Muslim. Sonia Khūrī lived as a Christian among
Muslims in West Africa, but was considered by many to be a Muslim
because he spoke Arabic as his mother tongue. [SK2007]_

Again, the power of language, or at least, the perception of language,
is a force that is foreign to most in the West. In the case of Islam, it
is not merely an issue of language, but of written language. Indeed, the
name People of the Book is no accident. A Muslim prophet is someone who
brings a written message from God (e.g. Moses brought the Tawrat; Jesus,
the Injil; and Mohammed, the Qu’ran). To the illiterate tribes of Africa
during the time of the Muslim conquests, the written language would have
been a powerful thing.

Like any language, Arabic carries with it a unique style, [MN2007]_
and because it is used for all religious aspects,
the speakers develop certain expectations of what religious language is
like. When presented with religious text in another language, they seem
weird or awkward.

Al-Amin bin Ali al-Mazrui, a Muslim Swahili scholar, spoke up on the
importance of Arabic to Islam. According to him, the study of Arabic is
a necessity for every Muslim. His belief was that Arabic is “the
language of Islam because Allah choose it to reveal the Qu’ran. There is
no other way to understand the Qu’ran but through the understanding of
Arabic.” [GC1991]_ [#f4]_

Diversion from Orthodox Islam
-----------------------------

In the end, the requirements to becoming a Muslim are few and small. In
general, new comers are required to recite the creed: “There is no God
but Allah, and Mohammad is His prophet,” but that is about as far as it
goes. Calasso notes this phenomenon:

    In all the other cases recorded by Ibn Sa’d, the learning of rituals,
    proper behavior, and the Qu’ran, always happens during the time
    following conversion, or better yet, following the “entrance into
    Islam.” [GC2001]_ [#f5]_

In contrast, the Christian approach implies a confession to a certain
set of beliefs and implies that a believer already has the foundational
knowledge for his or her faith. Because of this, Islam has been much
more vulnerable to syncretism.

The original Arabic missionaries (usually either invaders or merchants),
brought the message of Islam to groups of people who were largely
animistic. The new religious systems did not so much replace the old one
as it added a second layer of religion on top. Calasso cites Fisher,
saying:

    I do not think that conversion ... has occurred very often in the
    Islamic history of sub-Saharan Africa ... What has been much more
    common, in the early stages of Islamic development in any particular
    area, has been adhesion, as local people, still primary loyal to their
    ancestors, begin to add a little from Muslim faith and practice, “useful
    supplements” and “valuable safeguards”. [GC2001]_

As a result, the beliefs of these African Muslims are not always in line
with the orthodox Muslim beliefs in the Middle East. To this end,
missionaries from more orthodox Arabic nations have sometimes been sent
out to “reform” the natives. However, the Islamic missionaries seem to
have as much difficulty as the Christians ones when it comes to Muslims.

Sonia Khūrī, a Lebanese Christian and anthropologist once noted this
while he was living in Magburaka in Sierra Leone. In his book `An
Invitation to Laughter`, he describes the result of some Egyptian
Muslim scholars who come there as missionaries. In this text Alimami
Suri is a native Temne trying to explain the their local customs and
beliefs to the Egyptians.

    None of the Alimami Suri’s emphatic assertions about Temne Islam met
    the approval of the head of the Egyptian mission. Christians or Muslims,
    missionaries shared similar attitudes towards the natives: “We know
    better.” I later learned that the mission stayed for nine months in
    Magburaka and then returned to Egypt. They were replaced by another team
    of four, who stayed for two more years, and then the whole program was
    scrapped. It proved to be counterproductive, creating more apathy than
    empathy.  [SK2007]_

The Problem of “Conversion”
===========================

Problem as a Western Idea
-------------------------

Part of the difficulty when talking about converting others to
Christianity is that the idea of “conversion” involves a rather uniquely
Western view of religion. When we think of prototypical examples of
conversion, cases like Paul and his “Damascus experience” (Acts 9) come
to mind. A long with conversion is the implication that there is a
giving up of certain beliefs in favor of other ones.

Becoming a Muslim
-----------------

As it was mentioned earlier, the process of becoming a Muslim was in
some ways more of a “submission” than a “conversion”. In Arabic, the two
words are not actually distinguished. Calasso cites Bulliet to explain
this.

    The language (Arabic) does not allow the reader to disentangle the
    meaning « surrender » and « convert ». This raises the question as to
    whether these meanings were actually separated in the minds of the
    actors. After all the notion of religious conversion as a moral or
    spiritual act that could be taken independently of other political or
    social action may not have been widespread in seventh-century
    Arabia. [GC2001]_

Later on, he continues, highlighting the why it is problematic to look
at “becoming muslim” as the same as converting religions.

    “In a sense, a convert first became a member of the Muslim community
    and later discovered, or tried to discover what it meant to be a Muslim.
    This is why the personal conversion stories that have been preserved say
    so little about preaching, studying, or intellectual or moral
    transformation.”  [GC2001]_

Christians in a Muslim society
==============================

Bearing all of this in mind, how can a Christian worker have an impact
in the religious domain of an Islamic society. First and foremost, the
Christians must be able to communicate effectively in a way that speaks
to the hearts of the other people around. One of the examples where this
is a problem is the Arabic Bible. Cragg explains the problem in the
following words:

    It has often been observed from within Arab Islam that these do not
    read congenially. The reason is not simply the distinctive themes of
    Christian faith nor the overall shape of the New Testament with its
    fourfold Gospels and its church “correspondence.” It has also to do with
    the strange quality of the text when registered by readers whose sense
    of the scriptural in Arabic is made by the Qur’an. The style, the idiom,
    the grammar, and the syntax all convey an impression that puzzles, if it
    does not altogether deter, the Muslim Arab. [KC1991]_

When the German Bible was first translated into Low German, it received
poor reception. Some thought it was a nice idea, but could not bring
themselves to read it because the style did not feel proper. Others felt
strongly the translation should have never been done, that it was
disrespectful to use the language of Low German in the Christian
Scriptures. In the same way, Arabic speakers approach the Arabic Bible.
Although it is Arabic, it is based on styles from writers in different
languages and does not fit within the style of Arabic religious writing.

Even things as simple as a different name for God can be a difficult.
The Muslim believes very strongly that there is one God named Allah. To
them, the other names are all false gods and idol. As a result, many
Muslim Background Believers continue to use the name Allah to refer to
God. “I asked a Christian Arab why he continues to use the term Allah
when he prays, and he whispered to me, “I cannot bring myself to use the
Hebrew names, you know?” [EC2004]_

Another problem that must be handled is the issue of identity. If a
Muslim becomes a Christian, can he still be called a Muslim. Are some
Muslim practices still permissible. Historically, the answers to this
question have been pretty harsh. For example, the Anglicans in
Palestinian. Riah Abu El-Assal is an Arab who became an Anglican bishop.
Although this is in Palestine and not North Africa, the issue of
relating to Muslims still exists. In one of his online article, Abu
El-Assal shares the following insights regarding the European domination
of Europeans in the Christian climate of his own land.

    Much of the cultural legacy of the French, Dutch, British, Italians,
    and Americans has its comical side—like convincing Arab Anglicans to
    observe high tea at four Greenwich Mean Time! But, it has harmed
    Palestinian Christians, already adherents to a faith considered alien
    and aberrant by their neighbors, leaving them without control of their
    own church or power to implement the tenets of their
    faith. [AE2006]_

Different people have addressed this problem in different ways. One
common measure of the level of immersion is the C1–C6
scale [CM2003]_ which ranges from C1 (Traditional church
using an outsider language) to C6 (Small Christi-centered communities of
secret/underground believers. Note that in this scale, the members of
the categories C4–C6 do not even call themselves “Christian.”

There is no silver bullet to missionary work among the Muslim people.
However, it is important to understand the problems. Any attempt to
bring about religious change will never work if the cultural
considerations of the people are not kept in mind.

Appendix
========

.. figure:: /writings/school/africa_islam.svg
   :alt: image of map

   Map of Demographics

   Source: Self-made. Data from CIA Factbook and Adherents.com. SVG
   Graphics based on http://en.wikipedia.org/wiki/File:BlankMap-Africa.svg

Footnotes
=========
.. [#f1] Statistics based largely on the United States Department of State
         and CIA World Factbook, via
         http://en.wikipedia.org/w/index.php?title=Islam_by_country&oldid=280191331
.. [#f2] It seems that the word “submit” is actually more accurate than
         “convert” in this context. This will be discussed later.
.. [#f3] One could argue about particular languages within particular
         denominations of Christianity (e.g. Latin in the Roman Catholic
         Church); however, these languages never had quite the status that
         Arabic has in Islam. For example, it would be a bit ridiculous to
         call the Latin Vulgate the “ultimate form of God’s revelation” when
         it’s very name indicates that it was translated for the common
         people (who couldn’t read Greek).
.. [#f4] Original text: L’arabe, pour Sh.al-Amin b.Aly, était la langue de
         l’islam parce que Allah l’avait choisi pour révéler le Coran. Il
         n’existait aucun autre moyen de comprendre le Coran si ce n’est à
         travers la connaissance de l’arabe.
.. [#f5] Original text: Dans tous les autres cas enregistrés par Ibn Sa’d,
         l’apprentissage des rituels, des normes, du Coran, se situe
         toujours dans un moment suivant la conversion, mieux, suivant
         « l’entrée dans l’islam ».
.. [#f6] By Scripture Alone, Christ Alone, By Grace Alone, By Faith Alone,
         Glory to God Alone

References
==========

.. [GP1997]
    .. bib::
        :authors: G. Parsons
        :published: 1997
        :title: Quoted in Zondervan News Service
        :url: http://www.religioustolerance.org/growth_isl_chr.htm
        :retrieved: 2016-04-21
.. [B2005]
    .. bib::
        :published: 2005
        :title: Muslims in Europe: Country guide
        :url: http://news.bbc.co.uk/1/hi/world/europe/4385768.stm
        :retrieved: 2009-3-30
.. [HP2004]
    .. bib::
        :authors: Steven Hitlin and Jane Allyn Piliavin
        :published: 2004
        :title: Values: Reviving a Dormant Concept
        :journal: Annual Review of Sociology
        :page: 359-393
        :publisher: JSTOR
        :ref-page: 375
.. [GF2009]
    .. bib::
        :authors: G. Hofstede
        :published: 2009
        :title: West African Geert Hofstede Cultural Dimensions Explained
        :url: http://www.geert-hofstede.com/hofstede_west_africa.shtml
        :retrieved: 2009-3-30
.. [DS1984]
    .. bib::
        :authors: D. Smith
        :published: 1984
        :title: Make Haste Slowly
        :publisher: Institute for International Christian Communication
        :pages: 135-136
.. [MN2007]
    .. bib::
        :authors: J. Martin, T. Nakayama
        :published: 2007
        :title: Intercultural Communication in Contexts
        :edition: th ed
        :publisher: McGraw-Hill
        :pages: 138-139, 223
.. [SK2007]
    .. bib::
        :authors: Sonia Khūrī
        :published: 2007
        :title: An Invitation to Laughter
        :publisher: University of Chicago Press
        :pages: 41-47
.. [GC1991]
    .. bib::
        :authors: Françoise Le Guennec-Coppens and Patricia Caplan
        :published: 1991
        :title: Les Swahili entre Afrique et Arabie
        :publisher: KARTHALA Editions
        :page: 23
.. [GC2001]
    .. bib::
        :authors: Giovanna Calasso
        :published: 2001
        :title: Récites de conversion
        :journal: Conversions islamiques
        :pages: 19-47
        :publisher: Maisonneuve & Larose
        :ref-pages: 32-34
.. [KC1991]
    .. bib::
        :authors: K. Cragg
        :published: 1991
        :title: The Arab Christian: A History in the Middle East
        :publisher: Westminster John Knox Press
        :page: 282
.. [EC2004]
    .. bib::
        :authors: Ergun Caner
        :published: 2004
        :title: The MBBs’ “Dirty Little Secret.”
        :journal: Israel My Glory
        :volume: 62
        :issue: 6
        :page: 8
.. [AE2006]
    .. bib::
        :authors: Abu El-Assal
        :published: 2006
        :title: Arab Christians: An Endangered Species
        :url: http://web.archive.org/web/20090309074001/http://jerusalemites.org/jerusalem/christianity/2.htm
        :retrieved: 2009-3-30
        :page: 2
.. [CM2003]
    .. bib::
        :authors: A. Cooper, E. Maxwell
        :published: 2003
        :title: Ishmael My Brother
        :edition: 3rd ed
        :publisher: Kregel Publications
        :page: 279
.. [BD2000]
    .. bib::
        :authors: Bernard Dutch
        :published: 2000
        :title: Should Muslims become “Christians.”
        :journal: International Journal of Frontier Missions
        :volume: 17
        :issue: 1
        :page: 15-24
        :url: http://www.ijfm.org/PDFs_IJFM/17_1_PDFs/Muslims_as_Christians.pdf
.. [JW2007]
    .. bib::
        :authors: J Dudley Woodberry
        :published: 2007
        :title: To the Muslim I Became a Muslim
        :journal: International Journal of Frontier Missiology
        :volume: 24
        :page: 23-28
        :url: http://www.ijfm.org/PDFs_IJFM/24_1_PDFs/Woodberry.pdf

