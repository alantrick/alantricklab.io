==========================================
 Preparation and Analysis of Benzoic Acid
==========================================

:authors: Emily Koch, Alan Trick, Ian Trick
:tags: Paper, Science
:class: CHEM 199 at TWU
:date: 2009-3-18

Purpose
=======

The purpose of this experiment is to prepare and analyze benzoic acid.
It is also meant to demonstrate some general preparation methods, such
as using a reflux apparatus, vacuum filter, and a hot gravity filter,
plus determining melting point and titrating.

Introduction
============

Benzoic acid was first synthesized in the 16th century [W2009]_. It
is an organic acid that naturally forms monoclinic leaflets and is found
as a colourless solid. The molar mass of benzoic acid is 122.12 g and
its melting point is 122°C. In water at 1°C, benzoic acid has a
solubility of 1.7 g/liter, but at 95°C it has a solubility of 68
g/liter [ND2008]_. Due to its natural ability to impede the growth
of yeast, mold, and some bacteria, benzoic acid has been used to treat
fungal skin diseases such as athlete's foot and as a food preservative.

Oxidation Theory and Calculations
=================================

In general, a primary alcohol can be oxidized to lend either aldehydes
or carboxylic acids [JC2006]_. The reaction goes through two
processes. Note that R is shorthand for an arbitrary hydrocarbon
counterpart. For example, RCH\ :sub:`2`\ OH would be ethanol if R was
CH\ :sub:`3`.

1. RCH\ :sub:`2`\ OH + O- ⇋ RCOH + H\ :sub:`2`\ O
2. RCOH + O- ⇋ RCOOH

In the first stage, an available oxygen atom binds with two of the
hydrogen atoms at the end of the alcohol (RCH\ :sub:`2`\ OH) to produce an
aldehyde (RCOH). The oxygen within the alcohol then assumes a covalent
configuration with the carbon and the result is a molecule known as an
aldehyde. In the second stage, the hydrogen atom at the end of the
aldehyde incorporates another available oxygen to produce a carboxylic
acid (RCOOH). This reaction has several uses. For example, potassium
dichromate (an oxidant) will produce ethanal or ethanoic acid when mixed
with ethanol. This particular reaction used to be used as part of the
‘breathalyser’ procedure to test if drivers had consumed too much
alcohol. In our particular experiment, we are oxidizing benzyl alcohol
(C\ :sub:`6`\ H\ :sub:`5`\ CH\ :sub:`2`\ OH) with permanganate
(MnO\ :sub:`4`\ :sup:`-`). The goal is to produce benzoic acid
(C\ :sub:`6`\ H\ :sub:`5`\ COOH). Thus, in our case the R will be
C\ :sub:`6`\ H\ :sub:`5`, a phenyl ring. The permanganate is created by
dissolving potassium permanganate in sulphuric acid
(H\ :sub:`2`\ SO\ :sub:`4`) (the reason for using a highly acidic solvent
will be explained later). To understand how the reaction works, we must
first understand how the oxidation process works. Oxidation processes
are described using redox equation. Redox equations describe the
requirements for changing the oxidation level of a chemical. First,
consider the permanganate. It reacts in two steps: first to
MnO\ :sub:`2` then Mn\ :sup:`2+`. Their redox equation [VB1986]_ is
written as:

3. MnO\ :sub:`4`\ :sup:`-` + 4 H\ :sup:`+` + 3 e\ :sup:`-` ⇋
   MnO\ :sub:`2` + 2 H\ :sub:`2`\ O
4. MnO\ :sub:`2` + 4 H\ :sup:`+` + 2 e\ :sup:`-` ⇋
   Mn\ :sub:`2`\ + + 2 H\ :sub:`2`\ O

And for the two states of the alcohol to acid reaction:

5. RCH\ :sub:`2`\ OH ⇋ RCOH + 2 H\ :sup:`+` + 2 e\ :sup:`-`
6. RCOH + H\ :sub:`2`\ O ⇋ RCOOH + 2 H\ :sup:`+` + 2 e\ :sup:`-`

These equations are not complete since they have spare electrons on one
side (because of this they are often called half-equations). Note,
however, how equations 3 and 4, and 5 and 6 have spare electrons on the
opposite sides. This is because 3 and 4 are reduction while 5 and 6 are
oxidation. These equations can be combined to produce the full equations
for the oxidation. For example, combine 3, 4, and 5 to determine the
alcohol to aldehyde step:

7. 3 RCH\ :sub:`2`\ OH + 2 MnO\ :sub:`4`\ :sup:`-` + 2 H\ :sup:`+` ⇋
   3 RCOH + 2 MnO\ :sub:`2` + 4 H\ :sub:`2`\ O
8. RCH\ :sub:`2`\ OH + MnO\ :sub:`2` + 2 H\ :sup:`+` ⇋
   RCOH + Mn\ :sup:`2+` + 2 H\ :sub:`2`\ O
9. 5 RCH\ :sub:`2`\ OH + 2 MnO\ :sub:`4`\ :sup:`-` + 6 H\ :sup:`+` ⇋
   5 RCOH + 2 Mn\ :sup:`2+` + 8 H\ :sub:`2`\ O

Equation 7 is the combination of 3 and 5, the first step of manganese's
oxidation; 7 is from 4 and 5, the second step of the oxidation; and 9 is
the combination of 7 and 8. The same can be done to determine the
aldehyde to alcohol step:

10. 3 RCOH + 2 MnO\ :sub:`4`\ :sup:`-` + 2 H\ :sup:`+` ⇋
    3 RCOOH + 2 MnO\ :sub:`2` + H\ :sub:`2`\ O
11. RCOH + MnO\ :sub:`2` + 2 H\ :sup:`+` ⇋
    RCOOH + Mn\ :sup:`2+` + H\ :sub:`2`\ O
12. 5 RCOH + 2 MnO\ :sub:`4`\ :sup:`-` + 6 H\ :sup:`+` ⇋
    5 RCOOH + 2 Mn\ :sup:`2+` + 3 H\ :sub:`2`\ O

Finally, equations 9 and 12 can be combined to produce the equation for
the overall reation (assuming it goes to completion):

13. 5 RCH\ :sub:`2`\ OH + 4 MnO\ :sub:`4`\ :sup:`-` + 12 H\ :sup:`+` ⇋
    5 RCOOH + 4 Mn\ :sup:`2+` + 11 H\ :sub:`2`\ O

Replacing R with C\ :sub:`6`\ H\ :sub:`5` gives the reaction for the
benzyl chemicals.

14. 5 C\ :sub:`6`\ H\ :sub:`5`\ CH\ :sub:`2`\ OH + 4
    MnO\ :sub:`4`\ :sup:`-` + 12 H\ :sup:`+` ⇋
    5 C\ :sub:`6`\ H\ :sub:`5`\ COOH + 4 Mn\ :sup:`2+` + 11 H\ :sub:`2`\ O

Experimental Procedure
======================

Benzyl alcohol (4 ml) and KMnO\ :sub:`4` (about 9 g) are placed into 150
ml of a highly acidic solution (H\ :sub:`2`\ SO\ :sub:`4`). Once in the
solution, the KMnO\ :sub:`4` will dissolve into K\ :sup:`+` and
MnO\ :sub:`4`\ :sup:`-`. There are about twice as many moles of
KMnO\ :sub:`4` to encourage the alcohol to react as far as possible. As
the amount of benzyl alcohol approaches or exceeds the 5/4 of the amount
of KMnO\ :sub:`4`, there will be significant amounts of benzylhyde left
over. Also, the solution needs to be acidic so that it will supply the
free hydrogen ions that appear on the left-hand side of equation 14.

In order to push the equilibrium to the right as quickly as possible,
the solution is heated in a ball flask and stirred over a magnetic
stirring plate. Unfortunately, as the solution heats up, the solution
will start to evaporate. Benzaldehyde has the lowest boiling point
(178.1°C) and will evaporate first. Benzaldehyde has a strong smell of
vanilla, so it should be relatively noticeable when it starts to
evaporate. The benzaldehyde is an intermediate chemical in the reaction
and if it evaporates, the final yield will be reduced. To prevent this,
the solution is placed in a reflux apparatus while the heating is taking
place.

Apart from simply increasing the activity of the molecules, increasing
the reaction. It also helps the oxidation process since KMnO\ :sub:`4`
decomposition is sensitive to heat.

The reaction takes a long time to complete, so the procedure is stopped
part way through. As a result, the solution will still contain
MnO\ :sub:`4`\ :sup:`-` and MnO\ :sub:`2`. These are reduced by adding
sodium bisulphite (the MnO\ :sub:`4`\ :sup:`-` and MnO\ :sub:`2` oxidize
the bisulphite reacts with these to produce manganese, water, and
sulphate ions, see equations 15 and 16). The result of this should be
noticeable as the brownish color of manganese dioxide will fade. Not all
of the MnO\ :sub:`4`\ :sup:`-` and MnO\ :sub:`2` will be removed at this
point, so some color will persist.

15. 2 MnO\ :sub:`4`\ :sup:`-` + 4 HSO3- + 2 H\ :sup:`+` ⇋
    2 Mn\ :sup:`2+` + 5 SO\ :sub:`4`\ :sup:`2-` + 3 H\ :sub:`2`\ O
16. MnO\ :sub:`2` + HSO3- + H\ :sup:`+` ⇋
    Mn\ :sup:`2+` + SO\ :sub:`4`\ :sup:`2-` + H\ :sub:`2`\ O

The benzoic acid is slowly cooled. It freezes at 122.4°C, creating
crystals. The crystals are removed by vacuum filtration, and tests are
run on them.

After this, the benzoic acid crystals are dissolved in water for further
purification. Since MnO\ :sub:`2` and Mn\ :sup:`2+` are not soluble in
water, they are easily removed via gravity filtration. Then the acid is
split into two beakers: one hot, and one cool. These are then cooled so
that the acid will recrystallize. Note that unless the solution is
cooled ahead of time, the part of the solution that is place in the cool
beaker will cool down very rapidly. This hinders the crystallization
process because it produces large amounts of very small crystals.

The crystals are separated from the solution again and the final tests
are made.

Testing Procedures
==================

The effectiveness of the procedure is measured by 3 different tests.
First, the crude acid crystals are weighed and compared against the
expected yield to determine the % conversion. Second, some of the acid
crystals are melted to test the accuracy of the melting point (ideally
122°C). Third, some of the purified benzoic acid is redissolved in water
and titrated against sodium hydroxide (NaOH). The moles of NaOH needed
for the titration equals the moles of benzoic acid, which is then
converted to grams, and can be compared against the actual mass of
benzoic acid produced to determine the purity of the product.

Observations
============

During reflux in the first trial the solution changed colour several
times. First, it changed from dark purple to orange, then to light
yellow, then to dark yellow, then burnt orange, and finally a brownish
orange. 0.012 g of dullish light yellow crystals was retrieved, which
was very unfortunate because 0.1 g was needed for determining the
melting point and ~0.15 g to determine the purity through three
titrations. Much of the loss of crystals was during reflux when huge
amounts evaporated because the solution was too hot. During the second
trial the experiment was done more carefully and only a few vapours were
released when refluxation first started. Ten minutes into the procedure,
the solution turned a pinkish colour, then light yellow, then
darker-medium yellow at 15 minutes, and at 30 minutes the solution was
bright yellow with a thin orange layer floating on the top. After vacuum
filtration there was 1.509 g of light brown crystals. Two samples of the
filtered crystals were put into beakers, one pre-warmed and one cooled
in an ice bath. The latter crystals formed faster, cleaner, and smaller.
Whereas the others, left at room temperature, formed slower, but were
bigger. When the crystals on filter paper were examined, the cold
crystals were off-white and very fine with a paper-like texture, and the
warm crystals were coarse, off-white/pearly, and very bumpy. The average
melting point for the first trial was 94 degrees Celsius, and 115
degrees Celsius for the second trial. The normal melting point of
benzoic acid is 122.4 degrees. The purity of the crystals was measured
using titration with sodium hydroxide as the titrant.

Calculations for Percent Yield
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

| KMnO\ :sub:`4` (Molar mass = 158.03 g/mol)
| C\ :sub:`6`\ H\ :sub:`5`\ CH\ :sub:`2`\ OH (Molar mass: 108.14 g/mol ; Density: 1.04 g/ml)
| (1.04 g/ml C\ :sub:`6`\ H\ :sub:`5`\ CH\ :sub:`2`\ OH \* 4.00 ml) / 108.14 g/mol = 0.0385 mol C\ :sub:`6`\ H\ :sub:`5`\ CH\ :sub:`2`\ OH
| Experiment 1: 9.00g KMnO\ :sub:`4` / 158.03 g/mol = 0.570 mol KMnO\ :sub:`4`
| Experiment 2: 9.02g KMnO\ :sub:`4` / 158.03 g/mol = 0.571 mol KMnO\ :sub:`4`
| C\ :sub:`6`\ H\ :sub:`5`\ CH\ :sub:`2`\ OH is limiting reagent so 0.0385 moles of C\ :sub:`6`\ H\ :sub:`5`\ CH\ :sub:`2`\ OH results in 0.0385 moles of C\ :sub:`6`\ H\ :sub:`5`\ COOH C\ :sub:`6`\ H\ :sub:`5`\ COOH (Molar mass: 122.12 g/mol)
| 0.0385 moles\* 122.12 g/mol = 4.70 g C\ :sub:`6`\ H\ :sub:`5`\ COOH
| Theoretical yield is 4.70 g C\ :sub:`6`\ H\ :sub:`5`\ COOH.
| Grams of actual yield / grams of theoretical yield = Percent Yield
| Experiment 1: 0.12 g / 4.70 g \* 100% = 2.55%
| Experiment 2: 1.509g / 4.70g \* 100% = 32.1%

Titration Calculations
^^^^^^^^^^^^^^^^^^^^^^

Titration #1
~~~~~~~~~~~~

| 6.42 ml NaOH x 1L/1000 mL x 0.1357mol/1L = 8.71 x 10-4 moles
| 8.71 x 10-4 moles NaOH = 8.71 x 10-4 moles benzoic acid
| 8.71 x 10-4 moles benzoic acid x 122.12 g/mol = 0.106 g of benzoic acid
| % purity = 0.106g/0.216g x 100% = 49.1% purity

Titration #2
~~~~~~~~~~~~

| 27.9mL NaOH x 1L/1000mL x 0.1357mol/1L = 0.00379 mol NaOH
| 0.00379 mol NaOH = 0.00379 mol benzoic acid
| 0.00379 mol benzoic acid x 122.12 g/1mol = 0.463g of benzoic acid
| % purity = 0.463g/0.173g = 268% purity

Discussion
==========

In the first trial run of this experiment, 0.12 g of crystals was
obtained. These crude benzoic acid crystals were a dull yellow colour
and they didn’t look very pure. The low yield, 2.57%, was a result of
several errors. First of all, the 150 mL of H\ :sub:`2`\ SO\ :sub:`4`
was measured with a 400 mL beaker which is not very precise. Also, when
the original solution was refluxed a large amount of fumes escaped
through the top of the reflux apparatus without condensing. This was
because the heat was turned on too high, and so some of the crystals
evaporated, thereby reducing the yield. Also once the solution was
finished refluxing, the solution was a brownish-orange colour and
therefore the crystals were a dullish yellow colour versus being pure
and white. Because the crystals were not very pure, the melting point
was only found to be 94°C, whereas benzoic acid is supposed to have a
melting point of 122°C. After recrystallizing and purifying the crude
benzoic acid crystals, the cold crystal solution sat in the fridge and
the room temperature crystal solution sat in the lab for a week. Then,
because no crystals had formed over the week, the experiment was
discontinued and therefore neither the titrations nor the percent purity
calculations were completed. The lack of crystals was due to the fact
that there was a low percent yield of 2.57%, and the hot gravity
filtration apparatus was not completely hot when the crystals were
filtered through.

The second trial run was more successful because 1.509 g of crude
benzoic acid crystals was collected. The crude crystals were light brown
with some clear spots. There was less error in this experiment because
the 150 mL of H\ :sub:`2`\ SO\ :sub:`4` was measured with a 10 mL
graduated cylinder and care was taken to not release vapors during
refluxation. Although some vapors did escape, it was considerably less
than in the first trial run. The crude product resulted in a 32.3%
yield. We obtained 1.509 g of crystals compared to the theoretical
amount of 4.67 g. The melting point of these crude crystals was 115°C
which is very close to the actual melting point of benzoic acid: 122°C.
Once the crude crystals were recrystallized, purified, and separated
into two beakers of different temperature, it was discovered that the
cold crystals formed faster, yet were smaller, than the ones that formed
at room temperature. The 0.216 g of cold crystals were very fine and
off-white compared to the 0.173 g of lukewarm crystals which were coarse
and had an off-white, pearly appearance. In the experiment, the melting
point of the pure crystals was not determined, so it cannot be compared
with the melting point of the crude crystals. Because not a lot of pure
crystals were obtained, the titration was only completed twice although
it should have been done three times. The first titration of the room
temperature crystals determined that the crystals had 49.1% purity. The
second titration, which was of the cold pure crystals, determined a
purity of 268%. Obviously this percentage is inaccurate and that is due
to a faulty titration. Phenolphthalein was not added to the solution and
therefore the titration was off. When the phenolphthalein was added, the
solution was already bright pink and the titration was well beyond the
equivalence point.

Conclusion
==========

In this experiment benzoic acid was synthesized through the oxidation of
benzyl alcohol by acidified potassium permanganate.

The alteration of our initial solution into crude crystals was observed
through using a reflux apparatus, collecting and cleaning the crystals
with a vacuum filter, recrystallizing again through reflux, and finally
purifying with a hot gravity filter. The melting point was examined, the
percent yield calculated and the purification of the resulting crystals
was discovered through titration with phenolphthalein. Although the
first trial was not successful, the second trial resulted in a percent
yield of crude crystals of 32.1%, a melting point of 115°C, and a
percent purity of 49.1%.

References
==========

.. [W2009]
    .. bib::
        :published: 2009
        :title: Benzoic Acid
        :journal: Wikipedia, The Free Encyclopedia
        :retrieved: 2009-3-18
        :url: http://en.wikipedia.org/w/index.php?title=Benzoic_acid&oldid=276599988
.. [JC2006]
    .. bib::
        :authors: Jim Clark
        :published: 2006
        :title: Oxidation of Alcohols
        :retrieved: 2009-3-18
        :url: http://www.chemguide.co.uk/organicprops/alcohols/oxidation.html
.. [ND2008]
    .. bib::
        :authors: Nigel Dance et al.
        :published: 2008
        :title: Chemistry 114 Lab Manual
        :page: 91-97
        :publisher: UCFV
.. [VB1986]
    .. bib::
        :authors: Arthur Vogel and John Bassett
        :published: 1986
        :title: Textbook of Inorganic Analysis
        :edition: 4th ed
        :page: 42
        :retrieved: 2009-3-18
        :url: http://web.archive.org/web/20080317034731/http://www.chem.ubc.ca/faculty/wassell/CHEM415MANUAL/Experiment5/415E5Ref3.htm

