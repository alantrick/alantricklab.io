====================================================================
 Ejagoid: phonological analysis and inventory of a made-up language
====================================================================

:authors: Alan Trick, Katie Peacock, Jenni Fischer, and Sheryl Nelson
:class: LING 330 at TWU
:tags: Paper, Linguistics
:date: 2008-3-9

Syllable and Morpheme Divisions
===============================

==  =========  ===========  ===========  ============  =========
#   _ (imp.)   He _ (imp.)  He _ (hab.)  He _ (pres.)  ‘’
==  =========  ===========  ===========  ============  =========
1   βə.ɾ-ɛ     ɑ.-βə.ɾ-ɛ    ɑ.-βə.ɾ-ɑ    ɑ.-βə.ɾ-ɛ.ɑ   hides
2   fɔ.ɡ-ɛ     ɑ.-fɔ.ɡ-ɛ    ɑ.-fɔ.ɡ-ɑ    ɑ.-fɔ.ɡ-ɛ.ɑ   resembles
3   ɡɛ.ŋ-ɛ     ɑ.-ɡɛ.ŋ-ɛ    ɑ.-ɡɛ.ŋ-ɑ    ɑ.-ɡɛ.ŋ-ɛ.ɑ   leans
4   kɑ.ɾ-ɛ     ɑ.-kɑ.ɾ-ɛ    ɑ.-kɑ.ɾ-ɑ    ɑ.-kɑ.ɾ-ɛ.ɑ   gives
5   ku.m-i     ɑ.-ku.m-i    ɑ.-ku.m-ɑ    ɑ.-ku.m-i.ɑ   sits
6   pi.n-i     ɑ.-pi.n-i    ɑ.-pi.n-ɑ    ɑ.-pi.n-i.ɑ   tumbles
7   ty.ɡ-i     ɑ.-ty.ɡ-i    ɑ.-ty.ɡ-ɑ    ɑ.-ty.ɡ-i.ɑ   slings
8   βu.-i      ɑ.-βu.-i     ɑ.-βu.-ɑ     ɑ.-βu.-ɑ      blocks
9   d͡ʒɔ.-ɛ     ɑ.-d͡ʒɔ.-ɛ     ɑ.-d͡ʒɔ.-ɑ    ɑ.-d͡ʒɔ.-ɑ      remain
10  sɑ.-ɛ      ɑ.-sɑ.-ɛ     ɑ.-sɑ-ɑ      ɑ.-sɑ-ɑ       chews
11  sy.-i      ɑ.-sy.-i     ɑ.-sy.-ɑ     ɑ.-sy.-ɑ      unloads
==  =========  ===========  ===========  ============  =========


Morpheme Chart
==============

==  ===========  ==============  ============  ===========
#   Gloss        Form            Alternations  Features
==  ===========  ==============  ============  ===========
a   ‘_ (imp.)’   [-ɛ][-i]        [-ɛ]~[-i]     [high][tns]
b   ‘he’         [ɑ-]
c   ‘_ (hab.)’   [-ɑ]
d   ‘_ (pres.)’  [-ɛɑ][-iɑ][-ɑ]  [-ɛ]~[-i]~∅   [high][tns]
1   ‘hides’      [βəɾ]
2   ‘resembles’  [fɔɡ]
3   ‘leans’      [ɡɛŋ]
4   ‘gives’      [kɑɾ]
5   ‘sits’       [kum]
6   ‘tumbles’    [pin]
7   ‘slings’     [tyɡ]
8   ‘blocks’     [βu]
9   ‘remain’     [d͡ʒɔ]
10  ‘chews’      [sɑ]
11  ‘unloads’    [sy]
==  ===========  ==============  ============  ===========


Phonological Processes 1
========================

Rule Write-up
-------------

Title
    Deletion (D1)
Description
    the middle sound in VVV patterns is deleted.
Rule Notation
    V → ∅ / V_V
Motivation
    Syllable pattern conformity. Ejagoid does not allow VVV patterns.

Examples
--------

Rule applies:

+----+------------+---------------------+
| 7d | [ɑ.-βu.-ɑ] | ‘He blocks (pres.)’ |
+----+------------+---------------------+

Rule doesn’t apply:

+----+------------+---------------------+
| 7c | [ɑ.-βu.-ɑ] | ‘He blocks (hab.)’  |
+----+------------+---------------------+

The rule applies in 7d deleting the [ɛ,i] underlying sound it the (pres.) marker. It does not apply in 7c since that underlying sound (and the VVV pattern) is not there.

Underlying Sounds/Native Awareness
----------------------------------

(these sections were missing/incomplete in my draft)

Phonological Process 2
======================

Rule Write-up
-------------

Title
    Raising Harmony
Description
    [-bk] vowels become [+hi] when preceded by [+hi] vowels; elsewhere, they become [-hi]. Because this is vowel harmony a consonant can exists between the vowels.
Rule Notation
    | [-bk] → [+hi] / [+hi](C)_
    | [-bk] → [-hi] / elsewhere
Motivation
    The [-bk] vowel assimilates the [+hi] feature when preceded a [+hi] vowel.  The tongue remains high due to the tongue position of the preceding vowel.

Examples
--------

+----+-------------+--------------------+
| 2a | [fɔ.ɡ-ɛ]    | ‘resembles (imp.)’ |
| 7b | [ɑ.-ty.ɡ-i] | ‘He slings’        |
+----+-------------+--------------------+

Contrast
--------

+----+----------+------------------+
| 3a | [ɡɛ.ŋ-ɛ] | ‘leans (imp.)’   |
| 6a | [pi.n-i] | ‘tumbles (imp.)’ |
+----+----------+------------------+

The only two sounds involved in alternation are [ɛ] and [i]. These can be said to contrast due to the above example, which is a near-minimal pair. Apart from [ɛ] and [i], the only differences are place of articulation, which is not sufficient to motivate heightening or lowering of the vowel. It may be said that the height of the first vowel assimilates the height of the second, or vise-versa; but both vowels can not assimilate height simultaneously.

Classification
--------------

Classification is not possible and it does not have to be a phrase rule or a word rule. By default we say it is a phase rule.

Native Awareness
----------------

Ejagoid has two conceptual sound which can result in the [i] or [ɛ] phones: [i] and [ɛ,i]. Depending on which conceptual sound is present in the word, a native speaker may or may not be aware of the difference.

Alphabet and Conceptual Sounds
==============================

.. image:: /writings/school/ejagoid_inventory.svg
   :alt: Vector graphics image of the alphabet and conceptual sounds


Language Learning
=================

Mis-pronounced English words
----------------------------

+---+-----------------+------------------+----------------------+------------------+
| # | English Written | English Phonetic | Ejagoid Perception   | Ejagoid Accented |
+===+=================+==================+======================+==================+
| 1 | “to Ian”        | [tʰu i.ən]       | [t][u][i][ə][n]      | [tu ən]          |
+---+-----------------+------------------+----------------------+------------------+
| 2 | “he gets”       | [hi ɡɛts]        | [f][i][ɡ][ɛ,i][t][s] | [fi ɡits]        |
+---+-----------------+------------------+----------------------+------------------+

Notes on 2: say “gets” slowly and clearly, otherwise an English rule will change the pronunciation to [ɡəts].

Non-technical Explanation
-------------------------

Deletion Process
````````````````

Your language does not allow three vowels to occur beside each other, while English does.  In the phrase above; [u], [i], and [ə] occur consecutively. In Ejagoid, you wouldn't actually say the middle vowel, [i], but in English that rule doesn't exist, so you have to say all three vowels together. This will be difficult; however, one trick that will work at first is to put a little break between the two words [tʰuʔiən], this isn't exactly correct, but it is a good to place to start and English speakers will at least understand what you're saying.

Heightening Harmony Process
```````````````````````````

Your language has a rule that will cause certain vowels to change to conform to the vowel preceding it.  This makes the word easier to say.  However, English does not have this same rule.  A wide variety of vowels can occur within one word.  For example, in the English phrase above, “he gets”, we have an [i] sound followed by an [ɛ] sound.  Your language would not allow this to occur and would change the second vowel to match the first.  An awareness of the different rules which govern how vowels are handled in each language will help you to correct mispronunciations in English.