=============================================================================
 The Nature of Thought and Language: Difference, Determinism, and Relativity
=============================================================================


:class: LING 101 at TWU
:tags: Paper, Linguistics
:date: 2006 November 21

Introduction
============

Edward Sapir and Benjamin Whorf have become prominent linguists for
their work on the theories of linguistic determinism and relativity. The
theories have been highly confidential. Gumperz and
Levinson [GL1996]_ quote Wason and Johnson-Laird:
“Whorf's hypothesis [of linguistic determinism] has engendered much
confusion, and many circular arguments.” Some view the theory as a
threat to rationalism, while others support it, afraid of the
alternatives [JL1992]_.

There are three ideas that should be laid out before we proceed. First,
there is linguistic difference which states that languages differ in the
meanings they assign to different words. Second, linguistic determinism
proposes that the way we think is influenced by our language. This has
two forms: strong linguistic determinism where it is impossible to think
outside of the restraints of our language and weak linguistic
determinism where it is difficult (but still possible) to think outside
of these boundaries. The consequence of linguistic difference and
linguistic determinism is a third theory, linguistic relativity which
states that individual though processes differ among people in different
languages. [GL1996]_

This description of the theories does not do them justice. It is
plausible to rework linguistic difference and linguistic determinism to
imply a conclusion that is completely opposite to linguistic relativity.
The details and reasons for these ideas will be described later on.

The range of views on linguistic determinism relativity can also be
described with two extremes: the cloak and the mold theories. In the
cloak theories, language takes the form of a cloak by conforming to the
categories of its speakers' thoughts. In the mold theories, language is
viewed as a mold that it's speakers' thoughts must conform
to [DC1994]_. The strong determinism that Whorf was popular
would be a mold theory. Weak linguistic determinism would lie somewhere
in-between the two extremes.

Empirical evidence for these theories of language ranges from well
documented sociological experiments to trivial examples and appeals to
common sense. However, in spite of all of the research done over the
last century, there are still a large number of conflicting ideas on the
subject.

Linguistic Difference
=====================

According to Gumperz & Levinson [GL1996]_
linguistic difference is the hypothesis that language vary significantly
in their semantic structure including the senses and denotations of
semantic pieces. This begs the question of what a “semantic piece”
really is. The answer to this question is not trivial and is one place
where linguists vary.

A simple example of linguistic difference would be the word “charity” in
modern English and English a few centuries ago. Today, the word means
what used to be called “alms” or “giving to the poor”. In older English
the word meant something else—a sort of selfless, divine love (at the
moment English does not have an equivalent word).

In this topic there are two common views. The first one looks at only
the semantic pieces that are used when communicating between one person
and another (individual words as well as meanings that are brought about
by their various arrangements). This is the view that was taken up by
many of the linguists in the early part of the last century including
Whorf.

The second view is that there are smaller semantic atoms from which we
construct our words and phrases. These smaller pieces are common among
all languages, but the words and phrases that we build from them will
differ from one language to another. So even though the particular sense
and denotation of a word in one language is not provided by a single
word in a second language, the semantic pieces are still available and
it would be possible to construct such a
word [GL1996]_.

One example of this second stance known as the Language of Thought
Hypothesis (LOTH). LOTH proposes that the language of thought is
separate from the language we speak and attempts to explain how our mind
understands idea and how they are stored and represented. This is very
similar to the classical The language of them mind is often referred to
as mentalese [MA2004]_.

There has been a large amount of research on the existence and nature of
differences between languages. Lucy conducted a survey among English and
Yucatec speakers. The study was composed of asking the language speakers
to compare different picures containing animals, implements, and
substances. In the study it was found that English speakers mentioned
numbers more often, particularly for animals and implements. Yucatec
speakers mentioned numbers for substance and implements most often. Lucy
concludes that “these patterns suggest strongly that the frequency of
pluralization in each language influences both the verbal and non-verbal
interpretation of the pictures” [JL1992]_ and later on
“there is a good preliminary evidence that diverse language forms bear a
relationship to characteristic cognitive responses in
speakers.” [JL1992]_

We ought to be careful not to go over-board when searching for
differences between languages. The existence of differences on a subject
between languages does not prove that the viewers of those languages
view that subject differently. Paul Kay gave an example of the Brazilian
translation of “San Jose Exit” which is “Entrada São Jose” (lit.
Entrance San Jose). He pointed out that in spite of the differences in
the phrases, Brazilians and North Americans have the same view of their
highway systems. Incidentally, after giving the example Paul makes this
note: Several expressed the idea that this example had revealed to them
that Brazilian and North Americans in fact conceptualize highways in
distinct manners and went on to offer ingenious characterizations of the
conflicting Brazilian and North American conceptualizations of road
systems, based entirely on the observation regarding entrada versus
exist. [GL1996]_

Apart from paying attention to your professor during the lesson, this
example demonstrates the importance of empirical evidence in determining
the existence of real differences between the way that different
language groups view concepts.

Linguistic Determinism
======================

Linguistic determinism is the next realm of discussion. This piece is
concerned with the relationship between our thoughts and the language we
speak. The view of our mind thinks of words and phrases is critical to
this section. Traditional determinism as in the Sapir-Whorf hypothesis
carries a 'mold-like' view of thought and language. The way that we
understand our thoughts is through the faculties provided by are
language. The language of the mind is similar to the language that we
use to talk to each other. As noted earlier there is the strong view
that it is impossible to think outside what your language provides, as
well as a weaker view in which thinking outside the box of ones language
is possible, but difficult. Sapir describes the theory with the example
of a poem.

    The understanding of a simple poem, for instance, involves not merely
    an understanding of the single words in their average significance, but
    a full comprehension of the whole life of the community as it is
    mirrored in the words, or as it is suggested by their overtones.

And summarizes this view shortly afterward:

    We see and hear and otherwise experience very largely as we do because
    the language habits of our community predispose certain choices of
    interpretation. [HSM1985]_

A similar view comes from the Behaviorist's who present a strong,
deterministic view. Behaviorism views thought and language as the same
process and thus there can be no thinking outside of our
language [LH2004]_. Along with the view that languages are
significantly different in their semantic make-up, this is the most
extreme view of linguistic relativity. [GL1996]_

Portraits of linguistic determinism have shown up in literature as well.
In George Orwell's novel 1984, a totalitarian government creates a
language called 'Newspeak' built around its ideology known as Ingsoc.
One of its authors, Syme, describes the purpose of the language:

    Don't you see that the whole aim of Newspeak is to narrow the range of
    thought? In the end we shall make thoughtcrime literally im-possible,
    because there will be no words in which to express it ... Already, in
    the Eleventh Edition, we're not far from that point ... (but) in the end
    there won't be any need even for that. The Revolution will be complete
    when the language is perfect. Newspeak is Ingsoc and Ingsoc is
    Newspeak. [GO1984]_

Another relevant example of determinism is the Pirahã tribe of Brazil.
Their number system contains three different amounts: 1, 2, and many.
The situation seems to indicate that the Pirahã never had any use for
precision with numbers that were greater than two. Peter Gordon carried
out some experiments to determine the effect of this on their
understanding of number. With two or three objects, the Pirahã were able
to count an keep track of numbers, but anything greater than three and
the best they could do was approximate [CB2004]_.

The example of the Pirahã is not entirely different from much that goes
on in the rest of the modern world. The key here is that the human mind
has a finite amount of storage room. To use it efficiently, it decides
which pieces of information are useful and relevant, and which are not.

English speakers are unable to distinguish the [p] sound from the [pʰ]
sound because that distinction does not exist in our language. The
English speaker will still hear different sounds, but the brain will
categorize them as being the same. When looking at another ethic group,
a viewer sometimes remarks that “they all look the same”. The reality is
that they are just as different as everyone else, the viewer simply does
not have the mental categories to describe their differences. Over time,
this distinction will change if the viewer is more exposed to those
kinds of people and sub-consciously learn how to distinguish one person
from another.

Linguistic Relativity and Universalism
======================================

At the end we come to linguistic relativity and it's counterpart,
universalism. If the fundamental semantic structure of languages are
different and there is the semantic structure of a language affects the
way its speakers' understanding, then people will understand things in
different ways between languages. That is linguistic relativity which
Sapir and Whorf were well known for.

Universalism (in a linguistic sense) is the opposite of relativity.
Universalists usually hold that every language has some fundamental
semantic pieces which are the same. Chandler writes, quoting Karl
Popper.

Universalists argue that we can say whatever we want to say in any
language, and that whatever we say in one language can always be
translated into another. This is the basis for the most common
refutation of Whorfianism. 'The fact is,' insists the philosopher Karl
Popper, 'that even totally different languages are not untranslatable'.
The evasive use here of 'not untranslatable' is ironic. [DC1994]_

Chandler notes that most universalists will agree that this is somewhat
problematic and while different languages might not be 'untranslatable',
perfect translations are probably impossible.

Conclusion
==========

Such is the nature of thought and language. The cloak theories state
that there is difference between how we talk and how we talk affects how
we understand things; therefore, we understand things in different ways.
The mould theories stats that at a basic level all languages are
contained of the same 'stuff' and that our thought processes are
separate and independent from our language processes; therefore, it is
possible to reach a common understanding.

In reality, these theories are rarely as extreme as presented here. For
example, even though a mould theorist might put some level of
restrictions on our ability to understand each other across, language
barriers, that does not mean that understanding is altogether
impossible.

References
==========

.. [DC1994]
    .. bib::
        :authors: Daniel Chandler
        :published: 1994
        :title: The Sapir-Whorf Hypothesis
        :url: http://www.lex.unict.it/didattica/materiali06/storiamed_mz/vocabolario/svolta_linguistica.htm
        :retrieved: 2006-11-21
.. [MA2004]
    .. bib::
        :authors: Murat Aydede
        :published: 2004
        :title: The Language of Thought Hypothesis. Stanford Encyclopedia of Philosophy
        :url: http://setis.library.usyd.edu.au/stanford/entries/language-thought/
        :retrieved: 2006-11-21
.. [LH2004]
    .. bib::
        :authors: Larry Hauser
        :published: 2004
        :title: Behaviorism. Internet Encyclopedia of Philosophy
        :url: http://www.iep.utm.edu/b/behavior.htm
        :retrieved: 2006-11-21
.. [CB2004]
    .. bib::
        :authors: Celeste Biever
        :published: 2004
        :title: Language may shape human thought
        :url: http://www.newscientist.com/article.ns?id=dn6303
        :retrieved: 2006-11-21
.. [GL1996]
    .. bib::
        :authors: J. Gumperz, S. Levinson
        :published: 1996
        :title: Rethinking linguistic relativity
        :publisher: Cambridge University Press
        :pages: 23-24, 97
.. [HSM1985]
    .. bib::
        :authors: D. Hymes, E. Sapir, D. Mandelbaum
        :published: 1985
        :title: Selected Writings of Edward Sapir in Language, Culture and Personality
        :publisher: University of California Press
        :page: 162
.. [JL1992]
    .. bib::
        :authors: J. A. Lucy
        :published: 1992
        :title: Grammatical categories and cognition
        :publisher: Cambridge University Press
        :pages: 152-158
.. [GO1984]
    .. bib::
        :authors: G. Orwell
        :published: 1984
        :title: 1984
        :publisher: Signet Classic
        :page: 52
